BrightFire Stellar
==================
## 1.8 ##
### Adds
 * VISUAL EDITOR: adds style formats
 * VISUAL EDITOR: adds visual styles
 * VISUAL EDITOR: removes font family and font size dropdowns
 * CSS: force-no-container responsive classes
 * ADMIN: icons on stellar tabs
 * ADMIN: SCRIPTS tab
 * Output custom scripts in <head>
 * Reviews Aggregate Widget
 * Reviews Content Widget
 * Reviews Slider Widget
 * Deregister legacy review widgets
 * MENUS: add support for icons in tabbed megamenus
 * MENUS: reworks CSS and class options for more styles
 * RECIPES: adds Bob to Network Recipes
 
### Fixes
 * ADMIN: removes stellar admin bar from anything but administrators
 * ADMIN: flyout menu jump bug
 * MENUS: output menu item target correctly
 
## 1.7.2 ##
### Adds
 * FONT: Roboto Condensed
 * READY CLASSES: fixed line heights to widget global and breakpoints
 * SCSS: remove margin bottom from site identity widget tags
 
## 1.7.1 ##
### Adds
 * FONT: Quicksand
 * FONT: Merriweather Sans
 * FONT: Cormorant Infant
 * FONT: Martel
 * FONT: Alegreya Sans
 * FONT: Dosis
 
### Fixes
 * LOCATION WIDGET: Use new naming convention for section margins
 
### Refactor
 
## 1.7.0 ##
### Adds
 * PAGES: Add post_meta to pages for Form Assignment
 * FORM WIDGET: Add 'Dynamic' option to hook Form Assignment post_meta
 * MENUS: Allow fixed-line-height classes to inline and stacked menu widgets
 * LAYOUTS: Add global control for gutter size multiples
 * LAYOUTS: Add row option for additional gutter padding
 * READY CLASSES: Adds half-size padding and margins
 * NETWORK UPDATER: dB updater for Stellar 1.7 to update margin and padding classes to new naming convention

### Fixes
 * Remove margin from last .post and paragraphs in Recent Blog Post Widget
 * Theme JS: use jQuery actual plugin to determine heights of widgets (fixes match height in menu panels)

### Refactor
 * BUCKET WIDGET: Re-index 'background_image' to 'image_id'
 * SCSS: Reverse gradient/image priorities in background CSS build so gradients can be above images
 * READY CLASSES: Update naming convention for line-height, margins, and padding classes based on multiples
 

## 1.6.0 ##
### Adds
 * BUCKET WIDGET: widget title to BFucket Widget
 * READY CLASSES: text-, heading-, and link-color-gray to custom classes (we are already generating the CSS for these)
 * SCSS: better styles for pagination
 * MENU: Use caret instead of plus and minus signs. Use transform to rotate on open
 * SCSS: Make validation error text red
 
### Fixes
 * RECIPES: On recipe load, insure that any unused widgets are put in "Inactive Widgets"
 * MIXES: prevent "invalid foreach" when no mixes exist
 * MIXES: handle Title Callback mo better
 * MIXES: do not display Mix if it has no name
 * MIXES: don’t output empty mix class titles
 * MIXES: consistent field titles for our classes
 * MIXES: move mixes field functions to proper file
 * MIXES: Disallow deletion of mixes that are set on active widgets
 * CUSTOMIZER: Fix refresh bug (row settings aren't keeping after page refresh)
 
### Refactor
 * MENU: Major refactor to allow megamenu content to be directly below nav item in mobile. Adds scrollto for mobile menu clicks
 * MIXES: refactor repeater fields to have unique field ids and condense code for better readability
 
## 1.5.0 ##
 * FEATURE: Background image option on Bucket Widget global
 * FIX: specify direct child container of rows in customizer for proper preview
 * FIX: removes margin from list column classes

## 1.4.0 ##
 * FEATURE: adds Mixes feature
 * FIX: gravity forms conflicts in customizer js ( for good this time )
 * FIX: Template Admin page dynamic population of choices in "Main Content Row" and "Fixed Row" selectors
 * FIX: footer credit styles on mobile "credit-text"
 * REMOVE: vertical grid classes
 * REMOVE: several ready classes from rows and containers
 * ADD: hero text sizes
 * ADD: styles to footer credits widget and pagination from Insurance Library so we do not need core.min.css anymore
 * ADD: fixed height classes

## 1.3.2 ##
 * Fix Object conflicts in obj.customizer-preview.js
 * Create HoverIntent on menus

## 1.3.1 ##
 * FIX: customizer bug where gravity forms interfered with our Row class preview in customizer
 * REFACTOR: cleaned up customizer preview javascript
 * FIX: fixed ajaxurl for customizer preview of body styles
 * FIX: several out of place ready classes.
 * Rename match-widget-height ready class

## 1.3.0 ##
 * Major change to how line heights are computed
 * Documentation admin page is simplified
 * ADDS: .line-height-large to ready classes in specific widgets
 * ADDS: .force-no-padding to Rows/adds padding to left and right of all rows/removes body padding option
 * ADDS: .alpha50 ready class
 * ADDS: padding/margin XLarge and XXLarge ready classes
 * FIX: Widget Title Classes form in newly added widgets
 * FIX: Form widget default options
 * FIX: footer credit whitespace
 * FIX: button word wrapping
 * FIX: form file upload styles/force clear submit button when not displayed inline
 * FIX: Customizer Preview CSS

## 1.2.1 ##
 * FIX: old $usage that is not being used

## 1.2.0 ##
### Adds
 * BrightFire Form Widget
 * Stellar Status Page
 * Custom Menu colors option
 * Default sass variables save to database
 * Responsive image support and Image width option for Bucket Widget images
 
### Fixes
 * Menu Styles
 * Form styles
 * Border styles
 * Image import duplication while on same site
 * Image import when wp_error
 * Footer credit line height
 * Template assignments travel with recipes

## 1.1.0 ##
####Fixes
 * Recent Blog Posts Image Align: None
 * Mobile display of Child Page Links
 * 404 error display
 * Bucket Image icons render equal height and width (for usage with .circle-image)
 * Row min-height admin preview
 * List styles in content widgets
 * Desktop and Mobile classes in Customizer Rows
 * Remove default widget areas
 
####Adds
 * current-menu-item class to menus
 * Template column on Pages admin page
 * ability to hide labels in Gravity Forms
 * Desktop and Mobile classes for Row Containers
 * Italics ready classes
 * Location Styles
 * Body padding classes
 * Button text color choices
 * Disallow second step of Starter Form on mobile
 * Import images option for Recipes
 * Display error for non-existing menus
 * Removes Page Attributes meta box for page set to Front Page
 
## 1.0.0 ##
 * Add Gillman custom theme
 * Add row height option to customizer
 * Add featured image support for Rows and Bucket Widget
 * Add list column ready classes
 * Add circle image ready class
 * Refactor ready classes for optimization and future proofing
 * Refactor grid css to work outside of widgets
 * Fix menu class output
 * Fix page sidebar override on post_page
 * Fix Menu admin page errors in javascript and css
 * Fix Blog images for mobile
 
## 0.9.0 ##
 * Adds Smale Stover custom theme
 * Adds 3 more font choices
 * Several CSS bug fixes
 * Fix customizer Row class output for desktop and mobile
 * Fix undefined index in Inline Menu widget
 * Fix menu issue where click trigger caused bad propagation in customizer
 * Fix target lightbox for Bucket Widget button block
 * Fix image height in Bucket Widget
 * Fix where Bucket Widget image and button block link overrides were disappearing
 * Fix where desktop and mobile classes were not output in Bucket Widget Blocks

## 0.8.0 ##
 * Stellar beta for pioneers