<?php
/**
 * The template for displaying the header.
 *
 * @package BrightFire Stellar
 * @since 0.1.0
 */

add_action( 'brightfire_stellar_head', 'do_brightfire_stellar_head' );

function do_brightfire_stellar_head() {

	?>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta charset="<?php bloginfo( 'charset' ); ?>"/>
	<link rel="profile" href="http://gmpg.org/xfn/11"/>
	<?php
}

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<title><?php wp_title(); ?></title>
	<?php do_action( 'brightfire_stellar_head' ); ?>
	<?php echo \BrightFire\Theme\Stellar\stellar_font_styles(); ?>
	<?php wp_head();?>
	<?php echo \BrightFire\Theme\Stellar\custom_scripts( 'head' ); ?>
</head>


<?php $breakpoint = get_theme_mod('breakpoint', 768); ?>
<body <?php body_class(); ?> style="<?php echo \BrightFire\Theme\Stellar\stellar_body_background_styles() ?>" data-breakpoint=" <?php echo $breakpoint; ?>">