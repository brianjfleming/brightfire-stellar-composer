/*! BrightFire Stellar - v1.8
 * https://www.brightfire.com
 * Copyright (c) 2017; */
var $ = jQuery.noConflict();

$(document).ready(function () {

    // Utilities
    sortHandler();
    columnSlider();
    messageHandler();
    slideWidget();

});

function getRowValues() {

    var backgroundLayers = [];
    var containerBackgroundLayers = [];

        // Check for background layers
        $('#background-layers .bf-repeater').each( function() {

            var this_layer = {
                'color': $(this).find('input[data-key="color"]').val(),
                'color_bottom': $(this).find('input[data-key="color_bottom"]').val(),
                'color_stop': $(this).find('input[data-key="color_stop"]').val(),
                'color_bottom_stop': $(this).find('input[data-key="color_bottom_stop"]').val(),
                'gradient_angle': $(this).find('input[data-key="gradient_angle"]').val(),
                'featured_image': $(this).find('select[data-key="featured_image"]').val(),
                'image_id': $(this).find('input[data-key="image_id"]').val(),
                'image_attachment': $(this).find('select[data-key="image_attachment"]').val(),
                'image_size': $(this).find('select[data-key="image_size"]').val(),
                'image_repeat': $(this).find('select[data-key="image_repeat"]').val(),
                'image_position': $(this).find('select[data-key="image_position"]').val()
            };

            backgroundLayers.push( this_layer );

        } );

        $('#container-background-layers .bf-repeater').each( function() {

            var this_layer = {
                'color': $(this).find('input[data-key="color"]').val(),
                'color_bottom': $(this).find('input[data-key="color_bottom"]').val(),
                'color_stop': $(this).find('input[data-key="color_stop"]').val(),
                'color_bottom_stop': $(this).find('input[data-key="color_bottom_stop"]').val(),
                'gradient_angle': $(this).find('input[data-key="gradient_angle"]').val(),
                'featured_image': $(this).find('select[data-key="featured_image"]').val(),
                'image_id': $(this).find('input[data-key="image_id"]').val(),
                'image_attachment': $(this).find('select[data-key="image_attachment"]').val(),
                'image_size': $(this).find('select[data-key="image_size"]').val(),
                'image_repeat': $(this).find('select[data-key="image_repeat"]').val(),
                'image_position': $(this).find('select[data-key="image_position"]').val()
            };

            containerBackgroundLayers.push( this_layer );

        } );

        // All settings from the page set into an object
        settings = {
            'id': $('#id').val(),
            'data': {
                'name': $('#name').val(),
                'description': $('#description').val(),
                'left': $('.column-ranger').find('.left .number').text(),
                'right': $('.column-ranger').find('.right .number').text(),
                'background-behavior' : $('#background-behavior').val(),
                'animation' : $('#animation').val(),
                'slideshowSpeed' : $('#slideshowSpeed').val(),
                'animationSpeed' : $('#animationSpeed').val(),
                'min_height': $('#min_height').val(),
                'gutter' : $('#gutter').val(),
                'custom-classes': $('#custom-classes').val(),
                'desktop-classes': $('#desktop-classes').val(),
                'mobile-classes': $('#mobile-classes').val(),
                'background-layers': backgroundLayers,
                'container-background-behavior' : $('#container-background-behavior').val(),
                'container-animation' : $('#container-animation').val(),
                'container-slideshowSpeed' : $('#container-slideshowSpeed').val(),
                'container-animationSpeed' : $('#container-animationSpeed').val(),
                'container-classes': $('#container-classes').val(),
                'container-desktop-classes': $('#container-desktop-classes').val(),
                'container-mobile-classes': $('#container-mobile-classes').val(),
                'container-background-layers': containerBackgroundLayers,

            }
        };

        return settings;
}

function sortHandler() {

    $('.rows td').sortable({
        cursor: 'move',
        axis : 'y',
        distance: 2,
        opacity: 0.8,
        tolerance: 'pointer',
        start: function(e, ui){
            ui.placeholder.height(ui.item.height());
        },
    });

    $('.sort-items').find('tbody').sortable({
        cursor: 'move',
        axis : 'y',
        distance: 2,
        opacity: 0.8,
        tolerance: 'pointer',
        start: function(e, ui){
            ui.placeholder.height(ui.item.height());
        },
        helper: function(e, ui) {
            var children = ui.children();
            for ( var i=0; i<children.length; i++ ) {
                var selector = $(children[i]);
                selector.width( selector.width() );
            }
            return ui;
        },
        stop: function(e, ui) {
            // remove fixed widths
            ui.item.children().css('width','');
        },
        update: function(e, ui) {
            updateItemSort(e, ui);
        }
    });
}

function updateItemSort(e, ui) {
    var ids = [];
        option = ui.item.parents( 'table' ).attr( 'id' );

    ui.item.find('.spinner').addClass('is-active');

    $('#' + option ).find('tbody tr').each( function() {
        ids.push($(this).attr('id'));
    });

    sortVars = {
        'ids': ids,
        'option': option,
    };

    postVars = {
        action: 'layouts_ajax',
        sort_items: sortVars,
        security: $('#security').val(),
    };

    $.post(ajaxurl, postVars, function () {
        ui.item.find('.spinner').removeClass('is-active');
    });
}

function columnSlider(){

    $(document).on( 'input change', '.column-ranger input', function(){

        var left = $(this).val();
        right = 12-left;

        $(this).parent('.column-ranger').find('.left .number').text(left);
        $(this).parent('.column-ranger').find('.right .number').text(right);
    });
}

function messageHandler( message ) {

    // Print the object to the page
    $('#wpbody').append( message );

    // Removes Errors
    $( "#error-message" ).delay(2000).fadeOut(200,function() {
        $(this.remove());
    });

    // Removes Success
    $( "#success-message" ).delay(2000).fadeOut(200,function() {
        $(this.remove());
    });

}

function slideWidget() {

    // Toggle slide Title
    $(document).on( 'propertychange', 'select[data-key="slidebar"]', function() {
        var title = $(this).find('option:selected').text();
        $(this).parents('.bf-repeater').find('h3').text( title );
    } );

}

$( document ).ready( function() {

    saveRecipe();
    loadRecipe();
    deleteRecipe();
    exportRecipe();
    importRecipe();

} );

function saveRecipe(){

    // Save Recipe button click
    $('#save-recipe').click(function () {

        // Get the recipe name
        var recipe = $( '#new-recipe-name' );
        var recipeName = recipe.val();
        var scope = '';

        // Error for no name
        if ( recipeName === '' ) {
            recipe.css( 'border-color', 'red' );
            alert( 'You must name the recipe.' );
            return;
        }

        // Checking scope of where this should be saved
        if ( $('#network-recipe').is(':checked') ) {
            scope = 'network';
        } else {
            scope = 'site';
        }

        // Turn on spinner
        $("#saving.spinner").addClass('is-active');

        // Set up our JSON object to send
        var postVars = { action: 'recipe_ajax', save: recipeName, scope: scope, security: $('#security').val() };

        // Send off the ajax
        $.post( ajaxurl, postVars, function ( response ){

            // Reset the new values for our selectors
            $('#load-recipe-selector').replaceWith(response.load);
            $('#delete-recipe-selector').replaceWith(response.delete);
            $('#export-recipe-selector').replaceWith(response.export);
            $('#network-recipe').prop('checked', false);
            recipe.val('');

            // Turn off spinner
            $( '#saving.spinner' ).removeClass('is-active');

            messageHandler( response.message );

        });
    });
}

function exportRecipe() {

    $(document).on( 'submit', '#export-recipe-form', function() {

        // Need to make and set scope (network or site)
        var selected = $( '#export-recipe-selector').find( 'option:selected:not([disabled])' );


        if ( selected.length > 0 ) {
            var scope = selected.closest("optgroup").data('scope');
            $('input#export-recipe-scope').val(scope);
            return true;
        } else {
            alert('Please select a recipe to export');
            return false;
        }

    });

}

function importRecipe() {

    $(document).on( 'change', '#import-recipe-file', function() {

        if( $(this).val() !== '' ) {

            $('#importing').addClass('is-active');
            $('#import-recipe-form').submit();

        }

    });
}

function loadRecipe() {

    // Load Recipe button clicked
    $('#load-recipe').click(function () {

        // Get the selected option
        var selected = $( '#load-recipe-selector').find( 'option:selected' );
        var loadRecipe = selected.val();

        // Do we want images
        if ( $('#image-upload').is(':checked') ) {
            import_images = 'true';
        } else {
            import_images = '';
        }
        
        if ( loadRecipe ) {
            // Find the scope of the selected recipe
            var scope = selected.closest("optgroup").data('scope');

            var postVars = { action: 'recipe_ajax', load: loadRecipe, scope: scope, import_images: import_images, security: $('#security').val() };

            $("#loading.spinner").addClass('is-active');

            $.post( ajaxurl, postVars, function ( response ){
                location.href = response;
            });
        }
    });
}

function deleteRecipe() {
    $('#delete-site-recipe').click(function () {
        var selected = $( '#delete-recipe-selector' ).find( 'option:selected' );
        scope = selected.closest("optgroup").data('scope');
        deleteNetwork = '';

        if ( scope === 'network' ) {
            deleteNetwork = prompt( 'YOU ARE DELETING A NETWORK LEVEL RECIPE!\nIf you really want to do this type "DELETE" to continue:', '' );
        } else if ( scope === 'site' ) {
            deleteNetwork = 'DELETE';
        }

        var deleteRecipe = selected.val();

        var postVars = { action: 'recipe_ajax', delete: deleteRecipe, scope: scope, security: $('#security').val() };

        if ( deleteNetwork === 'DELETE' ) {
            $("#deleting.spinner").addClass('is-active');

            $.post( ajaxurl, postVars, function ( response ){
                
                // Replace our selectors with the new population
                $('#load-recipe-selector').replaceWith(response.load);
                $('#delete-recipe-selector').replaceWith(response.delete);
                $('#export-recipe-selector').replaceWith(response.export);

                // Turn off the spinner
                $( '#deleting.spinner' ).removeClass('is-active');

                messageHandler( response.message );

            });
        }
    });
}




$( document ).ready( function () {

    saveTemplate();
    deleteTemplate();
    toggleTemplateRepeater();

    $( document ).on( 'bf-repeater-sort bf-repeater-remove', function() {
        init_row_select();
    });

    $( document ).on( 'change', '#header select, #content select, #footer select', function() {
        init_row_select();
    });

} );

function getTemplateRows(){

    var allRows = {
        headerRows:[],
        contentRows:[],
        footerRows:[],
    };

    $('#header').find( 'select' ).each( function() {
        if( '' !== $(this).val() ) {
            allRows.headerRows.push($(this).val());
        }
    });

    $('#content').find( 'select' ).each( function() {
        if( '' !== $(this).val() ) {
            allRows.contentRows.push($(this).val());
        }
    });

    $('#footer').find( 'select' ).each( function() {
        if( '' !== $(this).val() ) {
            allRows.footerRows.push($(this).val());
        }
    });

    return allRows;
}

function saveTemplate() {

    $(document).on( 'click', '#save-template' , function() {

        var assignments = [];
            allRows = getTemplateRows();

        $('#assignments').find('.assignment').each(function () {
            if ($(this).prop('checked')) {
                assignments.push($(this).val());
            }
        });

        // All settings from the page set into an object
        settings = {
            'id': $('#id').val(),
            'data': {
                'name': $('#name').val(),
                'description': $('#description').val(),
                'header' : allRows.headerRows,
                'content' : allRows.contentRows,
                'footer' : allRows.footerRows,
                'assignments' : assignments,
                'sidebar_col' : $('#sidebarcol').find('select').val(),
                'sidebar_row' : $('#contentrow-select').val(),
                'fixed_row'   : $('#fixedrow').find('select').val()
            }
        };

        // Ajax vars
        postVars = {
            action: 'layouts_ajax',
            save_template: settings,
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function (response) {

            $('h1').text( response.title );
            history.pushState(null, null, response.history );
            // Lock the assignments
            $('#assignments').find('.assignment').each(function () {
                if ($(this).prop('checked')) {
                    $(this).attr('disabled', true);
                }
            });
            messageHandler( response.success );
        });
    });
}

function deleteTemplate() {
    $(document).on( 'click', '#delete-template' , function() {

        var deleteInfo = {
            id : $('#id').val(),
            name : $('#name').val(),
        };

        // Ajax vars
        postVars = {
            action: 'layouts_ajax',
            delete_template: deleteInfo,
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function ( response ) {
            location.href = response;
        });

    });
}

function toggleTemplateRepeater() {

    $(document).on( 'change', '#contentrow select, #sidebarcol select', function(){

        var contentRow = $('#contentrow').find('select').val();
        var sidebarCol = $('#sidebarcol').find('select').val();

        $('.sidebar-col').removeClass('sidebar-col');

        if ( contentRow && sidebarCol ){
            $(document).find( '.row-' + contentRow + ' .' + sidebarCol ).addClass('sidebar-col');
        }
    });

    $(document).on( 'change', '.admin_page_stellar-template-edit .bf-repeater-inside select', function(){

        var selectedRow = $(this).val();
        var titlebar = $(this).parents('.bf-repeater').find('.bf-repeater-top');
        var postVars = {
            action: 'repeater',
            rowSelected: selectedRow
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function ( response ) {
            titlebar.html('');
            titlebar.append(response).hide().fadeIn(300);
        });

    });
}

function init_row_select() {

    var rows_change = {
        'rows': getTemplateRows(),
        'content_selected': $('#contentrow').find('select').val(),
        'fixed_selected': $('#fixedrow').find('select').val()
    };

    // Ajax vars
    postVars = {
        action: 'layouts_ajax',
        rows_change: rows_change,
        security: $('#security').val(),
    };

    // Send to ajax and get the response
    $.post(ajaxurl, postVars, function (response) {
        $('#contentrow-select').html(response.content);
        $('#fixedrow-select').html(response.fixed);
    });
}

$( document ).ready( function() {

    stellarThemeHandlers();

} );

function stellarThemeHandlers() {

    $(document).on( 'click', '.stellar-theme-activate', function() {

        var postVars = { action: 'theme_ajax', set_theme: $(this).attr('data-theme'), security: $('#security').val() };
        var theme = $(this).attr('data-theme');

        // Turn on spinner
        $(this).siblings('.spinner').addClass('is-active');

        $.post( ajaxurl, postVars, function ( response ){

            restoreThemeDefaults();

            activateTheme( theme );

        });

    } );

    $(document).on( 'click', '.stellar-theme-deactivate', function() {

        var postVars = { action: 'theme_ajax', reset_theme: true, security: $('#security').val() };

        // Turn on spinner
        $(this).siblings('.spinner').addClass('is-active');

        $.post( ajaxurl, postVars, function ( response ){

            restoreThemeDefaults();

        });

    } );

    $(document).on( 'click', '.stellar-theme-overlay .close', function() {
        $('.stellar-theme-overlay').fadeOut(100);
    } );

    $(document).on( 'click', '.theme-backdrop', function() {
        $('.stellar-theme-overlay').fadeOut(100);
    } );

    $(document).on( 'click', '.stellar-theme', function(e) {

        if(  $(e.target).hasClass('stellar-theme-activate') ) {
            return false;
        }

        if(  $(e.target).hasClass('stellar-theme-deactivate') ) {
            return true;
        }

        var theme_info = $.parseJSON( $(this).attr('data-info') );

        // Set up markup
        var license_markup = '';
        var version_markup = 'Version: ' + theme_info.ver;
        var name_markup = theme_info.name + '<span class="theme-version">' + version_markup + '</span>';
        var author_markup = 'by <a href="' + theme_info.author_uri + '" target="_blank">' + theme_info.author + '</a>';
        var description_markup = '<p>' + theme_info.desc + '</p>';
        var inactive_action_markup = '<span class="spinner"></span> <a class="button button-primary stellar-theme-activate" href="javascript: void(0);" data-theme="' + theme_info.id + '">Activate</a>';
        var active_action_markup = '<span class="spinner"></span> <a class="button button-primary stellar-theme-deactivate" href="javascript: void(0);">Dectivate</a>';


        if( $(this).hasClass('active') ) {
            $('.stellar-theme-overlay .current-label').show();
            $('.stellar-theme-overlay .active-actions').show();
            $('.stellar-theme-overlay .inactive-actions').hide();
        } else {
            $('.stellar-theme-overlay .current-label').hide();
            $('.stellar-theme-overlay .active-actions').hide();
            $('.stellar-theme-overlay .inactive-actions').show();
        }

        if( theme_info.hasOwnProperty('license') ) {
            license_markup += 'Licensed under ' + theme_info.license;
        }

        if( theme_info.hasOwnProperty('license_uri') ) {
            license_markup += ' | <a href="' + theme_info.license_uri + '" target="_blank">Learn More</a>';
        }

        $('.theme-name').html(name_markup);
        $('.theme-author').html(author_markup);
        $('.theme-description').html(description_markup);
        $('.screenshot img').attr('src',theme_info.screenshot);
        $('.theme-license').html(license_markup);
        $('.stellar-theme-overlay .inactive-actions').html(inactive_action_markup);
        $('.stellar-theme-overlay .active-actions').html(active_action_markup);

        $('.stellar-theme-overlay').fadeIn(100);
    } );

}

function activateTheme( theme ) {
    $('.stellar-theme.' + theme).addClass('active').find('.stellar-theme-name, .stellar-theme-actions').addClass('active');
    $('.stellar-theme.' + theme + ' .active-actions').show();
    $('.stellar-theme.' + theme + ' .inactive-actions').hide();
    $('.stellar-theme-overlay').fadeOut(100);
}

function restoreThemeDefaults() {
    // Restores class defaults
    $('.stellar-theme-name, .stellar-theme, .stellar-theme-actions').removeClass('active');
    $('.inactive-actions').show();
    $('.active-actions').hide();
    $('.spinner').removeClass('is-active');
}


$( document ).ready( function() {

    addWidgetArea();
    toggleWidgetArea();
    saveWidgetArea();
    deleteWidgetArea();

} );

function addWidgetArea() {
    $(document).on( 'click', '.add-widget-area' , function( event ) {

        event.preventDefault();

        var option = $(this).data( 'option' );

        var placement = $('#' + option ).find('tbody');

        // Ajax vars
        postVars = {
            action: 'layouts_ajax',
            add_widget_area: option,
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function ( response ) {

            var newRow = $( response );

            placement.prepend( newRow );
        });

    });
}

function saveWidgetArea() {
    $(document).on( 'click', '.save-widget-area' , function() {

        $(this).parents('span').siblings('.spinner').addClass('is-active');

        var option = $(this).parents('table').attr('id');
        id = $(this).parents('tr').attr('id');
        name = $('#' + id).find('input.name').val();
        description = $('#' + id).find('input.description').val();
        allIds = [];

        $('#' + option ).find('tbody tr').each( function() {
            allIds.push( $(this).attr('id') );
        });

        if ( name === '') {
            return false;
        }

        widgetArea = {
            'sort': allIds,
            'option': option,
            'id': id,
            'data' : {
                'name': name,
                'description': description
            }
        };

        // Ajax vars
        postVars = {
            action: 'layouts_ajax',
            save_widget_area: widgetArea,
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function ( response ) {

            var row = $('#' + id );

            row.find('p.name').text( name );
            row.find('p.description').text( description );

            row.find('.editing-widget-area').hide();
            row.find('.view-widget-area').show();

           row.find('.spinner').removeClass('is-active');

        });

    });
}

function toggleWidgetArea() {

    $(document).on('click', '.cancel-widget-area', function() {

        var row = $(this).parents('tr');

        row.find('.editing-widget-area').hide();
        row.find('.view-widget-area').show();
    });

    $(document).on( 'click', '.edit-widget-area', function(){

        var row = $(this).parents('tr');

        row.find('.editing-widget-area').show();
        row.find('.view-widget-area').hide();
    });
}

function deleteWidgetArea() {

    $(document).on( 'click', '.delete-widget-area', function(){

        $(this).parents('span').siblings('.spinner').addClass('is-active');

        var option = $(this).parents('table').attr('id');
        tablerow = $(this).parents('tr');
        id = tablerow.attr('id');
        name = $(this).parents('tr').find('p.name').text();

        deleteInfo = {
            'id': id,
            'option': option,
            'name': name,
        };

        // Ajax vars
        postVars = {
            action: 'layouts_ajax',
            delete_widget_area: deleteInfo,
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function ( response ) {
            tablerow.remove();
        });
    });
}

$( document ).ready( function() {

    // Rows
    saveRow();
    deleteRow();
    toggleSlideOptions();

} );

function saveRow() {

    $(document).on( 'click', '#save-row' , function() {
        // Ajax vars
        postVars = {
            action: 'layouts_ajax',
            save_row: getRowValues(),
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function (response) {

            $('h1').text( response.title );
            history.pushState(null, null, response.history );
            messageHandler( response.success );
        });
    });
}

function deleteRow() {
    $(document).on( 'click', '#delete-row' , function() {

        var deleteInfo = {
            id : $('#id').val(),
            name : $('#name').val(),
            usage : $('#usage').val()
        };

        // Ajax vars
        postVars = {
            action: 'layouts_ajax',
            delete_row: deleteInfo,
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function ( response ) {
            location.href = response;
        });

    });
}

function toggleSlideOptions(){
    $(document).on( 'change', '#background-behavior', function(){
        var element = $('.slide-options');

        if ($(this).val() == 'layered'){
            element.hide( 200 );
        } else if ($(this).val() == 'row-slider'){
            element.show( 200 );
        }
    } );

    $(document).on( 'change', '#container-background-behavior', function(){
        var element = $('.container-slide-options');

        if ($(this).val() == 'layered'){
            element.hide( 200 );
        } else if ($(this).val() == 'row-slider'){
            element.show( 200 );
        }
    } );
}
// Init for mixes js
$( document ).ready( function() {

    saveMixes();
    updateTitle();
    mix_requirement();

} );

// AJAX Call to save Mixes
function saveMixes() {

    // Save Mixes click event
    $(document).on( 'click', '#save-mixes' , function() {
        // Ajax vars
        postVars = {
            action: 'mixes_ajax',
            save_mixes: getMixValues(),
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function (response) {
            messageHandler( response.success );
            updateTitle(); // Make sure our titles are accurate
        });
    } );
}

function getMixValues() {

    var mixes = {};

    $('.bf-repeater-container[data-type="mixes_repeater"] .bf-repeater').each( function() {

        var id = $(this).find('input[data-key="mix-id"]').val();
        var name = $(this).find('input[data-key="mix-name"]').val();
        name = name.replace(/'/g, '&rsquo;');
        var this_mix = {
            'name': name,
            'custom_classes': $(this).find('select[data-key="global"]').val(),
            'desktop_classes': $(this).find('select[data-key="desktop"]').val(),
            'mobile_classes': $(this).find('select[data-key="mobile"]').val(),
            'title_classes': $(this).find('select[data-key="title"]').val()
        };

        $.each( this_mix, function( index, value ) {
            console.log(index + ':' + value );
            if( null === value ) {
                this_mix[index] = [''];
            }
        });

        mixes[id] = this_mix;

    } );

    return mixes;

}

// Updates Titles
function updateTitle() {

    var mix_name_inputs = $('input[data-key="mix-name"]');

    $.each( mix_name_inputs, function() {
        if ( $(this).val() ){
            $(this).parents('.bf-repeater-set').find('h3').html( $(this).val() );
        }
    } );
}

// Disable mix delete
function mix_requirement() {
    var mix_required = $('input[data-required]');

    $.each( mix_required, function() {
        if ( true === $(this).data( 'required' ) ){
            $(this).parents('.bf-repeater-set').find('.del-bf-repeater').addClass( 'disabled' );
        }
    } );
}
$( document ).ready( function() {

    saveScripts();

} );

function saveScripts() {

    $(document).on( 'click', '#save-scripts' , function() {
        
        // Ajax vars
        postVars = {
            action: 'stellar_scripts',
            save_scripts: {
                'header_scripts': $("#stellar_scripts\\[header_scripts\\]").val(),
                'footer_scripts': $("#stellar_scripts\\[footer_scripts\\]").val()
            },
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function (response) {

            messageHandler( response );
        });
    });
}
$( document ).ready( function() {
    init_bf_background_preview();
} );

/** Repeater Title Background Preview **/
$.bf_background_preview = function( el ) {

    // Object Variables
    var set = $(el);
    set.backgroundData = {};
    set.backgroundPreview = set.find('.background-layer, .repeater-title-background');

    // Timeout Variables
    var timer = {};
    timer.interval = 600;
    timer.preview = function() {};

    var methods = {

        init: function() {

            set.on('bf-repeater-update', function() {
                clearTimeout( timer.preview );
                timer.preview = setTimeout( function() { methods.preview(); }, timer.interval );
            });
        },

        backgroundData: function() {
            set.backgroundData = {
                color: set.find('input[data-key="color"]').val(),
                color_bottom: set.find('input[data-key="color_bottom"]').val(),
                image_id: set.find('input[data-key="image_id"]').val(),
                image_attachment: set.find('select[data-key="image_attachment"]').val(),
                image_size: set.find('select[data-key="image_size"]').val(),
                image_repeat: set.find('select[data-key="image_repeat"]').val(),
                image_position: set.find('select[data-key="image_position"]').val(),
                gradient_angle: set.find('input[data-key="gradient_angle"]').val(),
                color_stop: set.find('input[data-key="color_stop"]').val(),
                color_bottom_stop: set.find('input[data-key="color_bottom_stop"]').val()
            };
        },

        preview: function() {

                methods.backgroundData();

                var postVars = {
                    action: 'slides_ajax',
                    formBackground: set.backgroundData
                };

                $.post(ajaxurl, postVars, function ( response ) {
                    set.backgroundPreview.attr('style', '');
                    set.backgroundPreview.attr('style', response ); // Should have a universal class for this
                    set.trigger('background-preview-update');
                });

        }

    };

    if( set.backgroundPreview.length ) {
        methods.init();
    }
};

$.fn.bf_background_preview = function() {
    return this.each(function() {
        new $.bf_background_preview(this);
    });
};

function init_bf_background_preview() {

    // Load for widgets when added and updated
    $( document ).on( 'register-repeater-set', function( e ) {
        $(e.target).bf_background_preview();
    } );

}

$( document ).ready( function() {
    init_row_background_preview();
} );

/** Row Background Preview **/
$.row_background_preview = function( el ) {

    var preview_pane = $(el);
        preview_pane.values = {};

    var methods = {

        init: function() {

            methods.preview(); // Initial Preview call

        },

        preview: function() {

            var settings = getRowValues();

            // Ajax vars
            var postVars = {
                action: 'layouts_ajax',
                preview_background: settings.data,
                security: $('#security').val()
            };

            // Send to ajax and get the response
            $.post(ajaxurl, postVars, function ( response ) {
                preview_pane.replaceWith( response );
                if ( settings.data['background-behavior'] == 'row-slider' || settings.data['container-background-behavior'] == 'row-slider' ){
                    slidebarSlider();
                }
            });
        }

    };

    methods.init();
};

$.fn.row_background_preview = function() {
    return this.each(function() {
        new $.row_background_preview(this);
    });
};

function init_row_background_preview() {

    // Load for widgets when added and updated
    $('div#background-preview').row_background_preview();

    // Trigger background preview from repeater
    $( document ).on( 'bf-repeater-sort background-preview-update bf-repeater-remove', function() {
        $('div#background-preview').row_background_preview();
    } );

    // Trigger background preview with global options
    $('#custom-classes, #background-behavior, #animation, #slideshowSpeed, #animationSpeed, #container-background-behavior, #container-animation, #container-slideshowSpeed, #container-animationSpeed, #container-classes, #container-desktop-classes, #container-mobile-classes').on( 'change', function(){
        $('div#background-preview').row_background_preview();
    });
}
( function( $ ) {
    // Variable for some backgrounds ( grid )
    var image   = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAAHnlligAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAHJJREFUeNpi+P///4EDBxiAGMgCCCAGFB5AADGCRBgYDh48CCRZIJS9vT2QBAggFBkmBiSAogxFBiCAoHogAKIKAlBUYTELAiAmEtABEECk20G6BOmuIl0CIMBQ/IEMkO0myiSSraaaBhZcbkUOs0HuBwDplz5uFJ3Z4gAAAABJRU5ErkJggg==',
    // html stuff for wpColorPicker copy of the original color-picker.js
        _before = '<a tabindex="0" class="wp-color-result" />',
        _after  = '<div class="wp-picker-holder" />',
        _wrap   = '<div class="wp-picker-container" />',
        _button = '<input type="button" class="button button-small hidden" />';

    /**
     * Overwrite Color
     * for enable support rbga
     */
    Color.fn.toString = function() {
        if ( this._alpha < 1 )
            return this.toCSS( 'rgba', this._alpha ).replace( /\s+/g, '' );

        var hex = parseInt( this._color, 10 ).toString( 16 );

        if ( this.error )
            return '';

        if ( hex.length < 6 )
            hex = ( '00000' + hex ).substr( -6 );

        return '#' + hex;
    };

    /**
     * Overwrite wpColorPicker
     */
    $.widget( 'wp.wpColorPicker', $.wp.wpColorPicker, {
        _create: function() {
            // bail early for unsupported Iris.
            if ( ! $.support.iris )
                return;

            var self = this,
                el   = self.element;

            $.extend( self.options, el.data() );

            // keep close bound so it can be attached to a body listener
            self.close = $.proxy( self.close, self );

            self.initialValue = el.val();

            // Set up HTML structure, hide things
            el.addClass( 'wp-color-picker' ).hide().wrap( _wrap );
            self.wrap            = el.parent();
            self.toggler         = $( _before ).insertBefore( el ).css( { backgroundColor : self.initialValue } ).attr( 'title', wpColorPickerL10n.pick ).attr( 'data-current', wpColorPickerL10n.current );
            self.pickerContainer = $( _after ).insertAfter( el );
            self.button          = $( _button );

            if ( self.options.defaultColor ) {
                self.button.addClass( 'wp-picker-default' ).val( wpColorPickerL10n.defaultString );
            } else {
                self.button.addClass( 'wp-picker-clear' ).val( wpColorPickerL10n.clear );
            }

            el.wrap( '<span class="wp-picker-input-wrap" />' ).after( self.button );

            el.iris( {
                target   : self.pickerContainer,
                hide     : self.options.hide,
                width    : self.options.width,
                mode     : self.options.mode,
                palettes : self.options.palettes,
                change   : function( event, ui ) {
                    if ( self.options.alpha ) {
                        self.toggler.css( { 'background-image' : 'url(' + image + ')' } ).html( '<span />' );
                        self.toggler.find( 'span' ).css( {
                            'width'                     : '100%',
                            'height'                    : '100%',
                            'position'                  : 'absolute',
                            'top'                       : 0,
                            'left'                      : 0,
                            'border-top-left-radius'    : '3px',
                            'border-bottom-left-radius' : '3px',
                            'background'                : ui.color.toString()
                        } );
                    } else {
                        self.toggler.css( { backgroundColor : ui.color.toString() } );
                    }

                    // Check for a custom cb
                    if ( $.isFunction( self.options.change ) )
                        self.options.change.call( this, event, ui );
                }
            } );

            el.val( self.initialValue );
            self._addListeners();

            if ( ! self.options.hide ) {
                self.toggler.click();
            }
        },
        _addListeners: function() {
            var self = this;

            // prevent any clicks inside this widget from leaking to the top and closing it
            self.wrap.on( 'click.wpcolorpicker', function( event ) {
                event.stopPropagation();
            } );

            self.toggler.on( 'click', function() {
                if ( self.toggler.hasClass( 'wp-picker-open' ) ) {
                    self.close();
                } else {
                    self.open();
                }
            });

            self.element.on( 'change', function( event ) {
                // Empty or Error = clear
                if ( $( this ).val() === '' || self.element.hasClass( 'iris-error' ) ) {
                    if ( self.options.alpha ) {
                        self.toggler.removeAttr( 'style' );
                        self.toggler.find( 'span' ).css( 'backgroundColor', '' );
                    } else {
                        self.toggler.css( 'backgroundColor', '' );
                    }

                    // fire clear callback if we have one
                    if ( $.isFunction( self.options.clear ) )
                        self.options.clear.call( this, event );
                }
            } );

            // open a keyboard-focused closed picker with space or enter
            self.toggler.on( 'keyup', function( event ) {
                if ( event.keyCode === 13 || event.keyCode === 32 ) {
                    event.preventDefault();
                    self.toggler.trigger( 'click' ).next().focus();
                }
            });

            self.button.on( 'click', function( event ) {
                if ( $( this ).hasClass( 'wp-picker-clear' ) ) {
                    self.element.val( '' );
                    if ( self.options.alpha ) {
                        self.toggler.removeAttr( 'style' );
                        self.toggler.find( 'span' ).css( 'backgroundColor', '' );
                    } else {
                        self.toggler.css( 'backgroundColor', '' );
                    }

                    if ( $.isFunction( self.options.clear ) )
                        self.options.clear.call( this, event );

                } else if ( $( this ).hasClass( 'wp-picker-default' ) ) {
                    self.element.val( self.options.defaultColor ).change();
                }
            });
        }
    });

    /**
     * Overwrite iris
     */
    $.widget( 'a8c.iris', $.a8c.iris, {
        _create: function() {
            this._super();

            // Global option for check is mode rbga is enabled
            this.options.alpha = this.element.data( 'alpha' ) || false;

            // Is not input disabled
            if ( ! this.element.is( ':input' ) )
                this.options.alpha = false;

            if ( typeof this.options.alpha !== 'undefined' && this.options.alpha ) {
                var self       = this,
                    el         = self.element,
                    _html      = '<div class="iris-strip iris-slider iris-alpha-slider"><div class="iris-slider-offset iris-slider-offset-alpha"></div></div>',
                    aContainer = $( _html ).appendTo( self.picker.find( '.iris-picker-inner' ) ),
                    aSlider    = aContainer.find( '.iris-slider-offset-alpha' ),
                    controls   = {
                        aContainer : aContainer,
                        aSlider    : aSlider
                    };

                if ( typeof el.data( 'custom-width' ) !== 'undefined' ) {
                    self.options.customWidth = parseInt( el.data( 'custom-width' ) ) || 0;
                } else {
                    self.options.customWidth = 100;
                }

                // Set default width for input reset
                self.options.defaultWidth = el.width();

                // Update width for input
                if ( self._color._alpha < 1 || self._color.toString().indexOf('rgb') != -1 )
                    el.width( parseInt( self.options.defaultWidth + self.options.customWidth ) );

                // Push new controls
                $.each( controls, function( k, v ) {
                    self.controls[k] = v;
                } );

                // Change size strip and add margin for sliders
                self.controls.square.css( { 'margin-right': '0' } );
                var emptyWidth   = ( self.picker.width() - self.controls.square.width() - 20 ),
                    stripsMargin = ( emptyWidth / 6 ),
                    stripsWidth  = ( ( emptyWidth / 2 ) - stripsMargin );

                $.each( [ 'aContainer', 'strip' ], function( k, v ) {
                    self.controls[v].width( stripsWidth ).css( { 'margin-left' : stripsMargin + 'px' } );
                } );

                // Add new slider
                self._initControls();

                // For updated widget
                self._change();
            }
        },
        _initControls: function() {
            this._super();

            if ( this.options.alpha ) {
                var self     = this,
                    controls = self.controls;

                controls.aSlider.slider({
                    orientation : 'vertical',
                    min         : 0,
                    max         : 100,
                    step        : 1,
                    value       : parseInt( self._color._alpha * 100 ),
                    slide       : function( event, ui ) {
                        // Update alpha value
                        self._color._alpha = parseFloat( ui.value / 100 );
                        self._change.apply( self, arguments );
                    }
                });
            }
        },
        _change: function() {
            this._super();

            var self = this,
                el   = self.element;

            if ( this.options.alpha ) {
                var	controls     = self.controls,
                    alpha        = parseInt( self._color._alpha * 100 ),
                    color        = self._color.toRgb(),
                    gradient     = [
                        'rgb(' + color.r + ',' + color.g + ',' + color.b + ') 0%',
                        'rgba(' + color.r + ',' + color.g + ',' + color.b + ', 0) 100%'
                    ],
                    defaultWidth = self.options.defaultWidth,
                    customWidth  = self.options.customWidth,
                    target       = self.picker.closest( '.wp-picker-container' ).find( '.wp-color-result' );

                // Generate background slider alpha, only for CSS3 old browser fuck!! :)
                controls.aContainer.css( { 'background' : 'linear-gradient(to bottom, ' + gradient.join( ', ' ) + '), url(' + image + ')' } );

                if ( target.hasClass( 'wp-picker-open' ) ) {
                    // Update alpha value
                    controls.aSlider.slider( 'value', alpha );

                    /**
                     * Disabled change opacity in default slider Saturation ( only is alpha enabled )
                     * and change input width for view all value
                     */
                    if ( self._color._alpha < 1 ) {
                        controls.strip.attr( 'style', controls.strip.attr( 'style' ).replace( /rgba\(([0-9]+,)(\s+)?([0-9]+,)(\s+)?([0-9]+)(,(\s+)?[0-9\.]+)\)/g, 'rgb($1$3$5)' ) );
                        el.width( parseInt( defaultWidth + customWidth ) );
                    } else {
                        el.width( defaultWidth );
                    }
                }
            }

            var reset = el.data( 'reset-alpha' ) || false;

            if ( reset ) {
                self.picker.find( '.iris-palette-container' ).on( 'click.palette', '.iris-palette', function() {
                    self._color._alpha = 1;
                    self.active        = 'external';
                    self._change();
                } );
            }
        },
        _addInputListeners: function( input ) {
            var self            = this,
                debounceTimeout = 100,
                callback        = function( event ) {
                    var color = new Color( input.val() ),
                        val   = input.val();

                    input.removeClass( 'iris-error' );
                    // we gave a bad color
                    if ( color.error ) {
                        // don't error on an empty input
                        if ( val !== '' )
                            input.addClass( 'iris-error' );
                    } else {
                        if ( color.toString() !== self._color.toString() ) {
                            // let's not do this on keyup for hex shortcodes
                            if ( ! ( event.type === 'keyup' && val.match( /^[0-9a-fA-F]{3}$/ ) ) )
                                self._setOption( 'color', color.toString() );
                        }
                    }
                };

            input.on( 'change', callback ).on( 'keyup', self._debounce( callback, debounceTimeout ) );

            // If we initialized hidden, show on first focus. The rest is up to you.
            if ( self.options.hide ) {
                input.on( 'focus', function() {
                    self.show();
                } );
            }
        }
    } );
}( jQuery ) );

/************************************************************************
* BrightFire Mod: Comment out the initialization, we do this already
************************************************************************/
// Auto Call plugin is class is color-picker
// jQuery( document ).ready( function( $ ) {
//     $( '.color-picker' ).wpColorPicker();
// } );
var $ = jQuery.noConflict();

if( typeof stellarPalettes != 'undefined' ) {

    $.wp.wpColorPicker.prototype.options = { palettes: stellarPalettes, hide: true };

}