$( document ).ready( function() {
    init_bf_background_preview();
} );

/** Repeater Title Background Preview **/
$.bf_background_preview = function( el ) {

    // Object Variables
    var set = $(el);
    set.backgroundData = {};
    set.backgroundPreview = set.find('.background-layer, .repeater-title-background');

    // Timeout Variables
    var timer = {};
    timer.interval = 600;
    timer.preview = function() {};

    var methods = {

        init: function() {

            set.on('bf-repeater-update', function() {
                clearTimeout( timer.preview );
                timer.preview = setTimeout( function() { methods.preview(); }, timer.interval );
            });
        },

        backgroundData: function() {
            set.backgroundData = {
                color: set.find('input[data-key="color"]').val(),
                color_bottom: set.find('input[data-key="color_bottom"]').val(),
                image_id: set.find('input[data-key="image_id"]').val(),
                image_attachment: set.find('select[data-key="image_attachment"]').val(),
                image_size: set.find('select[data-key="image_size"]').val(),
                image_repeat: set.find('select[data-key="image_repeat"]').val(),
                image_position: set.find('select[data-key="image_position"]').val(),
                gradient_angle: set.find('input[data-key="gradient_angle"]').val(),
                color_stop: set.find('input[data-key="color_stop"]').val(),
                color_bottom_stop: set.find('input[data-key="color_bottom_stop"]').val()
            };
        },

        preview: function() {

                methods.backgroundData();

                var postVars = {
                    action: 'slides_ajax',
                    formBackground: set.backgroundData
                };

                $.post(ajaxurl, postVars, function ( response ) {
                    set.backgroundPreview.attr('style', '');
                    set.backgroundPreview.attr('style', response ); // Should have a universal class for this
                    set.trigger('background-preview-update');
                });

        }

    };

    if( set.backgroundPreview.length ) {
        methods.init();
    }
};

$.fn.bf_background_preview = function() {
    return this.each(function() {
        new $.bf_background_preview(this);
    });
};

function init_bf_background_preview() {

    // Load for widgets when added and updated
    $( document ).on( 'register-repeater-set', function( e ) {
        $(e.target).bf_background_preview();
    } );

}