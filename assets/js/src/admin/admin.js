var $ = jQuery.noConflict();

$(document).ready(function () {

    // Utilities
    sortHandler();
    columnSlider();
    messageHandler();
    slideWidget();

});

function getRowValues() {

    var backgroundLayers = [];
    var containerBackgroundLayers = [];

        // Check for background layers
        $('#background-layers .bf-repeater').each( function() {

            var this_layer = {
                'color': $(this).find('input[data-key="color"]').val(),
                'color_bottom': $(this).find('input[data-key="color_bottom"]').val(),
                'color_stop': $(this).find('input[data-key="color_stop"]').val(),
                'color_bottom_stop': $(this).find('input[data-key="color_bottom_stop"]').val(),
                'gradient_angle': $(this).find('input[data-key="gradient_angle"]').val(),
                'featured_image': $(this).find('select[data-key="featured_image"]').val(),
                'image_id': $(this).find('input[data-key="image_id"]').val(),
                'image_attachment': $(this).find('select[data-key="image_attachment"]').val(),
                'image_size': $(this).find('select[data-key="image_size"]').val(),
                'image_repeat': $(this).find('select[data-key="image_repeat"]').val(),
                'image_position': $(this).find('select[data-key="image_position"]').val()
            };

            backgroundLayers.push( this_layer );

        } );

        $('#container-background-layers .bf-repeater').each( function() {

            var this_layer = {
                'color': $(this).find('input[data-key="color"]').val(),
                'color_bottom': $(this).find('input[data-key="color_bottom"]').val(),
                'color_stop': $(this).find('input[data-key="color_stop"]').val(),
                'color_bottom_stop': $(this).find('input[data-key="color_bottom_stop"]').val(),
                'gradient_angle': $(this).find('input[data-key="gradient_angle"]').val(),
                'featured_image': $(this).find('select[data-key="featured_image"]').val(),
                'image_id': $(this).find('input[data-key="image_id"]').val(),
                'image_attachment': $(this).find('select[data-key="image_attachment"]').val(),
                'image_size': $(this).find('select[data-key="image_size"]').val(),
                'image_repeat': $(this).find('select[data-key="image_repeat"]').val(),
                'image_position': $(this).find('select[data-key="image_position"]').val()
            };

            containerBackgroundLayers.push( this_layer );

        } );

        // All settings from the page set into an object
        settings = {
            'id': $('#id').val(),
            'data': {
                'name': $('#name').val(),
                'description': $('#description').val(),
                'left': $('.column-ranger').find('.left .number').text(),
                'right': $('.column-ranger').find('.right .number').text(),
                'background-behavior' : $('#background-behavior').val(),
                'animation' : $('#animation').val(),
                'slideshowSpeed' : $('#slideshowSpeed').val(),
                'animationSpeed' : $('#animationSpeed').val(),
                'min_height': $('#min_height').val(),
                'gutter' : $('#gutter').val(),
                'custom-classes': $('#custom-classes').val(),
                'desktop-classes': $('#desktop-classes').val(),
                'mobile-classes': $('#mobile-classes').val(),
                'background-layers': backgroundLayers,
                'container-background-behavior' : $('#container-background-behavior').val(),
                'container-animation' : $('#container-animation').val(),
                'container-slideshowSpeed' : $('#container-slideshowSpeed').val(),
                'container-animationSpeed' : $('#container-animationSpeed').val(),
                'container-classes': $('#container-classes').val(),
                'container-desktop-classes': $('#container-desktop-classes').val(),
                'container-mobile-classes': $('#container-mobile-classes').val(),
                'container-background-layers': containerBackgroundLayers,

            }
        };

        return settings;
}

function sortHandler() {

    $('.rows td').sortable({
        cursor: 'move',
        axis : 'y',
        distance: 2,
        opacity: 0.8,
        tolerance: 'pointer',
        start: function(e, ui){
            ui.placeholder.height(ui.item.height());
        },
    });

    $('.sort-items').find('tbody').sortable({
        cursor: 'move',
        axis : 'y',
        distance: 2,
        opacity: 0.8,
        tolerance: 'pointer',
        start: function(e, ui){
            ui.placeholder.height(ui.item.height());
        },
        helper: function(e, ui) {
            var children = ui.children();
            for ( var i=0; i<children.length; i++ ) {
                var selector = $(children[i]);
                selector.width( selector.width() );
            }
            return ui;
        },
        stop: function(e, ui) {
            // remove fixed widths
            ui.item.children().css('width','');
        },
        update: function(e, ui) {
            updateItemSort(e, ui);
        }
    });
}

function updateItemSort(e, ui) {
    var ids = [];
        option = ui.item.parents( 'table' ).attr( 'id' );

    ui.item.find('.spinner').addClass('is-active');

    $('#' + option ).find('tbody tr').each( function() {
        ids.push($(this).attr('id'));
    });

    sortVars = {
        'ids': ids,
        'option': option,
    };

    postVars = {
        action: 'layouts_ajax',
        sort_items: sortVars,
        security: $('#security').val(),
    };

    $.post(ajaxurl, postVars, function () {
        ui.item.find('.spinner').removeClass('is-active');
    });
}

function columnSlider(){

    $(document).on( 'input change', '.column-ranger input', function(){

        var left = $(this).val();
        right = 12-left;

        $(this).parent('.column-ranger').find('.left .number').text(left);
        $(this).parent('.column-ranger').find('.right .number').text(right);
    });
}

function messageHandler( message ) {

    // Print the object to the page
    $('#wpbody').append( message );

    // Removes Errors
    $( "#error-message" ).delay(2000).fadeOut(200,function() {
        $(this.remove());
    });

    // Removes Success
    $( "#success-message" ).delay(2000).fadeOut(200,function() {
        $(this.remove());
    });

}

function slideWidget() {

    // Toggle slide Title
    $(document).on( 'propertychange', 'select[data-key="slidebar"]', function() {
        var title = $(this).find('option:selected').text();
        $(this).parents('.bf-repeater').find('h3').text( title );
    } );

}