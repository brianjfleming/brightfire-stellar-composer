$( document ).ready( function() {

    saveScripts();

} );

function saveScripts() {

    $(document).on( 'click', '#save-scripts' , function() {
        
        // Ajax vars
        postVars = {
            action: 'stellar_scripts',
            save_scripts: {
                'header_scripts': $("#stellar_scripts\\[header_scripts\\]").val(),
                'footer_scripts': $("#stellar_scripts\\[footer_scripts\\]").val()
            },
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function (response) {

            messageHandler( response );
        });
    });
}