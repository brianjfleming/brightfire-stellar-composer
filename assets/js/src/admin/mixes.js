// Init for mixes js
$( document ).ready( function() {

    saveMixes();
    updateTitle();
    mix_requirement();

} );

// AJAX Call to save Mixes
function saveMixes() {

    // Save Mixes click event
    $(document).on( 'click', '#save-mixes' , function() {
        // Ajax vars
        postVars = {
            action: 'mixes_ajax',
            save_mixes: getMixValues(),
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function (response) {
            messageHandler( response.success );
            updateTitle(); // Make sure our titles are accurate
        });
    } );
}

function getMixValues() {

    var mixes = {};

    $('.bf-repeater-container[data-type="mixes_repeater"] .bf-repeater').each( function() {

        var id = $(this).find('input[data-key="mix-id"]').val();
        var name = $(this).find('input[data-key="mix-name"]').val();
        name = name.replace(/'/g, '&rsquo;');
        var this_mix = {
            'name': name,
            'custom_classes': $(this).find('select[data-key="global"]').val(),
            'desktop_classes': $(this).find('select[data-key="desktop"]').val(),
            'mobile_classes': $(this).find('select[data-key="mobile"]').val(),
            'title_classes': $(this).find('select[data-key="title"]').val()
        };

        $.each( this_mix, function( index, value ) {
            console.log(index + ':' + value );
            if( null === value ) {
                this_mix[index] = [''];
            }
        });

        mixes[id] = this_mix;

    } );

    return mixes;

}

// Updates Titles
function updateTitle() {

    var mix_name_inputs = $('input[data-key="mix-name"]');

    $.each( mix_name_inputs, function() {
        if ( $(this).val() ){
            $(this).parents('.bf-repeater-set').find('h3').html( $(this).val() );
        }
    } );
}

// Disable mix delete
function mix_requirement() {
    var mix_required = $('input[data-required]');

    $.each( mix_required, function() {
        if ( true === $(this).data( 'required' ) ){
            $(this).parents('.bf-repeater-set').find('.del-bf-repeater').addClass( 'disabled' );
        }
    } );
}