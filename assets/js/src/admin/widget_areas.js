

$( document ).ready( function() {

    addWidgetArea();
    toggleWidgetArea();
    saveWidgetArea();
    deleteWidgetArea();

} );

function addWidgetArea() {
    $(document).on( 'click', '.add-widget-area' , function( event ) {

        event.preventDefault();

        var option = $(this).data( 'option' );

        var placement = $('#' + option ).find('tbody');

        // Ajax vars
        postVars = {
            action: 'layouts_ajax',
            add_widget_area: option,
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function ( response ) {

            var newRow = $( response );

            placement.prepend( newRow );
        });

    });
}

function saveWidgetArea() {
    $(document).on( 'click', '.save-widget-area' , function() {

        $(this).parents('span').siblings('.spinner').addClass('is-active');

        var option = $(this).parents('table').attr('id');
        id = $(this).parents('tr').attr('id');
        name = $('#' + id).find('input.name').val();
        description = $('#' + id).find('input.description').val();
        allIds = [];

        $('#' + option ).find('tbody tr').each( function() {
            allIds.push( $(this).attr('id') );
        });

        if ( name === '') {
            return false;
        }

        widgetArea = {
            'sort': allIds,
            'option': option,
            'id': id,
            'data' : {
                'name': name,
                'description': description
            }
        };

        // Ajax vars
        postVars = {
            action: 'layouts_ajax',
            save_widget_area: widgetArea,
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function ( response ) {

            var row = $('#' + id );

            row.find('p.name').text( name );
            row.find('p.description').text( description );

            row.find('.editing-widget-area').hide();
            row.find('.view-widget-area').show();

           row.find('.spinner').removeClass('is-active');

        });

    });
}

function toggleWidgetArea() {

    $(document).on('click', '.cancel-widget-area', function() {

        var row = $(this).parents('tr');

        row.find('.editing-widget-area').hide();
        row.find('.view-widget-area').show();
    });

    $(document).on( 'click', '.edit-widget-area', function(){

        var row = $(this).parents('tr');

        row.find('.editing-widget-area').show();
        row.find('.view-widget-area').hide();
    });
}

function deleteWidgetArea() {

    $(document).on( 'click', '.delete-widget-area', function(){

        $(this).parents('span').siblings('.spinner').addClass('is-active');

        var option = $(this).parents('table').attr('id');
        tablerow = $(this).parents('tr');
        id = tablerow.attr('id');
        name = $(this).parents('tr').find('p.name').text();

        deleteInfo = {
            'id': id,
            'option': option,
            'name': name,
        };

        // Ajax vars
        postVars = {
            action: 'layouts_ajax',
            delete_widget_area: deleteInfo,
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function ( response ) {
            tablerow.remove();
        });
    });
}