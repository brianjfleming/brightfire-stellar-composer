
$( document ).ready( function() {

    // Rows
    saveRow();
    deleteRow();
    toggleSlideOptions();

} );

function saveRow() {

    $(document).on( 'click', '#save-row' , function() {
        // Ajax vars
        postVars = {
            action: 'layouts_ajax',
            save_row: getRowValues(),
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function (response) {

            $('h1').text( response.title );
            history.pushState(null, null, response.history );
            messageHandler( response.success );
        });
    });
}

function deleteRow() {
    $(document).on( 'click', '#delete-row' , function() {

        var deleteInfo = {
            id : $('#id').val(),
            name : $('#name').val(),
            usage : $('#usage').val()
        };

        // Ajax vars
        postVars = {
            action: 'layouts_ajax',
            delete_row: deleteInfo,
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function ( response ) {
            location.href = response;
        });

    });
}

function toggleSlideOptions(){
    $(document).on( 'change', '#background-behavior', function(){
        var element = $('.slide-options');

        if ($(this).val() == 'layered'){
            element.hide( 200 );
        } else if ($(this).val() == 'row-slider'){
            element.show( 200 );
        }
    } );

    $(document).on( 'change', '#container-background-behavior', function(){
        var element = $('.container-slide-options');

        if ($(this).val() == 'layered'){
            element.hide( 200 );
        } else if ($(this).val() == 'row-slider'){
            element.show( 200 );
        }
    } );
}