
$( document ).ready( function() {

    stellarThemeHandlers();

} );

function stellarThemeHandlers() {

    $(document).on( 'click', '.stellar-theme-activate', function() {

        var postVars = { action: 'theme_ajax', set_theme: $(this).attr('data-theme'), security: $('#security').val() };
        var theme = $(this).attr('data-theme');

        // Turn on spinner
        $(this).siblings('.spinner').addClass('is-active');

        $.post( ajaxurl, postVars, function ( response ){

            restoreThemeDefaults();

            activateTheme( theme );

        });

    } );

    $(document).on( 'click', '.stellar-theme-deactivate', function() {

        var postVars = { action: 'theme_ajax', reset_theme: true, security: $('#security').val() };

        // Turn on spinner
        $(this).siblings('.spinner').addClass('is-active');

        $.post( ajaxurl, postVars, function ( response ){

            restoreThemeDefaults();

        });

    } );

    $(document).on( 'click', '.stellar-theme-overlay .close', function() {
        $('.stellar-theme-overlay').fadeOut(100);
    } );

    $(document).on( 'click', '.theme-backdrop', function() {
        $('.stellar-theme-overlay').fadeOut(100);
    } );

    $(document).on( 'click', '.stellar-theme', function(e) {

        if(  $(e.target).hasClass('stellar-theme-activate') ) {
            return false;
        }

        if(  $(e.target).hasClass('stellar-theme-deactivate') ) {
            return true;
        }

        var theme_info = $.parseJSON( $(this).attr('data-info') );

        // Set up markup
        var license_markup = '';
        var version_markup = 'Version: ' + theme_info.ver;
        var name_markup = theme_info.name + '<span class="theme-version">' + version_markup + '</span>';
        var author_markup = 'by <a href="' + theme_info.author_uri + '" target="_blank">' + theme_info.author + '</a>';
        var description_markup = '<p>' + theme_info.desc + '</p>';
        var inactive_action_markup = '<span class="spinner"></span> <a class="button button-primary stellar-theme-activate" href="javascript: void(0);" data-theme="' + theme_info.id + '">Activate</a>';
        var active_action_markup = '<span class="spinner"></span> <a class="button button-primary stellar-theme-deactivate" href="javascript: void(0);">Dectivate</a>';


        if( $(this).hasClass('active') ) {
            $('.stellar-theme-overlay .current-label').show();
            $('.stellar-theme-overlay .active-actions').show();
            $('.stellar-theme-overlay .inactive-actions').hide();
        } else {
            $('.stellar-theme-overlay .current-label').hide();
            $('.stellar-theme-overlay .active-actions').hide();
            $('.stellar-theme-overlay .inactive-actions').show();
        }

        if( theme_info.hasOwnProperty('license') ) {
            license_markup += 'Licensed under ' + theme_info.license;
        }

        if( theme_info.hasOwnProperty('license_uri') ) {
            license_markup += ' | <a href="' + theme_info.license_uri + '" target="_blank">Learn More</a>';
        }

        $('.theme-name').html(name_markup);
        $('.theme-author').html(author_markup);
        $('.theme-description').html(description_markup);
        $('.screenshot img').attr('src',theme_info.screenshot);
        $('.theme-license').html(license_markup);
        $('.stellar-theme-overlay .inactive-actions').html(inactive_action_markup);
        $('.stellar-theme-overlay .active-actions').html(active_action_markup);

        $('.stellar-theme-overlay').fadeIn(100);
    } );

}

function activateTheme( theme ) {
    $('.stellar-theme.' + theme).addClass('active').find('.stellar-theme-name, .stellar-theme-actions').addClass('active');
    $('.stellar-theme.' + theme + ' .active-actions').show();
    $('.stellar-theme.' + theme + ' .inactive-actions').hide();
    $('.stellar-theme-overlay').fadeOut(100);
}

function restoreThemeDefaults() {
    // Restores class defaults
    $('.stellar-theme-name, .stellar-theme, .stellar-theme-actions').removeClass('active');
    $('.inactive-actions').show();
    $('.active-actions').hide();
    $('.spinner').removeClass('is-active');
}