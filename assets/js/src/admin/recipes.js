
$( document ).ready( function() {

    saveRecipe();
    loadRecipe();
    deleteRecipe();
    exportRecipe();
    importRecipe();

} );

function saveRecipe(){

    // Save Recipe button click
    $('#save-recipe').click(function () {

        // Get the recipe name
        var recipe = $( '#new-recipe-name' );
        var recipeName = recipe.val();
        var scope = '';

        // Error for no name
        if ( recipeName === '' ) {
            recipe.css( 'border-color', 'red' );
            alert( 'You must name the recipe.' );
            return;
        }

        // Checking scope of where this should be saved
        if ( $('#network-recipe').is(':checked') ) {
            scope = 'network';
        } else {
            scope = 'site';
        }

        // Turn on spinner
        $("#saving.spinner").addClass('is-active');

        // Set up our JSON object to send
        var postVars = { action: 'recipe_ajax', save: recipeName, scope: scope, security: $('#security').val() };

        // Send off the ajax
        $.post( ajaxurl, postVars, function ( response ){

            // Reset the new values for our selectors
            $('#load-recipe-selector').replaceWith(response.load);
            $('#delete-recipe-selector').replaceWith(response.delete);
            $('#export-recipe-selector').replaceWith(response.export);
            $('#network-recipe').prop('checked', false);
            recipe.val('');

            // Turn off spinner
            $( '#saving.spinner' ).removeClass('is-active');

            messageHandler( response.message );

        });
    });
}

function exportRecipe() {

    $(document).on( 'submit', '#export-recipe-form', function() {

        // Need to make and set scope (network or site)
        var selected = $( '#export-recipe-selector').find( 'option:selected:not([disabled])' );


        if ( selected.length > 0 ) {
            var scope = selected.closest("optgroup").data('scope');
            $('input#export-recipe-scope').val(scope);
            return true;
        } else {
            alert('Please select a recipe to export');
            return false;
        }

    });

}

function importRecipe() {

    $(document).on( 'change', '#import-recipe-file', function() {

        if( $(this).val() !== '' ) {

            $('#importing').addClass('is-active');
            $('#import-recipe-form').submit();

        }

    });
}

function loadRecipe() {

    // Load Recipe button clicked
    $('#load-recipe').click(function () {

        // Get the selected option
        var selected = $( '#load-recipe-selector').find( 'option:selected' );
        var loadRecipe = selected.val();

        // Do we want images
        if ( $('#image-upload').is(':checked') ) {
            import_images = 'true';
        } else {
            import_images = '';
        }
        
        if ( loadRecipe ) {
            // Find the scope of the selected recipe
            var scope = selected.closest("optgroup").data('scope');

            var postVars = { action: 'recipe_ajax', load: loadRecipe, scope: scope, import_images: import_images, security: $('#security').val() };

            $("#loading.spinner").addClass('is-active');

            $.post( ajaxurl, postVars, function ( response ){
                location.href = response;
            });
        }
    });
}

function deleteRecipe() {
    $('#delete-site-recipe').click(function () {
        var selected = $( '#delete-recipe-selector' ).find( 'option:selected' );
        scope = selected.closest("optgroup").data('scope');
        deleteNetwork = '';

        if ( scope === 'network' ) {
            deleteNetwork = prompt( 'YOU ARE DELETING A NETWORK LEVEL RECIPE!\nIf you really want to do this type "DELETE" to continue:', '' );
        } else if ( scope === 'site' ) {
            deleteNetwork = 'DELETE';
        }

        var deleteRecipe = selected.val();

        var postVars = { action: 'recipe_ajax', delete: deleteRecipe, scope: scope, security: $('#security').val() };

        if ( deleteNetwork === 'DELETE' ) {
            $("#deleting.spinner").addClass('is-active');

            $.post( ajaxurl, postVars, function ( response ){
                
                // Replace our selectors with the new population
                $('#load-recipe-selector').replaceWith(response.load);
                $('#delete-recipe-selector').replaceWith(response.delete);
                $('#export-recipe-selector').replaceWith(response.export);

                // Turn off the spinner
                $( '#deleting.spinner' ).removeClass('is-active');

                messageHandler( response.message );

            });
        }
    });
}


