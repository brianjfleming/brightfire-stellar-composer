var $ = jQuery.noConflict();

function deleteStellarCSS(){

    var postVars = {
        action: 'delete_stellar_css',
        delete: 'stellarCSS',
        security: resetCSS.security
    };

    $.post( resetCSS.ajaxurl, postVars, function (){

        location.reload();
    });
}