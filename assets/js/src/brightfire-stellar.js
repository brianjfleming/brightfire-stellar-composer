/*! BrightFire Stellar - v0.0.1
 * https://www.brightfire.com
 * Copyright (c) 2016; * Licensed GPLv2+ */
var $ = jQuery.noConflict();


/**
 *
 * STICKY ROWS
 *
 */

// Var Defaults

/**
 * Set up all our vars on window.load
 */
$(window).load(function () {
    isMobile();

    customizerExec();
});

$(window).resize(function () {
    isMobile();

    matchWidgetHeight();
});

function customizerExec() {
    reviewsExpand();
    reviewSlider();
    slidebarSlider();
    matchWidgetHeight();
}

function isMobile() {

    var breakpoint = $('body').attr('data-breakpoint');
    var width = $(window).width();

    if (width < breakpoint) {
        $('body').addClass('mobile');
        return true;
    } else {
        $('body').removeClass('mobile');
        return false;
    }
}

function setupFixedRows() {

    var body = $('body').not('.wp-admin').not('.mobile');
    var fixed_row_element = body.find('*[data-row-fixed="true"]');

    if (fixed_row_element.length) { // if .row-fixed exists
        eleOffset = fixed_row_element.offset();
        eleTop = eleOffset.top;
        eleHeight = fixed_row_element.outerHeight();
        menuBg = fixed_row_element.css('background-color');

        if ($('#wpadminbar').length) { // if #wpadminbar exists
            wpHeight = $('#wpadminbar').height();
        } else {
            wpHeight = 0;
        }

        if (!$('.row-fixed-placeholder').length) {
            $(fixed_row_element).after('<div class="row-fixed-placeholder" style="display: none; height: ' + eleHeight + 'px; background-color: ' + menuBg + ';"></div>');
        }

        var placeholder = $('.row-fixed-placeholder');

        $(document).scroll(function () {

            var scrollOffset = $(document).scrollTop();

            if ($('#wpadminbar').length) { // if #wpadminbar exists
                wpBottom = ( $('#wpadminbar').offset().top + $('#wpadminbar').height() ) - scrollOffset;
            } else {
                wpBottom = 0;
            }

            if (wpBottom <= 0) {
                wpBottom = 0; // If it's less than 0, make it 0
            }

            if (scrollOffset > ( eleTop - wpHeight )) {

                // Calculate relative height

                placeholder.show(); // Put in the placeholder so we don't lose vertical alignment
                fixed_row_element.css('position', 'fixed').css('width', '100%').css('top', wpBottom + 'px').css('left', 0).addClass('traveling');
            } else {
                placeholder.hide();
                fixed_row_element.css({'position': 'relative', 'width': 'auto', 'top': '0'}).removeClass('traveling');
            }

        });

        // Switch for wpadminbar responsive styles (height)
        $(window).resize(function () {

            eleHeight = fixed_row_element.outerHeight(); // If we resize, make sure our height didn't change
            placeholder.css('height', eleHeight + 'px'); // on resize, make sure our placeholder gets height changes

            if (fixed_row_element.hasClass('traveling')) { // if our header is sticky, compensate for possible change in wpHeight
                fixed_row_element.css('top', wpBottom + 'px');
            }
        });
    }
}

function slidebarSlider() {

    var slidebar = $('.brightfire-slidebar');

    $.each(slidebar, function () {

        var height = $(this).data('height');
        var transition = $(this).data('animation');
        var slideshowSpeed = $(this).data('slideshowspeed');
        var animationSpeed = $(this).data('animationspeed');
        var directionNav = $(this).data('directionnav');

        slideshowSpeed = slideshowSpeed * 1000;
        animationSpeed = animationSpeed * 1000;

        var animation = transition;
        var fadeOutDuringTransition = false;

        if (transition == 'crossfade') {
            fadeOutDuringTransition = true;
            animation = 'fade';
        } else if (transition == 'slide') {
            fadeOutDuringTransition = true;
        }

        // Determine height of our slider
        if (height) {
            $(this).height(height + 'px');
        } else {
            var allHeights = [];

            // This uses the jquery.actual.js plugin
            $.each($(this).find('li'), function () {
                allHeights.push($(this).actual('height'));
            });

            // Grab the largest number from our heights
            var tallest = Math.max.apply(Math, allHeights);

            // Set our slider height and show it
            $(this).height(tallest + 'px');
        }

        // Initialize our slider
        $(this).flexslider({
                namespace: 'bf-slider-',
                pauseOnHover: true,
                itemWidth: '100%',
                controlNav: false,
                directionNav: directionNav,
                prevText: "",
                nextText: "",
                fadeFirstSlide: true,
                animation: animation,
                fadeOutDuringTransition: fadeOutDuringTransition,
                slideshowSpeed: slideshowSpeed,
                animationSpeed: animationSpeed
            }
        ).animate({opacity: 1});
    });
}

function matchWidgetHeight() {

    // Find all our match elements
    var body = $('body').not('.wp-admin').not('.mobile'); // Don't do this on mobile
    var columns = body.find('.column');

    // Reset all '.matched' element heights
    $('.matched').height('auto').removeClass('matched');

    // Loop Through Each of our columns
    $.each(columns, function () {

        var max_height = 0; // Reset our max-height for each columng
        var match_elements = $(this).find('.match-widget-height'); // All elements to match in this column

        // Loop through all '.match-height' widgets
        $.each(match_elements, function () {
            var ele_height = $(this).actual('height');

            if (ele_height > max_height) {
                max_height = ele_height;
            }
        });

        match_elements.height(max_height).addClass('matched'); // Set all to max height

    });

}

function reviewsExpand() {
    $(document).on('click','.review-expand',function() {

        var target = $(this).attr('data-show');
        $(target).slideDown();


        $(this).removeClass('review-expand').addClass('review-hide');
        return true;
    });
    $(document).on('click','.review-hide',function() {

        var target = $(this).attr('data-show');
        $(target).slideUp();


        $(this).removeClass('review-hide').addClass('review-expand');
        return true;
    });
}

function reviewSlider() {
    $('.review-slider').flexslider(
        {
            'namespace': 'flex-',
            'pauseOnHover': true,
            'animation': 'slide',
            'itemWidth': '100%',
            'slideshowSpeed': '5500',
            'controlNav': false,
            'directionNav': false,
            'prevText': "",
            'nextText': "",
            'start': function(slider){
                // Stops flash of white when first loaded
                // https://github.com/woothemes/FlexSlider/issues/848#issuecomment-42573918
                slider.removeClass('loading');
                // Display all hidden slides after window loads
                $('.review-slider .slides li.hide-initially').fadeIn('slow');

            }
        }
    );
}