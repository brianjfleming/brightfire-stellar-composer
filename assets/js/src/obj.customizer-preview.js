/**
 * Customizer Previewer
 */
( function ( wp, $ ) {
    "use strict";

    // Bail if the customizer isn't initialized
    if ( ! wp || ! wp.customize ) {
        return;
    }

    var api = wp.customize, wpPreview;
    var optionsRow = {
        'custom-classes': '',
        'desktop-classes': 'd-',
        'mobile-classes': 'm-',
    };

    var optionsContainer = {
        'container-classes': '',
        'container-desktop-classes': 'd-',
        'container-mobile-classes': 'm-'
    };

    // Custom Customizer Preview class (attached to the Customize API)
    api.stellarPreview = {

        /**
         * Init Settings
         */
        init: function () {
            var self = this; // Store a reference to "this"

            // When the previewer is active, the "active" event has been triggered (on load)
            // This also triggers when a widget or navigation click loads or refreshes a page
            this.preview.bind( 'active', function() {

                // Update our body Settings
                self.bodySettings( self );

                // Keep any previewed SCSS changes
                self.getDirtySCSS();

                // addRowClasses
                $.each( JSON.parse(row_ids), function( index, id ) {
                    self.addRowClasses( id );
                } );

                self.bindPartials();

            } );

            // Callback to place styles
            self.recompileCallback( self );

            // Set up Row Short cut ( alt+click )
            self.rowShortcut( self );

            // Handler for body class loading assignments
            self.bodyLoadingClass( self );

            // Binds row inputs
            self.rowPreviewBuilder( JSON.parse( row_ids ) );

        },

        bindPartials: function() {

            wp.customize.selectiveRefresh.partial.bind( 'add', function( addedPartial ) {
                console.log(addedPartial);
                customizerExec();
            } );

            // Run a handful of JS scripts we need to instantiate different widget functionality
            wp.customize.selectiveRefresh.bind( 'partial-content-rendered', function( widgetPartial ) {
                customizerExec();
            } );

        },

        /**
         * Sets up binds for Stellar -> Body Settings setting/control pairs
         * @param self
         */
        bodySettings: function( self ) {

                wp.customize('body-background-color-top', function (value) {
                    value.bind(function () {
                        self.backgroundPreview('body');
                    });
                });

                wp.customize('body-background-color-bottom', function (value) {
                    value.bind(function () {
                        self.backgroundPreview('body');
                    });
                });

                wp.customize('body-background-image', function (value) {
                    value.bind(function () {
                        self.backgroundPreview('body');
                    });
                });

                wp.customize('body-background-repeat', function (value) {

                    value.bind(function (newval) {
                        $('body').css('background-repeat', newval);
                    });

                });

                wp.customize('body-background-attachment', function (value) {

                    value.bind(function (newval) {
                        $('body').css('background-attachment', newval + ',scroll' );

                    });

                });

                wp.customize('body-background-size', function (value) {

                    value.bind(function (newval) {
                        $('body').css('background-size', newval);
                    });

                });

                wp.customize('body-background-position', function (value) {

                    value.bind(function (newval) {
                        $('body').css('background-position', newval);
                    });

                });
        },

        /**
         * Replaces our <style id="stellar-preview"> with our override preview styles
         * @param self
         */
        recompileCallback: function( self ) {

            self.preview.bind( 'update-scss-data', function( response ) {

                // Put Our Styles Where They Belong and send back the reset
                $('#stellar-preview').replaceWith( '<style id="stellar-preview">' + response + '</style>' );

            } );

            self.preview.bind( 'update-fonts-data', function( response ) {

                // Put Our Styles Where They Belong and send back the reset
                $('#stellar-google-fonts').replaceWith( response );

            } );
        },

        /**
         * Bind Events to add and remove loading class
         * @param self
         */
        bodyLoadingClass: function( self ) {

            self.preview.bind( 'add-loading-class', function() {

                // Put Our Styles Where They Belong and send back the reset
                $('body').addClass('wp-customizer-unloading');

            } );

            self.preview.bind( 'remove-loading-class', function() {

                // Put Our Styles Where They Belong and send back the reset
                $('body').removeClass('wp-customizer-unloading');

            } );

        },

        /**
         * Bind events for Alt+Click Shortcut
         * @param self
         */
        rowShortcut: function( self ) {

            $( document ).on( 'click', '.row', function( event ) {

                if (event.altKey) {
                    var rowID = $(this).attr('id');
                    var sectionID = 'bf_stellar_row_' + rowID;
                    self.preview.send('focus-customizer-section', sectionID);
                }

            } );

        },

        /**
         * Builds background previews
         * @param element
         * @param id
         */
        backgroundPreview: function (element, id) {
            var top_color, bottom_color, image_id, attachment, size, position, repeat, postVars;

            if ( 'body' == element ) {

                top_color = wp.customize('body-background-color-top').get();
                bottom_color = wp.customize('body-background-color-bottom').get();
                image_id = wp.customize('body-background-image').get();
                attachment = wp.customize('body-background-attachment').get();
                size = wp.customize('body-background-size').get();
                position = wp.customize('body-background-position').get();
                repeat = wp.customize('body-background-repeat').get();

            } else {

                top_color = wp.customize('stellar_layout[rows][' + id + '][background][color]').get();
                bottom_color = wp.customize('stellar_layout[rows][' + id + '][background][color_bottom]').get();
                image_id = wp.customize('stellar_layout[rows][' + id + '][background][image][id]').get();
                attachment = wp.customize('stellar_layout[rows][' + id + '][background][image][attachment]').get();
                size = wp.customize('stellar_layout[rows][' + id + '][background][image][size]').get();
                position = wp.customize('stellar_layout[rows][' + id + '][background][image][position]').get();
                repeat = wp.customize('stellar_layout[rows][' + id + '][background][image][repeat]').get();
            }

            if (!image_id) {

                if (top_color && bottom_color) {
                    $(element).css('background-image', 'linear-gradient(to bottom, ' + top_color + ' 0%, ' + bottom_color + ' 100%)');
                } else {
                    $(element).css('background-color', top_color);
                    $(element).css('background-image', 'none');
                }

            } else if(image_id) {

                postVars = {
                    action: 'customizer_ajax',
                    imageID: image_id,
                };

                if (top_color && bottom_color) {
                    $.post( ajaxurl, postVars, function ( response ) {
                        $(element).css('background-image', 'url(' + response + '), ' + 'linear-gradient(to bottom, ' + top_color + ' 0%, ' + bottom_color + ' 100%)');
                    });
                } else {
                    $(element).css('background-color', top_color);
                    $.post( ajaxurl, postVars, function ( response ) {
                        $(element).css('background-image', 'url(' + response + ')');
                    });
                }

                $(element).css({
                    'background-attachment': attachment + ',scroll',
                    'background-position': position,
                    'background-size': size,
                    'background-repeat': repeat
                });
            }
        },

        addRowClasses: function( id ) {

            var row               = '#' + id;
            var container         = row + ' > .container';

            // Reset row and container
            $( row ).removeClass().addClass( 'row' );
            $( container ).removeClass().addClass( 'container');

            // Add back settings for min_height and gutter
            $( row ).addClass( wp.customize('stellar_layout[rows][' + id + '][gutter]').get() );
            $( container ).addClass( wp.customize('stellar_layout[rows][' + id + '][min_height]').get() );

            // Add back in custom classes for row
            $.each( optionsRow, function( option, prefix ) {
                var classes = wp.customize('stellar_layout[rows][' + id + '][' + option + ']').get();
                
                $.each( classes, function( key, theClass ) {
                    if ( '' !== theClass ){
                        $( row ).addClass( prefix + theClass );
                    }
                });
            });

            // Add back in custom classes for container
            $.each( optionsContainer, function( option, prefix ) {
                var classes = wp.customize('stellar_layout[rows][' + id + '][' + option + ']').get();

                $.each( classes, function( key, theClass ) {
                    if ( '' !== theClass ){
                        $( container ).addClass( prefix + theClass );
                    }
                });
            });
        },

        /**
         * Build binds for all our row setting/control pairs
         * called by PHP to provide ids
         * @param ids
         */
        rowPreviewBuilder: function( ids ) {
            var self = this;
            var previewOptions = $.extend({}, optionsRow, optionsContainer );

            $.each( ids, function (index, id) {

                $.each( ['min_height', 'gutter'], function( index, value  ){
                    wp.customize('stellar_layout[rows][' + id + '][' + value + ']', function (value) {
                        value.bind(function () {
                            self.addRowClasses( id );
                        });
                    });
                });

                $.each( previewOptions, function( key, option ){
                    wp.customize('stellar_layout[rows][' + id + '][' + key + ']', function (value) {
                        value.bind(function () {
                            self.addRowClasses( id );
                        });
                    });
                });

            });
        },

        getDirtySCSS: function() {

            this.preview.send('request-dirty-scss');

        }
    };

    /**         BIND LISTENERS FOR STELLAR -> BODY SETTINGS        **/
    /** ----------------------------------------------------------- */




    /**
     * Extend the wp.customize object with our own bind events and functions
     */
    wpPreview = api.Preview;
    api.Preview = wpPreview.extend( {
        initialize: function( params, options ) {
            // Store a reference to the Preview
            api.stellarPreview.preview = this;

            // Call the old Preview's initialize function
            wpPreview.prototype.initialize.call( this, params, options );
        }
    } );

    /**
     * Initialize our customize preview
     */
    $( function () {
        // Initialize our Preview
        api.stellarPreview.init();
    } );

} )( window.wp, jQuery );