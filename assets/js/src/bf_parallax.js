$(window).load(function() {

    // parallax rows
    $('div.stellar-row-parallax').bf_parallax();

});

/**
 * Parallax Object
 * @param el
 */
$.bf_parallax = function( el ) {

    var viewport = $(el);
        viewport.layers = viewport.find("div[data-type='parallax']");

    var methods = {

        init: function() {

            viewport.height = viewport.outerHeight();
            viewport.offsetVertical = viewport.offset().top;

            $(window).on('scroll', function() {
                methods.scrollLayers();
            });

            $(window).on('resize', function() {

                methods.resizeAdjust();

            });

            methods.scrollLayers();

        },

        scrollLayers: function() {

            var window_scroll = $(window).scrollTop();
            var relative_scroll = window_scroll - viewport.offsetVertical;

            if( relative_scroll >= 0 ) {

                viewport.layers.each( function() {
                    var layer = $(this);
                    var depth = layer.attr('data-depth');
                    var movement = ( ( relative_scroll ) * depth );
                    layer.css({"transform": "translate3d(0px, " + movement + "px, 0px)"});
                } );

            } else {
                viewport.layers.css({"transform": "translate3d(0px, 0px, 0px)"});
            }

        },

        resizeAdjust: function() {
            viewport.height = viewport.outerHeight();
            viewport.offsetVertical = viewport.offset().top;
        }

    };

    methods.init(); // Fire, baby

};

/**
 * Parallax init crawler
 */
$.fn.bf_parallax = function() {
    return this.each(function() {
        new $.bf_parallax(this);  // Instantiate a new object for each output range slider
    });
};