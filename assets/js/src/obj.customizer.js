/**
 * Customizer Communicator
 */
( function ( exports, $ ) {
    "use strict";

    var api = wp.customize, wpPreviewer;

    var themeSettingIDS = [
        'header-font-family',
        'base-font-family',
        'accent-font-family',
        'primary-color',
        'secondary-color',
        'tertiary-color',
        'accent-color',
        'base-font-size',
        'font-ratio',
        'base-border-radius',
        'button-border-radius',
        'form-border-radius',
        'max-width',
        'gutter',
        'breakpoint',
        'custom1-color',
        'custom2-color',
    ];

    var dirtySCSS = false;

    // Custom Customizer Previewer class (attached to the Customize API)
    api.stellarPreviewer = {

        /**
         * Init
         */
        init: function () {
            var self = this; // Store a reference to "this"

            this.bindThemeSettings( self );

            this.injectRecompileButton( self );

            this.focusCustomizerSection( self );

            this.requestDirtySCSS();

            this.toggleSlidebars();

            /** TODO: panel listener for focus and blur on theme settings **/

        },

        /**
         * Bind Event for Alt+Click to focus row section
         * @param self
         */
        focusCustomizerSection: function( self ) {
            this.preview.bind( 'focus-customizer-section', function( sectionID ) {

                // Focus the section ID we receive
                wp.customize.section( sectionID ).focus();

            } );
        },

        /**
         * Injects Recompile Button and binds events
         * @param self
         */
        injectRecompileButton: function ( self ) {

            // Inject Our Button
            // $('#customize-header-actions').prepend(' <a href="javascript: void(0);" name="recompile_scss" id="recompile_scss" class="button button-primary" disabled="" title="Recompile SCSS" style="margin-left: 12px;"><i class="fa fa-refresh" aria-hidden="true"></i></a> ');

            // Bind Click
            $(document).on( 'click', '#recompile_scss', function() {
                self.recompile( self );
            } );

        },

        /**
         * Calls our recompile
         * @param self
         */
        recompile: function( self ) {

            var getValues = {};

            $.each( themeSettingIDS, function( index, id ){
                getValues[id] = wp.customize(id).get();
            });

            var postVars = {
                action: 'customizer_ajax',
                build: getValues
            };

            self.preview.send( 'add-loading-class' );
            
            // Send to ajax and get the response
           $.post(ajaxurl, postVars, function ( response ) {

               // Send it to our main page
                self.preview.send( 'update-scss-data', response.css ); // Trigger action with param
                self.preview.send( 'update-fonts-data', response.fonts );

                dirtySCSS = response.css;
                self.recompileButtonState( false );
                self.preview.send( 'remove-loading-class' );

           });


        },

        /**
         * Handle the request for DirtySCSS
         *
         * Global object variable so the CSS stays with us on navigation change
         */
        requestDirtySCSS: function () {
            var self = this;

            this.preview.bind( 'request-dirty-scss', function() {

                if( dirtySCSS ) {
                    self.preview.send('update-scss-data', dirtySCSS );
                }

            } );
        },

        /**
         * Binds our theme settings to enable recomp button
         * @param self
         */
        bindThemeSettings: function ( self ) {

            $.each(themeSettingIDS, function( index, id ){
                wp.customize( id, function( value ) {
                    value.bind( function() {
                        self.recompileButtonState( true );
                    } );

                } );
            });
        },

        /**
         * Helper Method for activating/disabling recomp button
         * @param state
         */
        recompileButtonState: function ( state ) {
            // Disable our Recompile Button
            if( true === state ) {
                $('#recompile_scss').removeAttr('disabled');
            } else {
                $('#recompile_scss').attr('disabled', 'disabled');
            }
        },

        /**
         * Show and hide our Slidebars based on selection
         */
        toggleSlidebars: function() {

            $(document).on( 'change', '[data-key="slidebar"]' , function() {

                var newValue = $(this).val(); // New active widget area
                var oldValue = $(this).attr('data-current-slidebar'); // Last active widget area
                var selectedValues = []; // Container for all active widget areas

                // Sets our last active widget area to the new value for our next change event
                $(this).attr('data-current-slidebar', newValue);

                // Gets all active widget areas and stores in array
                $('[data-key="slidebar"]').each( function() {
                    selectedValues.push( $( this ).val() );
                } );

                // Check if we need to deactivate our last active widget area
                if( oldValue && $.inArray( oldValue, selectedValues ) == -1 ) {
                    wp.customize.section('sidebar-widgets-' + oldValue).deactivate();
                }

                // If inactive, activate our new widget area
                wp.customize.section( 'sidebar-widgets-' + newValue ).activate();
            });
        }
    };






    /**
     * Extend the wp.customize object with our own bind events and functions
     */
    wpPreviewer = api.Previewer;
    api.Previewer = wpPreviewer.extend( {
        initialize: function( params, options ) {
            // Store a reference to the Previewer
            api.stellarPreviewer.preview = this;

            // Call the old Previewer's initialize function
            wpPreviewer.prototype.initialize.call( this, params, options );
        }
    } );

    /**
     * Initialize our Previewer JS
     */
    $( function() {
        // Initialize our Previewer
        api.stellarPreviewer.init();
    } );

} )( wp, jQuery );