<?php

/**
 * BrightFire Stellar functions and definitions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * @package BrightFire Stellar
 * @since 0.1.0
 */

// BUST CACHE EVERY PAGE LOAD ON DEV ENVIRONMENT
if ( defined( 'BF_ENV' ) && BF_ENV == 'DEV' ) {
	define( 'BF_STELLAR_VERSION',  time() );
} else {
	define( 'BF_STELLAR_VERSION', '1.8' );
}

define( 'BF_STELLAR_URL',          get_stylesheet_directory_uri() );
define( 'BF_STELLAR_TEMPLATE_URL', get_template_directory_uri() );
define( 'BF_STELLAR_PATH',         get_template_directory() . '/' );
define( 'BF_STELLAR_INC',          BF_STELLAR_PATH . 'includes/' );
define( 'BF_STELLAR_LAYOUTS_CAPABILITY', 'edit_theme_options' );

// Required
require_once( BF_STELLAR_INC . 'functions/utilities.php' );
require_once( BF_STELLAR_INC . 'functions/ready_classes.php' );
require_once( BF_STELLAR_INC . 'functions/core.php' );
\BrightFire\Theme\Stellar\child_theme_functions();

// Shortcodes
require_once( BF_STELLAR_INC . 'functions/shortcodes-bucket.php' );
require_once( BF_STELLAR_INC . 'functions/shortcodes-reviews.php' );

// Widgets
require_once( BF_STELLAR_INC . 'functions/class.widget-bucket.php' );
require_once( BF_STELLAR_INC . 'functions/class.widget-classes.php' );
require_once( BF_STELLAR_INC . 'functions/class.widget-forms.php' );

require_once( BF_STELLAR_INC . 'functions/class.widget-reviews-slider.php' );
require_once( BF_STELLAR_INC . 'functions/class.widget-reviews-content.php' );
require_once( BF_STELLAR_INC . 'functions/class.widget-reviews-aggregate.php' );

require_once( BF_STELLAR_INC . 'functions/class.widget-slidebar.php' );

// Load Files
require_once( BF_STELLAR_INC . 'layouts/load.php' );
require_once( BF_STELLAR_INC . 'customizer/load.php' );
require_once( BF_STELLAR_INC . 'menus/load.php' );
require_once( BF_STELLAR_INC . 'admin/load.php' );

// Run the setup functions
BrightFire\Theme\Stellar\setup();