<?php

namespace BrightFire\Theme\Stellar\Themes\Gillman;

/** Custom Widgets */
require_once('widgets/class.gillman-counter-widget.php');
require_once('widgets/class.gillman-reviews-widget.php');


/**
 * Add Custom Ready Classes for this theme
 * @param $class_array
 *
 * @return array
 */
function gillman_RowClasses( $class_array ) {

	$add_classes = array(
		'gillman-widget-flex' => array(
			'name' => 'Gillman: Widget Flex - Stretch',
			'desc' => 'Adds flexbox to widget displays with justify-content: stretch'
		),
		'gillman-shadow' => array(
			'name' => 'Gillman: Shadow',
			'desc' => 'Adds a drop-shadow to the row'
		)
	);

	$new = array_merge( $add_classes, $class_array );

	asort( $new );

	return $new;
}
add_filter( 'stellar_row_class_choices', __NAMESPACE__ . '\gillman_RowClasses', 11, 2 );

/**
 * Add Custom Ready Classes for widgets
 * @param $class_array
 * @param $widget
 *
 * @return array
 */
function gillman_WidgetClasses( $class_array, $widget ) {

	// Custom classes for widgets
	$add_classes = array(
		'gillman-shadow' => array(
			'name' => 'Gillman: Shadow',
			'desc' => 'Adds a drop-shadow to the widget'
		),
		'gillman-recent-blog' => array(
			'name' => 'Gillman: Featured Blog Posts',
			'desc' => 'Adds button styling to read more links using the accent color; used for Homepage Featured Blog Posts'
		),
		'animate-slideInUp' => array(
			'name' => 'Animate: SlideInUp',
			'desc' => 'Animation hook to slide content into view when scrolled.'
		)
	);

	// Special case for Menu Bar
	if ( is_a( $widget, 'WP_Widget' ) && 'brightfire_menu_bar' == $widget->id_base ){
		$add_classes['gillman-menu'] = array(
			'name' => 'Gillman: Menu',
			'desc' => 'Applies default Gillman menu styles; Accent Color links with down-arrow dropdown indicators and secondary color hover',
		);
	}

	$new = array_merge( $add_classes, $class_array );

	asort( $new );

	return $new;

}
add_filter( 'stellar_widget_class_choices', __NAMESPACE__ . '\gillman_WidgetClasses', 11, 2 );