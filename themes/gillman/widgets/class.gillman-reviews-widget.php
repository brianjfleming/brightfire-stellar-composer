<?php

/**
 * Adds BrightFire_Slides_Widget widget.
 */
class Gillman_Review_Slides_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'gillman_review_slider_widget', // Base ID
			__( 'Gillman Review Slider', 'text_domain' ), // Name
			array( 'description' => __( 'Gillman Review Slider', 'text_domain' ), 'customize_selective_refresh' => true ) // Args
		);

	}


	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {

		echo $args['before_widget'];

		$slides_posts = get_posts(
			array(
				'orderby'   => 'menu_order',
				'order'     => 'ASC',
				'post_type' => 'bf_review',
			)
		);

		// Build an object of all slides with only what we need for output
		// Doing this so we can access specific slide data outside of foreach loop in output
		$i = 0;
		$slides = array();
		foreach ( $slides_posts as $slide ) :
			$slide_image_id = get_post_thumbnail_id($slide->ID);

			$slides[$i]['content'] = $slide->post_content;
			$slides[$i]['review_extras'] = get_post_meta( $slide->ID, 'review_meta', true );
			$slides[$i]['li_attrs'] =  ($i == 0) ? '' : ' style="display: none" class="hide-initially"';

			$i++;
		endforeach;

		?>

		<div class="gillman-review-slider loading">
			<h1 class="text-center"><? echo $instance['title']; ?></h1>
			<ul class="slides" style="list-style: none">
				<?php foreach ( $slides as $slide ) : ?>
					<li <?php echo $slide['li_attrs']; ?>>
						<?php if( !empty($slide['content']) ) :?>
							<div class="slide-content">
								<?php echo '<span class="slide-review">'.do_shortcode( $slide['content'] ).'</span>'; ?>
								<?php if (!empty($slide['review_extras']['review_author']))  echo $slide['review_extras']['review_author'] ; ?>
								<?php if (!empty($slide['review_extras']['review_author_meta'] ))  echo ' - '.$slide['review_extras']['review_author_meta'] ; ?>
							</div>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>

		<?php
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$fields = array(
			'title' => array(
				'label' => 'Title',
				'type' => 'text',
				'default_value' => '',

			)
		);

		$args = array(
			'fields'   => $fields,
			'instance' => $instance,
			'widget_instance' => $this,
			'display'  => 'basic',
			'echo'     => true,
		);
		\BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

	}



} // class BrightFire_Review_Slides_Widget


// register BrightFire_Review_Slides_Widget widget
function register_gillman_review_slider_widget() {
	register_widget( 'Gillman_Review_Slides_Widget' );
}

add_action( 'widgets_init', 'register_gillman_review_slider_widget' );
