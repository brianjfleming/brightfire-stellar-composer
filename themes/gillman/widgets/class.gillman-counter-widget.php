<?php

/**
 * BrightFire Recent Blog Posts widget class
 */
class Gillman_Counter_Widget extends WP_Widget {

	public function __construct() {

		$widget_ops = array(
			'classname'                   => 'gillman-counter',
			'description'                 => __( 'Three JS Counters and labels with a title' ),
			'customize_selective_refresh' => true,
		);

		parent::__construct( 'gillman_counter', __( 'Gillman JS Counter' ), $widget_ops );
	}

	// Front end display
	public function widget( $args, $instance ) {

		// Default is instance title
		$title = trim( $instance['title'] );

		// Apply Filters and Print Widget
		echo $args['before_widget'];
		echo $args['before_title'] . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $args['after_title'];
		echo '<div class="counter-wrapper">';
			echo '<div class="counter-one">';
				echo '<h5 class="counter-one-title">' . $instance[ 'counter_one_title' ] . '</h5>';
				echo '<span class="counter-one-value gillman-counter-value">' . $instance[ 'counter_one_value' ] . '</span>';
			echo '</div>';

			echo '<div class="counter-two">';
				echo '<h5 class="counter-two-title">' . $instance[ 'counter_two_title' ] . '</h5>';
				echo '<span class="counter-two-value gillman-counter-value">' . $instance[ 'counter_two_value' ] . '</span>';
			echo '</div>';

			echo '<div class="counter-three">';
				echo '<h5 class="counter-three-title">' . $instance[ 'counter_three_title' ] . '</h5>';
				echo '<span class="counter-three-value gillman-counter-value">' . $instance[ 'counter_three_value' ] . '</span>';
			echo '</div>';
		echo '</div>';
		echo $args['after_widget'];

	}

	// Admin display
	public function form( $instance ) {

		// My Form
		$fields = array(
			'title'               => array(
				'label' => 'Title',
				'type'  => 'text',
				'default_value' => ''
			),
			'counter_one_title'   => array(
				'label' => 'Counter 1: Label',
				'type'  => 'text',
				'default_value' => ''
			),
			'counter_one_value'   => array(
				'label' => 'Counter 1: Value',
				'type'  => 'number',
				'default_value' => '0'
			),
			'counter_two_title'   => array(
				'label' => 'Counter 2: Label',
				'type'  => 'text',
				'default_value' => ''
			),
			'counter_two_value'   => array(
				'label' => 'Counter 2: Value',
				'type'  => 'number',
				'default_value' => '0'
			),
			'counter_three_title' => array(
				'label' => 'Counter 3: Label',
				'type'  => 'text',
				'default_value' => ''
			),
			'counter_three_value' => array(
				'label' => 'Counter 3: Value',
				'type'  => 'number',
				'default_value' => '0'
			)
		);

		// Build our widget form
		$args = array(
			'fields'          => $fields,
			'instance'        => $instance,
			'widget_instance' => $this,
			'display'         => 'basic',
			'echo'            => true,
		);
		\BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

	}


	public function update( $new_instance, $old_instance ) {
		$instance                        = $old_instance;
		$instance['title']               = $new_instance['title'];
		$instance['counter_one_title']   = $new_instance['counter_one_title'];
		$instance['counter_one_value']   = $new_instance['counter_one_value'];
		$instance['counter_two_title']   = $new_instance['counter_two_title'];
		$instance['counter_two_value']   = $new_instance['counter_two_value'];
		$instance['counter_three_title'] = $new_instance['counter_three_title'];
		$instance['counter_three_value'] = $new_instance['counter_three_value'];

		return $instance;
	}
}

// Register widget
add_action( 'widgets_init', function () {
	register_widget( 'Gillman_Counter_Widget' );
} );
