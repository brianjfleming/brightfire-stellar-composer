/**
 * Theme Name:  Gillman Custom Theme
 * Theme URI:   https://www.brightfire.com
 * Description: Registers the following Custom CSS Classes: <br><br>&bull; <b>Gillman: Menu</b> - Applies default Gillman Ins menu styles<br>&bull; <b>Gillman: Widget Flex</b> - Applies flexbox to a row using the STRETCH param<br>&bull; <b>Gillman: Shadow</b> - Available on widgets and rows; adds a simple box-shadow<br>&bull; <b>Gillman: Featured Blog Posts</b> - Applies Gillman HP featured news styles to a recent-blog-post widget<br>&bull; <b>Animate: slideInUp</b> - Creates a slide-up animation when widget scrolls into viewport.<br><br>Custom Widgets: <br><br>&bull; <b>Gillman JS Counter</b> - Widget that counts up to a given number when scrolled into view. Allows for three label/value pairs.<br>&bull; <b>Gillman Review Slider</b> - Custom reviews slider
 * Author:      Brian Fleming
 * Author URI:  https://www.brightfire.com
 * Version:     1.0
 * License:     GPLv2+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */