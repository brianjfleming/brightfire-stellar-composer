/**
 * Created by brian on 6/7/16.
 */
var $=jQuery.noConflict();

$(document).ready( function() {
    setupFixedHeader();
});

function setupFixedHeader() {
    $('header#site-header').attr('data-row-fixed','true');
}