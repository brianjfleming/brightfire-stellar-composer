/**
 * Theme Name:  Peterson Insurance
 * Theme URI:   https://www.brightfire.com
 * Description: Custom theme designed for Peterson Insurance
 * Author:      Brian Fleming
 * Author URI:  https://www.brightfire.com
 * Version:     0.1.0
 * Tags:
 * Text Domain: bf_stellar_peterson
 *
 * License:     GPLv2+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */