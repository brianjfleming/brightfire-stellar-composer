<?php

namespace BrightFire\Theme\Stellar\Themes\Peterson;


/**
 * Add Custom Ready Classes for widgets
 * @param $class_array
 * @param $widget
 *
 * @return array
 */
function peterson_WidgetClasses( $class_array, $widget ) {

	// Custom classes for widgets
	$add_classes = array(
		'peterson-logo' => array(
			'name' => 'Peterson: Header Logo',
			'desc' => 'Will allow logo to overhang off the row it is assigned to'
		),
	);

	// Special case for Menu Bar
	if ( is_a( $widget, 'WP_Widget' ) && 'brightfire_menu_bar' == $widget->id_base ){
		$add_classes['peterson-dividers'] = array(
			'name' => 'Peterson: Menu Dividers',
			'desc' => 'Applies dividers to menu bar'
		);
		$add_classes['peterson-text-shadow'] = array(
			'name' => 'Peterson: Menu Text Shadow',
			'desc' => 'Applies a light text-shadow to top-level links'
		);
		$add_classes['peterson-screen-hover'] = array(
			'name' => 'Peterson: Screen Hover',
			'desc' => 'Overrides default hover state to use a light screen'
		);
		$add_classes['peterson-text-size-menu'] = array(
			'name' => 'Peterson: Text Size: Menu',
			'desc' => 'Custom text-size for menu links'
		);
	}

	$new = array_merge( $add_classes, $class_array );

	asort( $new );

	return $new;

}
add_filter( 'stellar_widget_class_choices', __NAMESPACE__ . '\peterson_WidgetClasses', 11, 2 );


/**
 * Add Custom Ready Classes for this theme
 * @param $class_array
 *
 * @return array
 */
function peterson_RowClasses( $class_array ) {

	$add_classes = array(
		'abs-row' => array(
			'name' => 'Absolute Row',
			'desc' => 'Allows position: absolute elements inside the row and will guarantee'
		),
		'force-col' => array(
			'name' => 'Force Display Column',
			'desc' => 'Force display of columns, even if it contains no widgets'
		),
		'drop-shadow' => array(
			'name' => 'Drop Shadow',
			'desc' => 'Adds a drop shadow to the row'
		)
	);

	$new = array_merge( $add_classes, $class_array );

	asort( $new );

	return $new;
}
add_filter( 'stellar_row_class_choices', __NAMESPACE__ . '\peterson_RowClasses', 11, 2 );