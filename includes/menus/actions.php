<?php

namespace BrightFire\Theme\Stellar;

/**
 * Add a new metabox to the menus area
 *
 * TODO: Should we remove this? Is it worth having or does it create confusion?
 */
function stellar_menu_item_add() {

	// Adds Custom Menu Item Type Stellar Menu Item
	// Just an extension of the Custom Link type without a HREF field
	// Automatically puts a '#' in _post_meta_url
	add_meta_box('stellar-menu-item','Stellar Menu Item', __NAMESPACE__ . '\stellar_menu_item','nav-menus','side', 'default');
}