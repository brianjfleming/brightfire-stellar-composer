<?php

namespace BrightFire\Theme\Stellar;

/**
 * Recursive function to build out a nested array of nav items
 *
 * @param $menu_id
 * @param $parent_id
 * @param $depth
 *
 * @return array
 */
function stellar_menus_format_arr_recurse ( $menu_id, $parent_id, $depth ) {

	// Optional query params to only get menu_items of the current parent
	$args = array(
		'meta_key' => '_menu_item_menu_item_parent',
		'meta_value' => $parent_id
	);

	// Get all of our menu_items from WP and set up our container array
	$children = wp_get_nav_menu_items( $menu_id, $args );
	$child_array = array();


	// For each of our elements get our post_meta and save as a simple array element
	// with the menu_item id as index
	foreach( $children as $child_item ) {

		// Get our post meta
		$child_item_meta = get_post_meta( $child_item->ID );
		$item_icon       = ( isset( $child_item_meta['_stellar_menu_item_icon'][0] ) ? $child_item_meta['_stellar_menu_item_icon'][0] : '' );
		$item_hidelabel  = ( isset( $child_item_meta['_stellar_menu_item_hidelabel'][0] ) ? $child_item_meta['_stellar_menu_item_hidelabel'][0] : false );
		$item_href       = ( isset ( $child_item_meta['_stellar_menu_item_killhref'][0] ) ? $child_item_meta['_stellar_menu_item_killhref'][0] : false );
		$item_panel_id   = ( isset( $child_item_meta['_stellar_menu_item_panel_id'][0] ) ? $child_item_meta['_stellar_menu_item_panel_id'][0] : 'none' );
		$item_classes    = ( isset( $child_item->classes ) ? $child_item->classes : '' );
		$item_target    = ( isset( $child_item->target ) ? $child_item->target : '' );

		// Set up our menu_item element
		$child_array[$child_item->ID] = array(
			'depth' => $depth,
			'title' => $child_item->title,
			'icon' => $item_icon,
			'hidelabel' => $item_hidelabel,
			'killhref' => $item_href,
			'url'       => $child_item->url,
			'panel_id' => $item_panel_id,
			'classes' => $item_classes,
			'object_id' => $child_item->object_id,
			'target'    => $item_target
		);

		// Recurse our method to find any children of this element
		$child_array[$child_item->ID]['children'] = stellar_menus_format_arr_recurse( $menu_id, $child_item->ID, $depth+1 );

	}

	return $child_array;
}

/**
 * Builds our markup for our menu and outputs to page
 * @param $atts
 */
function stellar_custom_menu( $atts ) {

	// Shortcode Defaults
	$atts = shortcode_atts( array(
		'menu_id' => 0,
		'menu_align' => 'left',
		'menu_display' => 'menubar',
		'uniqid' => uniqid(),
		'mobile_menu_text' => '',
		'separator' => '',
		'menu_style' => '',
		'text_menu'         => '',
		'text_hover'        => '',
		'background_hover'  => '',
		'drop_text_hover'  => '',
		'drop_background_hover'  => ''

	), $atts, 'stellar_custom_menu' );

	// make sure this is actually a menu
	if( ! is_numeric($atts['menu_id'] ) || ! is_nav_menu( $atts['menu_id'] ) ) {
		echo \BrightFire\Library\BFCore\format_bf_error( 'Unknown Menu Id: ' . $atts['menu_id'], $atts );
		return;
	}

	// Format our array using our recursive method
	// Defaults parent_id and depth params to 0 for top level
	$formatted_menu_arr = stellar_menus_format_arr_recurse( $atts['menu_id'], 0, 0 );

	$output = '';

	// Display Classes
	switch( $atts['menu_display'] ) {
		case 'menubar' :
			$display_class = 'bfmenu-menubar';
			break;
		case 'inline' :
			$display_class = 'bfmenu-inline';
			break;
		case 'stacked' :
			$display_class = 'bfmenu-stacked';
			break;
		default :
			$display_class = 'bfmenu-menubar';
	}

	$menu_style_class = ( ! empty ( $atts['menu_style'] ) ) ? "bfmenu-{$atts['menu_style']}" : '';

	// Menu Wrapper and Recursion
	$output .= '<div id="bfmenu_' . $atts['uniqid'] . '" class="bfmenu responsive ' . $menu_style_class . '">';
	$output .= '<ul class="' .  $display_class . ' bfmenu-top-level">';
	$output .= stellar_custom_menu_recurse( $formatted_menu_arr, $atts );
	$output .= '</ul>';
	$output .= '</div>';

	// Instantiate our menu directly
	if ( 'bfmenu-menubar' === $display_class ) {
		$output .= format_menu_javascript( $atts );
	}

	// Echo our final output
	echo $output;

}
add_shortcode( 'stellar_custom_menu', __NAMESPACE__ . '\stellar_custom_menu' );

/**
 * Recursion method to build each child element of our menu
 * @param $child_items
 *
 * @return string
 */
function stellar_custom_menu_recurse( $child_items, $atts ) {

	$output = $dropdown_background = '';

	// Get our depth of the first set in the elements
	// we are handling to determine dropdown, etc
	$depth = array_values( $child_items )[0]['depth'];

	$li_classes = [];

	if ( 'menubar' == $atts[ 'menu_display' ] ){

		if ( 0 == ( $depth ) ) {
			$li_classes[] = 'text-menu-' . $atts[ 'text_menu' ];
			$li_classes[] = 'text-hover-' . $atts[ 'text_hover' ];
			$li_classes[] = 'background-hover-' . $atts[ 'background_hover' ];
			$li_classes[] = 'dropdown-border-' . $atts[ 'background_hover' ];

		} elseif ( 0 == $depth % 2  ) {

			$li_classes[] = 'text-menu-' . $atts[ 'drop_text_hover' ];
			$li_classes[] = 'text-hover-' . $atts[ 'text_hover' ];
			$li_classes[] = 'background-hover-' . $atts[ 'background_hover' ];
			$li_classes[] = 'dropdown-border-' . $atts[ 'drop_background_hover' ];
			$dropdown_background = 'background-' . $atts[ 'drop_background_hover' ];

		} else {
			$li_classes[] = 'text-menu-' . $atts[ 'text_hover' ];
			$li_classes[] = 'text-hover-' . $atts[ 'drop_text_hover' ];
			$li_classes[] = 'background-hover-' . $atts[ 'drop_background_hover' ];
			$li_classes[] = 'dropdown-border-' . $atts[ 'background_hover' ];
			$dropdown_background = 'background-' . $atts[ 'background_hover' ];
		}
	}

	if( 0 < $depth ) {
		$output .= '<ul class="dropdown ' . $dropdown_background . '">';
	}

	$i = 0;
	foreach( $child_items as $menu_item_id => $menu_item_data ) {

		$i++;
		$classes = [];

		// Add class to menu item that correspond to the currently rendered page.
		$classes[] = ( get_queried_object_id() == $menu_item_data['object_id'] ) ? 'current-menu-item' : '';

		$child_panels = false;

		// Check for Children with PANEL ID set
		if( !empty( $menu_item_data['children'] ) ) {

			foreach( $menu_item_data['children'] as $child_id => $child_data ) {
				if( 'none' !== $child_data['panel_id'] ) {
					$child_panels = true;
				}
			}
		}

		if ( $child_panels ) {
			$classes[] = 'megamenu-parent';
		} elseif ( 'menubar' == $atts['menu_display']  && 'none' != $menu_item_data['panel_id']  ) {
			$classes[] = 'megamenu-panel';
		}

		if ('inline' == $atts['menu_display'] && '' != $atts['separator'] && 1 != $i) {
			$output .= '<li class="separator">' . $atts['separator']  . '</li>';
		}

		// Custom Classes;
		$classes = array_merge( $classes, $menu_item_data[ 'classes' ], $li_classes );

		// Output our list item and link
		$output .= '<li class="' . implode( ' ', $classes ) . '">';
		$output .= format_menu_item( $menu_item_data, $atts );

		// If this item has a panel association or dropdowns
		if ( $child_panels ) {

			$output .= format_menu_tab( $atts, $menu_item_data['children'] );

		} elseif ( 'menubar' == $atts['menu_display']  && 'none' !== $menu_item_data[ 'panel_id' ] ) {

			$output .= format_menu_panel( $menu_item_data );

		} elseif ( ! empty( $menu_item_data['children'] ) ) {

			$output .= stellar_custom_menu_recurse( $menu_item_data['children'], $atts );
		}

		// Close our list item
		$output .= '</li>';

	}

	if( 0 < $depth ) {
		$output .= '</ul>';
	}

	return $output;

}

/**
 * Helper Function to format parent link and panel for menu
 * @param $item
 *
 * @return string
 */
function format_menu_panel( $item ) {

	$item_output = '';

	$item_output .= '<div class="megamenu">';

	ob_start();

	dynamic_sidebar( $item['panel_id'] );
	$item_output .= ob_get_contents();

	ob_end_clean();

	$item_output .= '</div>';

	return $item_output;
}

function format_menu_tab( $atts, $children ) {

	$tab_base_id = uniqid('tab');

	$nav_output = $tabs_output = '';

	$count = 0;

	$classes = [];

	$classes[] = 'text-menu-' . $atts[ 'drop_text_hover' ];
	$classes[] = 'text-hover-' . $atts[ 'text_hover' ];
	$classes[] = 'background-hover-' . $atts[ 'background_hover' ];
	$classes[] = 'dropdown-border-' . $atts[ 'drop_background_hover' ];
	$ul_background = 'background-' . $atts[ 'drop_background_hover' ];
	$dropdown_background = 'background-' . $atts[ 'background_hover' ];

	foreach( $children as $child_item_id => $child_item_data ) {

		$active = ( 0 == $count ? 'active' : '' );
		$tab_id = "{$tab_base_id}-$count";

		// Desktop Tabs
		$nav_output .= '<li class="tab-nav-item ' . implode( ' ', $classes ) . '"><a href="#" class="' . $active . '" rel="' . $tab_id . '">';
		$nav_output .= ( ! empty( $child_item_data['icon'] ) ) ? '<i class="fa fa-' . $child_item_data['icon'] . '" aria-hidden="true"></i> ' : ''; // Icon
		$nav_output .= ( ! empty( $child_item_data['icon'] ) && false == $child_item_data['hidelabel'] ) ? '&nbsp;' : '' ; // Space between Icon and Label
		$nav_output .= ( true == $child_item_data['hidelabel'] ) ? '' : $child_item_data['title'] ; // Item Label
		$nav_output .= '</a></li>';

		// Mobile Tabs
		$tabs_output .= '<a href="" class="bfmenu-tab-heading '. $active . '" rel="' . $tab_id . '">';
		$tabs_output .= ( ! empty( $child_item_data['icon'] ) ) ? '<i class="fa fa-' . $child_item_data['icon'] . '" aria-hidden="true"></i> ' : ''; // Icon
		$tabs_output .= ( ! empty( $child_item_data['icon'] ) && false == $child_item_data['hidelabel'] ) ? '&nbsp;' : '' ; // Space between Icon and Label
		$tabs_output .= ( true == $child_item_data['hidelabel'] ) ? '' : $child_item_data['title'] ; // Item Label
		$tabs_output .= '</a>';

		// Tab Content
		$tabs_output .= '<div class="bfmenu-tab-content ' . $active . ' ' . $dropdown_background . '"  id="' . $tab_id . '">';

		ob_start();

		dynamic_sidebar( $child_item_data['panel_id'] );
		$tabs_output .= ob_get_contents();

		ob_end_clean();

		$tabs_output .= '</div>';

		$count++;

	}

	$output = '<div class="megamenu megamenu-tab-panel"><div class="bfmenu-tabs-wrap ' . $ul_background . '">';

		$output .= '<ul class="bfmenu-tabs-nav">' . $nav_output . '</ul>';

		$output .= '<div class="bfmenu-tabs">' . $tabs_output . '</div>';

	$output .= '</div></div>';

	return $output;

}

/**
 * Helper Function to format the Link part of our menu items
 * @param $item
 *
 * @return string
 */
function format_menu_item( $item, $atts ) {

	$item_output = $item_target = '';

	$link_classes = array();

	if( 0 == $item['depth'] && substr_count( $atts['menu_style'], 'button-bar') ) {
		$link_classes[] = 'btn';
	}

	$item_target = ( $item[ 'target' ] ) ? 'target="_blank"' : '';
	$item_url = ( false == $item['killhref'] ) ? $item['url'] : 'javascript: void(0);';

	$link_classes = implode(',', $link_classes );

	$item_output .= '<a href="' . $item_url . '" ' . $item_target . ' class="'. $link_classes .'"><span class="menu-text">';
	$item_output .= ( ! empty( $item['icon'] ) ) ? '<i class="fa fa-' . $item['icon'] . '" aria-hidden="true"></i> ' : ''; // Item Icon
	$item_output .= ( ! empty( $item['icon'] ) && false == $item['hidelabel'] ) ? '&nbsp;' : '' ; // Space between Icon and Item Label
	$item_output .= ( true == $item['hidelabel'] ) ? '' : $item['title'] ; // Item Label
	$item_output .= '</span></a>';

	return $item_output;
}

/**
 * Instantiate our menu
 * @param $id
 *
 * @return string
 */
function format_menu_javascript( $atts ) {

	return '<script type="text/javascript">' . "\n" .
	       'jQuery(document).ready(function(){' . "\n" .
	       '    jQuery("#bfmenu_' .$atts['uniqid'] . '").bfmenu({' . "\n" .
	       '        mobileMenuText: "' . $atts['mobile_menu_text'] . '",' . "\n" .
	       '        showDelay: 300,' . "\n" .
	       '        effect: "fade",' . "\n" .
	       '    });' . "\n" .
	       '});' . "\n" .
	       "\n" .
	       'jQuery("#bfmenu_' . $atts['uniqid'] . '").parents(".row").css({"z-index":10});' . "\n" .
	       '</script>';

}
