<?php

namespace BrightFire\Theme\Stellar;

/**
 * Admin API Field array for our custom Menu Item Fields
 * @return array
 */
function stellar_define_custom_fields( $item_id ) {
	
	$prefix = '_stellar_menu_item_';
	
	return array(
		$prefix . 'icon' => array(
			'name' => $prefix . 'icon[' . $item_id . ']',
			'type' => 'fontawesome_select',
			'label' => __('Item Icon', 'stellar'),
			'container_class' => 'stellar-icon',
			'default_value' => '',
			'sanitize' => 'wp_kses',
			'search' => true,
		),
		$prefix . 'hidelabel' => array(
			'name' => $prefix . 'hidelabel[' . $item_id . ']',
			'type' => 'selectize',
			'label' => __('Hide Label', 'stellar'),
			'container_class' => 'stellar-option',
			'choices' => 'yesno',
			'default_value' => '0',
		),
		$prefix . 'killhref' => array(
			'name' => $prefix . 'killhref[' . $item_id . ']',
			'type' => 'selectize',
			'label' => __('Disable HREF Link', 'stellar'),
			'container_class' => 'stellar-option',
			'choices' => 'yesno',
			'default_value' => '0',
		),
		$prefix . 'panel_id' => array(
			'name' => $prefix . 'panel_id[' . $item_id . ']',
			'type' => 'selectize',
			'label' => __('Select Panel To Use', 'stellar'),
			'container_class' => 'stellar-option',
			'choices' => format_panel_row_choices(),
			'default_value' => 'none',
		),
	);
}

/**
 * Choices for our Panel ID field
 * Returns all Menu Panels registered in Layouts. Appends 
 * 'None' and 'Treat as Tab Panel' for alt behaviours.
 * 
 * @return array
 */
function format_panel_row_choices() {

	global $stellar_layout;

	$menu_panels = $stellar_layout->get_layout_option( 'menu_panels' );
	$avail_rows = array();

	$avail_rows['none'] = 'None';

	foreach( $menu_panels as $panel_id => $data ) {
			$avail_rows[ $panel_id ] = $data['name'];
	}

	return $avail_rows;

}

/**
 * Stellar Menu Item Metabox
 *
 * TODO: Should we remove this? Is it worth having or does it create confusion?
 *
 */
function stellar_menu_item()
{
	global $_nav_menu_placeholder, $nav_menu_selected_id;

	$_nav_menu_placeholder = 0 > $_nav_menu_placeholder ? $_nav_menu_placeholder - 1 : -1;

	?>
	<div class="stellarcustomlinkdiv" id="stellarcustomlinkdiv">
		<input type="hidden" value="custom" name="menu-item[<?php echo $_nav_menu_placeholder; ?>][menu-item-type]" />

		<p id="menu-item-name-wrap" class="wp-clearfix">
			<label class="howto" for="stellar-menu-item-name" style="float: left; margin-top: 6px;">Link Text</label>
			<input id="stellar-menu-item-name" name="menu-item[<?php echo $_nav_menu_placeholder; ?>][menu-item-title]" type="text" class="regular-text menu-item-textbox" style="width:180px; float: right">
		</p>

		<p class="button-controls">
			<span class="add-to-menu">
				<input type="submit"<?php wp_nav_menu_disabled_check( $nav_menu_selected_id ); ?> class="button-secondary submit-add-to-menu right" value="<?php esc_attr_e('Add to Menu'); ?>" name="add-custom-menu-item" id="submit-stellarcustomlinkdiv" />
				<span class="spinner"></span>
			</span>
		</p>

	</div><!-- /.stellarcustomlinkdiv -->

	<?php
}
