<?php

class Stellar_Widget_Classes {

	/**
	 * Adds form fields to Widget
	 * @static
	 *
	 * @param $widget
	 * @param $return
	 * @param $instance
	 *
	 * @return array
	 * @since 1.0
	 */
	public static function extend_widget_form( $widget, $return, $instance ) {

		// For core CSS classes used for structure - one-half, one-third, etc
		if ( ! isset( $instance['structure_class'] ) ) {
			$instance['structure_class'] = 'full-width';
		}

		// For CSS classes
		if ( ! isset( $instance['custom_classes'] ) ) {
			$instance['custom_classes'] = array();
		}

		// For Desktop CSS classes
		if ( ! isset( $instance['desktop_classes'] ) ) {
			$instance['desktop_classes'] = array();
		}

		// For Mobile CSS classes
		if ( ! isset( $instance['mobile_classes'] ) ) {
			$instance['mobile_classes'] = array();
		}

		$structure_class_options  = apply_filters( 'stellar_widget_structure_classes', \BrightFire\Theme\Stellar\widget_structure_choices() );
		$offset_class_options     = apply_filters( 'stellar_widget_offset_classes', \BrightFire\Theme\Stellar\widget_offset_choices() );
		$custom_class_options     = apply_filters( 'stellar_widget_class_choices', \BrightFire\Theme\Stellar\widget_class_choices(), $widget );
		$breakpoint_class_options = apply_filters( 'stellar_widget_breakpoint_choices', \BrightFire\Theme\Stellar\widget_breakpoint_choices(), $widget );
		$title_class_options      = apply_filters( 'stellar_widget_title_class_choices', \BrightFire\Theme\Stellar\widget_title_class_choices(), $widget );
		$custom_mixes             = apply_filters( 'stellar_widget_mix_choices', \BrightFire\Theme\stellar\widget_mix_choices(), $widget );

		$structure_fields = array(
			'structure_class' => array(
				'label'   => '<i class="fa fa-columns"></i> ' . __( 'Widget Width', 'bf-admin-api' ),
				'type'    => 'selectize',
				'choices' => $structure_class_options,
				'permit'  => 1,
			),
			'offset_class'    => array(
				'label'   => '<i class="fa fa-indent"></i> ' . __( 'Widget Offset', 'bf-admin-api' ),
				'type'    => 'selectize',
				'choices' => $offset_class_options,
				'permit'  => 1,
			)
		);

		$class_fields = array(
			'mixes'           => array(
				'label'   => '<i class="fa fa-tint"></i> ' . __( 'Mixes', 'bf-admin-api' ),
				'type'    => 'selectize',
				'choices' => $custom_mixes,
				'atts'    => array(
					'multiple' => 'multiple',
				),
				'permit'  => 1,
			),
			'custom_classes'  => array(
				'label'   => '<i class="fa fa-globe"></i> ' . __( 'Global', 'bf-admin-api' ),
				'type'    => 'selectize',
				'choices' => $custom_class_options,
				'atts'    => array(
					'multiple' => 'multiple',
				),
				'permit'  => 1,
			),
			'desktop_classes' => array(
				'label'   => '<i class="fa fa-laptop"></i> ' . __( 'Desktop', 'bf-admin-api' ),
				'type'    => 'selectize',
				'choices' => $breakpoint_class_options,
				'atts'    => array(
					'multiple' => 'multiple',
				),
				'permit'  => 1,
			),
			'mobile_classes'  => array(
				'label'   => '<i class="fa fa-mobile"></i> ' . __( 'Mobile', 'bf-admin-api' ),
				'type'    => 'selectize',
				'choices' => $breakpoint_class_options,
				'atts'    => array(
					'multiple' => 'multiple',
				),
				'permit'  => 1,
			),
		);

		$title_fields = array(
			'title_classes' => array(
				'label'   => '<i class="fa fa-header"></i> ' . __( 'Widget Title', 'bf-admin-api' ),
				'type'    => 'selectize',
				'choices' => $title_class_options,
				'atts'    => array(
					'multiple' => 'multiple',
				),
				'permit'  => 1,
			),
		);

		$fields_concat = array_merge( $structure_fields, $class_fields, $title_fields );

		// Admin API field call
		$fields = \BF_Admin_API_Fields::bf_admin_api_fields_build(
			array(
				'fields'     => $fields_concat,
				'instance'   => $instance,
				'display'    => 'basic',
				'section_id' => 'widget-' . $widget->id_base . '[' . $widget->number . ']',
				'echo'       => false,
			)
		);

		echo '<div class="stellar-widget-options">';
		echo '<h3>Stellar Options</h3>';
		echo $fields;
		echo '</div>';

		return $instance;
	}

	/**
	 * Updates the Widget with the classes
	 * @static
	 *
	 * @param $instance
	 * @param $new_instance
	 *
	 * @return array
	 * @since 1.0
	 */
	public static function update_widget( $instance, $new_instance ) {
		$instance['mixes']           = $new_instance['mixes'];
		$instance['custom_classes']  = $new_instance['custom_classes'];
		$instance['desktop_classes'] = $new_instance['desktop_classes'];
		$instance['mobile_classes']  = $new_instance['mobile_classes'];
		$instance['structure_class'] = $new_instance['structure_class'];
		$instance['offset_class']    = $new_instance['offset_class'];
		$instance['title_classes']   = $new_instance['title_classes'];
		do_action( 'widget_css_classes_update', $instance, $new_instance );

		return $instance;
	}

	/**
	 * Adds the classes to the widget in the front-end
	 * @static
	 *
	 * @param $params
	 *
	 * @return mixed
	 * @since 1.0
	 */
	public static function add_widget_classes( $params ) {

		if ( is_admin() ) {

			return $params;
		}

		global $wp_registered_widgets, $widget_number;

		$arr_registered_widgets = wp_get_sidebars_widgets(); // Get an array of ALL registered widgets
		$sidebar_id             = $params[0]['id']; // Get the id for the current sidebar we're processing
		$widget_id              = $params[0]['widget_id'];
		$widget_obj             = $wp_registered_widgets[ $widget_id ];
		$widget_num             = $widget_obj['params'][0]['number'];
		$widget_opt             = get_option( $widget_obj['callback'][0]->option_name );
		$css_classes            = $title_classes = array();
		$mixes                  = get_option( 'stellar_mixes' );

		// add CSS structure class
		if ( isset( $widget_opt[ $widget_num ]['structure_class'] ) && ! empty( $widget_opt[ $widget_num ]['structure_class'] ) ) {
			$css_classes[] = $widget_opt[ $widget_num ]['structure_class'];
		} else {
			$css_classes[] = "full-width";
		}

		// add CSS offset class
		if ( isset( $widget_opt[ $widget_num ]['offset_class'] ) && ! empty( $widget_opt[ $widget_num ]['offset_class'] ) ) {
			$css_classes[] = $widget_opt[ $widget_num ]['offset_class'];
		}

		// add Mixes
		if ( isset( $widget_opt[ $widget_num ]['mixes'] ) && ! empty( $widget_opt[ $widget_num ]['mixes'] ) ) {
			foreach ( $widget_opt[ $widget_num ]['mixes'] as $mix ) {


				if ( empty( $mix ) ) {
					break;
				}

				$mix_data = $mixes[ $mix ];

				foreach ( $mix_data['custom_classes'] as $class ) {
					$css_classes[] = $class;
				}

				foreach ( $mix_data['desktop_classes'] as $class ) {
					$css_classes[] = "d-{$class}";
				}

				foreach ( $mix_data['mobile_classes'] as $class ) {
					$css_classes[] = "m-{$class}";
				}

				foreach ( $mix_data['title_classes'] as $class ) {
					$title_classes[] = $class;
				}

			}
		}


		// add Custom CSS classes
		if ( isset( $widget_opt[ $widget_num ]['custom_classes'] ) && ! empty( $widget_opt[ $widget_num ]['custom_classes'] ) ) {
			foreach ( $widget_opt[ $widget_num ]['custom_classes'] as $class ) {
				$css_classes[] = $class;
			}
		}

		// add Desktop Only CSS classes
		if ( isset( $widget_opt[ $widget_num ]['desktop_classes'] ) && ! empty( $widget_opt[ $widget_num ]['desktop_classes'] ) ) {
			foreach ( $widget_opt[ $widget_num ]['desktop_classes'] as $class ) {
				$css_classes[] = "d-{$class}";
			}
		}

		// add Mobile Only CSS classes
		if ( isset( $widget_opt[ $widget_num ]['mobile_classes'] ) && ! empty( $widget_opt[ $widget_num ]['mobile_classes'] ) ) {
			foreach ( $widget_opt[ $widget_num ]['mobile_classes'] as $class ) {
				$css_classes[] = "m-{$class}";
			}
		}

		// add Widget Title CSS classes
		if ( isset( $widget_opt[ $widget_num ]['title_classes'] ) && ! empty( $widget_opt[ $widget_num ]['title_classes'] ) ) {
			foreach ( $widget_opt[ $widget_num ]['title_classes'] as $class ) {
				$title_classes[] = $class;
			}
		}

		// add first, last, even, and odd classes

// REMOVED TEMPORARILY - CAUSING FALSE POSITIVES IN CUSTOMIZER
// WITH SELECTIVE REFRESH, AND ON EVERY PARTIAL UPDATE WOULD
// ALWAYS ADD WIDGET-FIRST, WIDGET-LAST, AND WIDGET-ODD
// RESULTING IN UNINTENDED BEHAVIOURS
//
//		if ( ! $widget_number ) {
//			$widget_number = array();
//		}
//
//		if ( ! isset( $arr_registered_widgets[ $sidebar_id ] ) || ! is_array( $arr_registered_widgets[ $sidebar_id ] ) ) {
//			return $params;
//		}
//
//		if ( isset( $widget_number[ $sidebar_id ] ) ) {
//			$widget_number[ $sidebar_id ] ++;
//		} else {
//			$widget_number[ $sidebar_id ] = 1;
//		}
//
//		$css_classes[] = "widget-{$widget_number[ $sidebar_id ]}";
//		$widget_first  = 'widget-first';
//		$widget_last   = 'widget-last';
//
//		if ( $widget_number[ $sidebar_id ] == 1 ) {
//			$css_classes[] = $widget_first;
//		}
//
//		if ( $widget_number[ $sidebar_id ] == count( $arr_registered_widgets[ $sidebar_id ] ) ) {
//
//			$css_classes[] = $widget_last;
//			$widget_even   = 'widget-even';
//			$widget_odd    = 'widget-odd';
//
//			$css_classes[] = ( ( $widget_number[ $sidebar_id ] % 2 ) ? $widget_odd : $widget_even );
//		}

		$classes                    = implode( " ", array_unique( $css_classes ) );
		$title_classes              = implode( " ", array_unique( $title_classes ) );
		$params[0]['before_widget'] = preg_replace( '/class="/', "class=\"$classes ", $params[0]['before_widget'], 1 );
		$params[0]['before_title']  = preg_replace( '/class="/', "class=\"$title_classes ", $params[0]['before_title'], 1 );

		// Last Classes
		$match_candidates = array(
			'one-half-last',
			'one-third-last',
			'one-fourth-last',
			'one-fifth-last',
			'two-thirds-last',
			'two-fifths-last',
			'three-fourths-last',
			'three-fifths-last',
			'four-fifths-last',
		);

		$matches = array_intersect( $match_candidates, $css_classes );

		if ( ! empty( $matches ) ) {
			$params[0]['after_widget'] = $params[0]['after_widget'] . '<div class="clearfix"></div>';
		}

		return $params;
	}
}

/**
 * Add form to widget
 */
add_action( 'in_widget_form', array( 'Stellar_Widget_Classes', 'extend_widget_form' ), 10, 3 );

/**
 * Update widget options
 */
add_filter( 'widget_update_callback', array( 'Stellar_Widget_Classes', 'update_widget' ), 10, 2 );

/**
 * Add classes to widgets
 */
add_filter( 'dynamic_sidebar_params', array( 'Stellar_Widget_Classes', 'add_widget_classes' ), 10, 1 );
