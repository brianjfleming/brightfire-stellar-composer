<?php


namespace BrightFire\Theme\Stellar;

/**
 * Shortcode for BF Button
 *
 * @param $atts
 *
 * @return string
 */
function bf_button_shortcode( $atts ) {

	$defaults = array(
		'button_icon'   => '',
		'button_style'  => 'btn',
		'button_color'  => '',
		'button_size'   => '',
		'button_text'   => '',
		'button_link'   => '',
		'button_rel'    => '',
		'button_target' => '',
	);
	$atts     = shortcode_atts( $defaults, $atts, 'bf_button_shortcode' );

	$icon = '';
	$classes = array();

	// Button Icon
	if ( ! empty( $atts[ 'button_icon' ] ) ) {
		$icon = '<i class="fa fa-' . $atts[ 'button_icon' ] . '"></i> ';
	}

	// Button Color
	if ( ! empty( $atts[ 'button_color' ] ) ) {
		array_push( $classes, 'btn-' . $atts[ 'button_color' ] );
	}

	// Button Size
	if ( ! empty( $atts[ 'button_size' ] ) ) {
		array_push( $classes, 'btn-' . $atts[ 'button_size' ] );
	}

	// Lightbox
	if (  '_lightbox' == $atts[ 'button_target' ] ) {
		array_push( $classes, 'bf-lightbox-iframe' );
	}

	$classes = implode( ' ', $classes );

	// Output Link Markup
	$output = '<span class="' . $classes . '"><a href="' . $atts[ 'button_link' ] . '" rel="' . $atts[ 'button_rel' ] . '" target="' . $atts[ 'button_target' ] . '" class=" ' . $atts[ 'button_style' ] . '">' . $icon . $atts[ 'button_text' ] . '</a></span>';

	return $output;

}

add_shortcode( 'bf_button', __NAMESPACE__ . '\bf_button_shortcode' );