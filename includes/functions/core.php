<?php
namespace BrightFire\Theme\Stellar;

/**
 * Set up theme defaults and register supported WordPress features.
 *
 * @since 0.1.0
 *
 * @uses add_action()
 *
 * @return void.
 */
function setup() {
	$n = function ( $function ) {
		return __NAMESPACE__ . "\\$function";
	};

	add_action( 'init', $n( 'i18n' ) );
	add_action( 'init', $n( 'theme_settings' ) );
	add_action( 'wp_loaded', $n( 'bf_add_scripts_styles_actions' ) );
	add_action( 'wp_enqueue_scripts', $n( 'scripts' ) );
	add_action( 'wp_enqueue_scripts', $n( 'styles' ) );

	add_action( 'customize_save_after', $n( 'recompile_scss' ), 10, 1);
	add_action( 'after_setup_theme', $n( 'check_for_css' ), 50 );

	// Customize Register
	add_action( "customize_register", __NAMESPACE__ . '\stellar_customize_register' );

	// Customizer Object Scripts
	add_action( 'customize_preview_init', __NAMESPACE__ . '\stellar_customize_preview_init' );
	add_action( 'customize_controls_enqueue_scripts', __NAMESPACE__ . '\stellar_customize_controls_enqueue_scripts' );

	// Adds palette vars to admin screens
	add_action( 'admin_print_scripts', __NAMESPACE__ . '\color_picker_palettes' );

	// Adds palette vars to customizer screen
	add_action('customize_controls_print_footer_scripts', __NAMESPACE__ . '\color_picker_palettes' );

	// Enable Field Label Visibility Settings in Gravity Forms
	add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

	// Body Classes
	add_filter( 'body_class', __NAMESPACE__ . '\stellar_body_classes' );
	
}

/**
 *
 * Outputs the primary structure of the theme
 *
 */
function load() {

	get_header();

	load_layout_template();
	
	get_footer();

}

/**
 * Makes WP Theme available for translation.
 *
 * Translations can be added to the /lang directory.
 * If you're building a theme based on WP Theme, use a find and replace
 * to change 'wptheme' to the name of your theme in all template files.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 *
 * @since 0.1.0
 *
 * @return void.
 */
function i18n() {
	load_theme_textdomain( 'bf_stellar', BF_STELLAR_PATH . '/languages' );
 }

/**
 * Enqueue scripts for front-end.
 *
 * @uses wp_enqueue_script() to load front end scripts.
 *
 * @since 0.1.0
 *
 * @return void.
 */
function scripts( $debug = false ) {

	// Theme Scripts
	$theme = get_theme_mod( 'theme', false );

	$path = BF_STELLAR_URL . '/assets/js/brightfire-stellar.min.js';

	if( $theme ) {
		$path = BF_STELLAR_URL . '/themes/' . $theme . '/compiled.min.js';
	}

	wp_enqueue_script(
		'bf_stellar',
		$path,
		array(),
		BF_STELLAR_VERSION
	);

	admin_bar_scripts();
}

/**
 * Enqueue styles for front-end.
 *
 * @uses wp_enqueue_style() to load front end styles.
 *
 * @since 0.1.0
 *
 * @return void.
 */
function styles() {

	$site_css_url = site_stellar_css_url();

	if( is_customize_preview() ) {
		$ver = time();
	} else {
		$ver = BF_STELLAR_VERSION;
	}

	wp_enqueue_style(
		'bf_stellar_site',
		$site_css_url,
		array(),
		$ver
	);

	wp_dequeue_style('brightfire-reviews');

}

function theme_settings() {

	// Don't add wrap shortcode in paragraph tags
	add_filter( 'widget_text', 'shortcode_unautop' );

	// Allow shortcodes in text widgets and widget titles - http://sillybean.net/2010/02/using-shortcodes-everywhere/
	add_filter( 'widget_text', 'do_shortcode' );
	add_filter( 'widget_title', 'do_shortcode');

	// We need some theme supports
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'customize-selective-refresh-widgets' );
}

function child_theme_functions() {
	$theme = get_theme_mod( 'theme', false );

	if( file_exists( BF_STELLAR_PATH . "themes/" . $theme . "/functions.php" ) ) {
		require_once( BF_STELLAR_PATH . "themes/" . $theme . "/functions.php" );
	}
}

/**
 * Add humans.txt to the <head> element.
 *
 * @uses apply_filters()
 *
 * @since 0.1.0
 *
 * @return void.
 */
function header_meta() {
	$humans = '<link type="text/plain" rel="author" href="' . BF_STELLAR_TEMPLATE_URL . '/humans.txt" />';

	echo apply_filters( 'bf_stellar_humans', $humans );
}


/**
 * Gives us action hooks to register BrightFire theme scripts and styles at the proper time
 */
function bf_add_scripts_styles_actions() {

	do_action( 'bf_register_scripts' );
	do_action( 'bf_register_styles' );

}

/**
 * Make sure ALL of our default sass variables are saved so we can use them with confidence
 */
function ensure_default_sass_vars() {

	$defaults = array(
		'primary-color'        => '#1499D3',
		'secondary-color'      => '#4D6684',
		'tertiary-color'       => '#3D3D3D',
		'accent-color'         => '#bf1111',
		'header-font-family'   => '$helvetica',
		'base-font-family'     => '$helvetica',
		'accent-font-family'   => '$helvetica',
		'base-font-size'       => '16',
		'font-ratio'           => '$major-third',
		'base-border-radius'   => '3',
		'button-border-radius' => '$base-border-radius',
		'form-border-radius'   => '$base-border-radius',
		'max-width'            => '1200',
		'gutter'               => '1',
		'breakpoint'           => '768',
		'custom1-color'        => '#8f1111',
		'custom2-color'        => '#4f1111'
	);

	foreach ( $defaults as $mod => $value ) {
		if ( empty( get_theme_mod( $mod ) ) ) {
			set_theme_mod( $mod, $value );
		}
	}
}

/**
 * Compiles individual site CSS.
 *
 * @param null $customizer
 *
 * @return string
 */
function compile_site_scss( $customizer = null ) {

	// Theme switching hook
	if ( true == get_theme_mod( 'stellar_theme_switched' ) ) {
		do_action( 'before_compile_site_scss' );
		remove_theme_mod( 'stellar_theme_switched' );
	}

	// Override fields
	global $theme_section_fields;
	$theme_fields = $theme_section_fields[ 'bf_stellar_theme' ][ 'fields' ];
	unset( $theme_fields[ 'theme' ], $theme_fields[ 'build-css'] );

	// Check for a theme and get it's name
	$theme = get_theme_mod( 'theme', false );
	
	$theme_vars = $theme_scss = $custom_vars = '';

	ensure_default_sass_vars();

	// Run through override fields and setup sass variables
	foreach ( $theme_fields  as $setting => $properties ) {

		// Gets our value from the database, or defaults from fields
		$value = get_theme_mod( $setting );

		// If this is the customizer, get our value from there
		if ( $customizer ) {
			$value = $customizer[ $setting ];
		}

		// Filter our values for certain variables
		if ( !empty( $value ) ) {
			switch ( $setting ){
				case 'header-font-family':
				case 'base-font-family':
				case 'accent-font-family':
					if ( array_key_exists( $value, font_choices_array() ) ) {
						$value = font_choices_array()[ $value ][ 'stack' ];
					}
					break;
				case 'base-font-size':
				case 'base-border-radius':
				case 'max-width':
				case 'gutter':
				case 'breakpoint':
					$value = $value . 'px';
					break;
			}
		}

		// Only set our sass variables if we have a value
		if ( ! in_array( $setting, array( 'build-css', 'theme' ) ) ) {
			$custom_vars .= '$' . $setting . ":{$value};\r\n";
		};
	}

	$cols_gutters = get_column_width( $customizer );

	// Sass framework
	$core =  file_get_contents( BF_STELLAR_PATH . 'assets/css/sass/brightfire/_core.scss' );

	// Ready Classes for use in with site specific Admin styles
	$ready_classes = file_get_contents( BF_STELLAR_PATH . 'assets/css/sass/brightfire/_ready_classes.scss' );

	// Base set of variables for use in our theme
	$stellar_vars =  file_get_contents( BF_STELLAR_PATH . 'assets/css/sass/brightfire/stellar-vars.scss' );

	// Base theme
	$stellar =  file_get_contents( BF_STELLAR_PATH . 'assets/css/sass/brightfire/stellar.scss' );

	// Theme
	if ( 'default' != $theme && '' != $theme ) {
		$theme_vars =  file_get_contents( BF_STELLAR_PATH . 'themes/' . $theme . '/vars.scss' );
		$theme_scss =  file_get_contents( BF_STELLAR_PATH . 'themes/' . $theme . '/styles.scss' );
	}

	// Site theme compile
	$compile = $cols_gutters . $core . $custom_vars . $stellar_vars . $theme_vars . $stellar . $theme_scss;

	// Site admin compile
	$admin = $core . $custom_vars . $stellar_vars . $ready_classes;

	/*
	 * Fire up our compiler
	 */
	require_once( BF_STELLAR_INC . 'scssphp/scss.inc.php' );
	$scss = new \Leafo\ScssPhp\Compiler();

	// Gives compiler context for our @imports
	$scss->setImportPaths( array(
		BF_STELLAR_PATH . 'assets/css/sass/brightfire/', 
	) );

	// Crunched basically minifies the output http://leafo.github.io/scssphp/docs/#output-formatting
	$scss->setFormatter( 'Leafo\ScssPhp\Formatter\Crunched' );

	// Site Theme CSS compiled
	$compiled = $scss->compile( $compile );

	// Send off the CSS to the customizer
	if( $customizer ) {

		$style = $compiled;

		return $style;

	} else {

		// Site Admin CSS compiled
		$admin_compiled = $scss->compile( $admin );

		// Final files saved
		file_put_contents( site_stellar_css_path() , $compiled );
		file_put_contents( site_stellar_css_path( true ) , $admin_compiled );
	}
}

/**
 * Get the path of the stellar.css file for the current site
 *
 * @return string
 */
function site_stellar_css_path( $admin = false ) {

	$upload_dir = wp_upload_dir();
	$site_upload_dir = $upload_dir['basedir'];
	if ( $admin ) {
		return trailingslashit( $site_upload_dir ) . "stellar-admin.css";
	} else {
		return trailingslashit( $site_upload_dir ) . "stellar.css";
	}
}

/**
 * Deletes the compiled stellar.css file for the current site
 * @uses site_stellar_css_path
 */
function delete_stellar_css() {
	unlink( site_stellar_css_path() );
}

function site_stellar_css_url( $admin = false ) {

	// We can't simply use wp_upload_dir because it doesn't return the forced SSL if the url is on
	// insurance.brightfiregroup.com so we have to do a little extra work to make it function properly

	// Get the full upload URL which includes the site id but doesn't contain SSL if on insurance.brightfiregroup.com
	$upload_dir = wp_upload_dir();
	$site_upload_url = $upload_dir['baseurl'];

	// Look for everything past /wp-content ( /wp-content/uploads/sites/[site_id])
	$regex = "/(\\/wp-content.*)/";
	preg_match($regex, $site_upload_url, $matches);

	// site_url does contain SSL if we're on a site without a mapped domain
	$site_url = site_url();

	if ( $admin ) {
		return $site_url . $matches[1] . "/stellar-admin.css";
	} else {
		return $site_url . $matches[ 1 ] . "/stellar.css";
	}
}

function check_for_css() {

	if ( ! file_exists( site_stellar_css_path() ) ) {
		compile_site_scss();
	}
}


function recompile_scss( $wp_customize_manager ) {

	// A little song and dance to get updated values
	$instance = $wp_customize_manager;
	$updated = $instance->unsanitized_post_values();

	global $theme_section_fields;
	$theme_fields = $theme_section_fields[ 'bf_stellar_theme' ][ 'fields' ];

	foreach( $updated as $key => $value ) {

		if ( array_key_exists( $key, $theme_fields ) ) {
			compile_site_scss();
			break;
		}
	}
}

function get_column_width( $customizer = null ) {

	$base_font_size = get_theme_mod( 'base-font-size' );
	$gutter = get_theme_mod( 'gutter' );
	$max_width = get_theme_mod( 'max-width' );

	if( $customizer ) {
		$base_font_size = $customizer[ 'base-font-size' ];
		$gutter = $customizer[ 'gutter' ];
		$max_width = $customizer[ 'max-width' ];
	}

	// Set column width to adjust gutter
	$base_spacing = $base_font_size * 1.75; // same as in stellar_vars.scss but we need it before bourbon/neat is loaded
	$gutter_width = $base_spacing * $gutter; // approximates padding, margin and line height options

	$column_ratio = (( $max_width/$gutter_width ) - 11 ) / 12 ; // This is where the magic happens

	//	gutter must be set here with 1em since size of column and gutter are relative to one another
	return '$gutter: 1em !global;$column: '. $column_ratio .'em !global;';

}
