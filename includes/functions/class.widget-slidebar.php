<?php

class Stellar_Slidebar_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {

		parent::__construct(
			'bf_slidebar_slider', // Base ID
			__( 'BrightFire Slidebar&trade; Slider', 'bf_stellar' ),
			array(
				'description'                 => __( 'BrightFire Slidebar&trade; Slider', 'bf_stellar', '' ),
				'customize_selective_refresh' => true,
			)
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {

		$data_atts = array(
			'height',
			'animation',
			'slideshowSpeed',
			'animationSpeed',
			'directionNav',
		);

		$instance[ 'directionNav' ] = 'true';

		$data_attributes = '';

		foreach ( $data_atts as $attribute ) {

			$data = '';

			if ( isset( $instance[ $attribute ] ) ) {
				$data = $instance[ $attribute ];
			}

			$data_attributes .= " data-{$attribute}='{$data}'";
		}

		echo $args[ 'before_widget' ];
		echo "<div class='brightfire-slidebar'{$data_attributes}'><ul class='slides'>";
		if ( isset( $instance[ 'slides' ] ) ) {
			foreach ( $instance[ 'slides' ] as $widget_area ) {

				$defaults = array(
					'slidebar'         => '',
					'color'            => '',
					'color_bottom'     => '',
					'color_stop'        => '0',
					'color_bottom_stop' => '100',
					'gradient_angle'    => '180',
					'image_id'         => '',
					'image_attachment' => '',
					'image_size'       => '',
					'image_repeat'     => '',
					'image_position'   => '',
					'slide_padding'    => 'no',
					'widget_position'  => '',
				);

				$widget_area = wp_parse_args( $widget_area, $defaults );

				$slide_styles = \BrightFire\Theme\Stellar\background_style_generator( $widget_area );

				$slide_classes = 'widget-position-' . $widget_area['widget_position'];

				if( 'yes' == $widget_area['slide_padding'] ) {
					$slide_classes .= ' slide-padding';
				}

				echo '<li style="' . $slide_styles . '" class="' . $slide_classes . '">';
					echo '<div class="slidebar-widgets-wrap">';
						echo '<div class="slidebar-widgets">';
							dynamic_sidebar( $widget_area[ 'slidebar' ] );
						echo '</div>';
					echo '</div>';
				echo '</li>';
			}
		}
		echo '</ul></div>';
		echo $args[ 'after_widget' ];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$global_fields = array(
			'fields'          => array(
				'height'         => array(
					'label'       => 'Height',
					'description' => 'Number of pixels. This is not required.',
					'type'        => 'number',
					'permit'      => 1,
				),
				'animation'      => array(
					'label'   => 'Animation',
					'type'    => 'selectize',
					'choices' => array(
						'fade'      => 'Fade',
						'crossfade' => 'Cross Fade',
						'slide'     => 'Slide',
					),
					'permit'  => 1,
				),
				'slideshowSpeed' => array(
					'label'         => 'Slide Duration',
					'type'          => 'range',
					'default_value' => 7,
					'permit'        => 1,
					'atts'          => array(
						'min'  => 0,
						'max'  => 7,
						'step' => .1
					),
					'settings'      => array(
						'min-label'    => 0,
						'max-label'    => 7,
						'units'        => 'Seconds',
						'show-current' => true,
					),
				),
				'animationSpeed' => array(
					'label'         => 'Transition Speed',
					'type'          => 'range',
					'default_value' => .6,
					'permit'        => 1,
					'atts'          => array(
						'min'  => 0,
						'max'  => 7,
						'step' => .1
					),
					'settings'      => array(
						'min-label'    => 0,
						'max-label'    => 7,
						'units'        => 'Seconds',
						'show-current' => true,
					),
				),
			),
			'instance'        => $instance,
			'widget_instance' => $this,
			'display'         => 'basic',
			'echo'            => false
		);

		echo \BF_Admin_API_Fields::bf_admin_api_fields_build( $global_fields );

		$field_prefix = $this->get_field_name( 'slides' );
		$slides       = array();

		if ( isset( $instance[ 'slides' ] ) ) {
			foreach ( $instance[ 'slides' ] as $index => $values ) {

				$id_prefix = "{$field_prefix}[{$index}]";

				$slides[ $index ]['title_bar'] = \BrightFire\Theme\Stellar\slidebar_background_title( $values );
				$slides[ $index ]['fields']    = \BrightFire\Theme\Stellar\slidebar_background_fields( $id_prefix, $values );
			}
		}

		$slidebar_fields = array(
			'slides' => array(
				'type' => 'repeater',
				'instance' => $slides,
				'title_cb' => array(
					'function' => '\BrightFire\Theme\Stellar\slidebar_background_title',
					'params' => array(),
				),
				'fields_cb' => array(
					'function' => '\BrightFire\Theme\Stellar\slidebar_background_fields',
					'params' => array( $field_prefix )
				),
				'js_cb' => array( 'slideWidget', 'init_colorPickers', 'init_selectize' ),
				'collapse' => true,
				'sortable' => true,
				'add_label' => 'Add Slidebar Below',
				'del_label' => 'Remove Slidebar',
				'sanitize' => 'bypass',

			),
		);

		// Build our widget form
		$args = array(
			'fields'          => $slidebar_fields,
			'display'         => 'basic',
			'echo'            => true,
			'instance'        => $instance,
		);
		\BF_Admin_API_Fields::bf_admin_api_fields_build( $args );
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance[ 'height' ]         = $new_instance[ 'height' ];
		$instance[ 'animation' ]      = $new_instance[ 'animation' ];
		$instance[ 'slideshowSpeed' ] = $new_instance[ 'slideshowSpeed' ];
		$instance[ 'animationSpeed' ] = $new_instance[ 'animationSpeed' ];
		$instance[ 'slides' ]         = $new_instance[ 'slides' ];

		return $instance;
	}
}

function register_stellar_slidebar_widget() {

	register_widget( 'Stellar_Slidebar_Widget' );

}

add_action( 'widgets_init', 'register_stellar_slidebar_widget' );