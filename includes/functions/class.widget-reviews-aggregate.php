<?php

namespace BrightFire\Theme\Stellar;


if ( ! class_exists( 'WP_Widget' ) ) {
	return;
}


/**
 * Class BrightFire_Reviews_Aggregate
 */
class BrightFire_Reviews_Aggregate extends \WP_Widget {

	public $defaults = array(
		'title'          => '',
		'rating_color'   => 'rating-gold',
	);

	function __construct() {

		// Setup Parent Globals, etc
		$widget_ops = array(
			'classname'                   => 'brightfire-reviews-aggregate-widget',
			'description'                 => __( 'Outputs Reviews Aggregate' ),
			'customize_selective_refresh' => true,
		);

		parent::__construct( 'brightfire_reviews_aggregate', __( 'BrightFire Reviews Aggregate Widget' ), $widget_ops );
	}

	/**
	 * Widget: Output our widget
	 *
	 * @param array $args
	 * @param array $instance
	 *
	 * @return bool
	 */
	public function widget( $args, $instance ) {

		$instance = wp_parse_args( $instance, $this->defaults );

		// BEFORE WIDGET
		echo $args['before_widget'];

		// TITLE
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . do_shortcode( $instance['title'] ) . $args['after_title'];
		}

		// REVIEWS AGGREGATE
		echo do_shortcode('[bf_stellar_reviews_aggregate rating_color="' . $instance['rating_color'] . '"]');

		// AFTER WIDGET
		echo $args['after_widget'];

		// return
		return true;
	}

	/**
	 * Form: Output our widget options
	 *
	 * @param array $instance
	 *
	 * @return bool
	 */
	public function form( $instance ) {

		// Get our Fields
		$fields   = $this->widget_define_fields();
		$instance = wp_parse_args( $instance, $this->defaults );

		// Build our widget form
		$args = array(
			'fields'          => $fields,
			'display'         => 'basic',
			'echo'            => true,
			'widget_instance' => $this,
			'instance'        => $instance
		);
		\BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

		// return
		return true;

	}

	/**
	 * Update Widget: Save our instance
	 *
	 * @param array $new_instance
	 * @param array $old_instance
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['title']          = $new_instance['title'];      // Widget Title
		$instance['rating_color']          = $new_instance['rating_color'];      // Widget Title

		return $instance;
	}

	/**
	 * Widget Fields: Field define for our widget form
	 * @return array
	 */
	function widget_define_fields() {
		return array(
			'title'          => array(
				'type'   => 'text',
				'label'  => 'Title',
				'permit' => 1
			),
			'rating_color'   => array(
				'type' => 'selectize',
				'label' => 'Star Rating Color',
				'choices' => array(
					'rating-gold' => 'Gold',
					'rating-light' => 'Light',
					'rating-dark' => 'Dark',
					'rating-primary' => 'Primary',
					'rating-secondary' => 'Secondary',
					'rating-tertiary' => 'Tertiary',
					'rating-accent' => 'Accent',
				),
				'permit' => 1
			)
		);
	}

}

/**
 * Registers the widget
 * @return bool
 */
function register_reviews_aggregate_widget() {
	register_widget( 'BrightFire\Theme\Stellar\BrightFire_Reviews_Aggregate' );
}
add_action( 'widgets_init', __NAMESPACE__ . '\register_reviews_aggregate_widget' );