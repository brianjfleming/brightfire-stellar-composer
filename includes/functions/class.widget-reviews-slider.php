<?php

namespace BrightFire\Theme\Stellar;


if ( ! class_exists( 'WP_Widget' ) ) {
	return;
}


/**
 * Class BrightFire_Reviews_Content
 */
class BrightFire_Reviews_Slider extends \WP_Widget {

	public $defaults = array(
		'title'          => '',
		'limit'          => '5',
		'rating_color'   => 'rating-gold',
		'show_image'     => 'hide-avatar',
		'social_style' => 'circle'
	);

	function __construct() {

		// Setup Parent Globals, etc
		$widget_ops = array(
			'classname'                   => 'brightfire-reviews-slider-widget',
			'description'                 => __( 'Outputs Reviews Slider' ),
			'customize_selective_refresh' => true,
		);

		parent::__construct( 'brightfire_reviews_slider', __( 'BrightFire Reviews Slider Widget' ), $widget_ops );
	}

	/**
	 * Widget: Output our widget
	 *
	 * @param array $args
	 * @param array $instance
	 *
	 * @return bool
	 */
	public function widget( $args, $instance ) {

		$instance = wp_parse_args( $instance, $this->defaults );

		// BEFORE WIDGET
		echo $args['before_widget'];

		// TITLE
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . do_shortcode( $instance['title'] ) . $args['after_title'];
		}

		echo do_shortcode('[bf_stellar_reviews limit="' .  $instance['limit'] . '" slider="true" show_image="' .  $instance['show_image'] . '" social_style="' .  $instance['social_style'] . '" rating_color="' . $instance['rating_color'] . '"]');

		// AFTER WIDGET
		echo $args['after_widget'];

		// return
		return true;
	}

	/**
	 * Form: Output our widget options
	 *
	 * @param array $instance
	 *
	 * @return bool
	 */
	public function form( $instance ) {

		// Get our Fields
		$fields   = $this->widget_define_fields();
		$instance = wp_parse_args( $instance, $this->defaults );

		// Build our widget form
		$args = array(
			'fields'          => $fields,
			'display'         => 'basic',
			'echo'            => true,
			'widget_instance' => $this,
			'instance'        => $instance
		);
		\BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

		// return
		return true;

	}

	/**
	 * Update Widget: Save our instance
	 *
	 * @param array $new_instance
	 * @param array $old_instance
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['title']          = $new_instance['title'];      // Widget Title
		$instance['limit']          = $new_instance['limit'];      // Limit
		$instance['show_image']          = $new_instance['show_image'];      // Show Reviewer Avatar
		$instance['social_style']          = $new_instance['social_style'];      // Show Reviewer Avatar
		$instance['rating_color']          = $new_instance['rating_color'];      // Color

		return $instance;
	}

	/**
	 * Widget Fields: Field define for our widget form
	 * @return array
	 */
	function widget_define_fields() {
		return array(
			'title'          => array(
				'type'   => 'text',
				'label'  => 'Title',
				'permit' => 1
			),
			'limit'          => array(
				'type'   => 'number',
				'label'  => 'Reviews to show',
				'permit' => 1
			),
			'show_image' => array(
				'type' => 'selectize',
				'label' => 'Show Reviewer Avatar?',
				'choices' => array(
					'show-avatar' => 'Yes',
					'hide-avatar' => 'No',
				),
				'permit' => 1
			),
			'rating_color'   => array(
				'type' => 'selectize',
				'label' => 'Star Rating Color',
				'choices' => array(
					'rating-gold' => 'Gold',
					'rating-light' => 'Light',
					'rating-dark' => 'Dark',
					'rating-primary' => 'Primary',
					'rating-secondary' => 'Secondary',
					'rating-tertiary' => 'Tertiary',
					'rating-accent' => 'Accent',
				),
				'permit' => 1
			),
			'social_style' => array(
				'type' => 'selectize',
				'label' => 'Social Icon Style',
				'choices' => array(
					'circle' => 'Circle',
					'square' => 'Square',
					'logo' => 'Logo'
				),
				'permit' => 1
			),
		);
	}

}

/**
 * Registers the widget
 * @return bool
 */
function register_reviews_slider_widget() {
	unregister_widget( 'BrightFire_Reviews_Widget' );
	register_widget( 'BrightFire\Theme\Stellar\BrightFire_Reviews_Slider' );
}
add_action( 'widgets_init', __NAMESPACE__ . '\register_reviews_slider_widget' );