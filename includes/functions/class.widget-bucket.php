<?php

namespace BrightFire\Theme\Stellar;


if ( ! class_exists( 'WP_Widget' ) ) {
	return;
}


/**
 * Class BrightFire_Bucket_Widget
 */
class BrightFire_Bucket_Widget extends \WP_Widget {

	public $defaults = array();
	public $block_defaults = array();

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {

		// Define Defaults
		$this->defaults       = define_defaults();
		$this->block_defaults = define_block_defaults();

		// Setup Parent Globals, etc
		$widget_ops = array(
			'classname'                   => 'brightfire-bucket-widget',
			'description'                 => __( 'Headings, Text, Images, Buttons, Links. This is your favorite widget to build callouts with!' ),
			'customize_selective_refresh' => true
		);
		parent::__construct( 'brightfire_bucket_widget', __( 'BrightFire Bucket Widget' ), $widget_ops );

	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {

		// Parse our instance into our defaults
		$instance = wp_parse_args( $instance, $this->defaults );

		echo $args['before_widget'];


		$bucket_target   = ( $instance['link_target'] == '_lightbox' ) ? '' : $instance['link_target'];
		$bucket_lightbox = ( $instance['link_target'] == '_lightbox' ? ' bf-lightbox-iframe' : '' );
		$bucket_link     = parse_url_override( $instance['link_wrap'], $instance['link_wrap_url'] );

		if ( ! empty( $instance['image_id'] ) ) {

			$layer = array(
				'image_id'         => $instance['image_id'],
				'image_attachment' => 'scroll',
				'image_repeat'     => 'no-repeat',
				'image_size'       => 'cover',
				'image_position'   => 'center center',
			);

			$background = background_style_generator( $layer );
			echo "<div class='background-layer' style='{$background}'></div>";
		}

		if ( ! empty( $bucket_link ) ) {
			echo '<a href="' . do_shortcode( $bucket_link ) . '" rel="' . $instance['link_rel'] . '" target="' . $bucket_target . '" class="bucket-link' . $bucket_lightbox . '"></a>';
		}

		// TITLE
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . do_shortcode( $instance['title'] ) . $args['after_title'];
		}

		foreach ( $instance['blocks'] as $block ) {

			$block = wp_parse_args( $block, $this->block_defaults );

			$global_classes = $desktop_classes = $mobile_classes = $block_class = '';

			foreach ( array( 'heading', 'text', 'image', 'button' ) as $type ) {
				if ( $type == $block['block_type'] ) {
					$global_classes  = $block[ $type . '_global_classes' ];
					$desktop_classes = $block[ $type . '_desktop_classes' ];
					$mobile_classes  = $block[ $type . '_mobile_classes' ];
					$block_class     = array( 'bf-bucket-' . $type );

					$desktop_classes = array_map( function ( $class ) {
						return 'd-' . $class;
					}, $desktop_classes );
					$mobile_classes  = array_map( function ( $class ) {
						return 'm-' . $class;
					}, $mobile_classes );
				}
			}

			$block_classes = array_merge( $global_classes, $desktop_classes, $mobile_classes, $block_class );

			$block_classes = implode( ' ', $block_classes );

			echo "<div class='bucket-widget-block {$block_classes}'>";

			switch ( $block['block_type'] ) {

				/**
				 * Heading
				 */
				case 'heading' :
					echo "<{$block[ 'block_heading_size' ]}>" . do_shortcode( $block['block_heading'] ) . "</{$block[ 'block_heading_size' ]}>";
					break;

				/**
				 * Text
				 */
				case 'text' :
					echo do_shortcode( $block['block_text'] );
					break;

				/**
				 * Image/Icon
				 */
				case 'image' :

					$output = '';

					// Check if we already have a link on the main Bucket
					if ( empty( $bucket_link ) ) {


						$image_target   = ( $block['image_target'] == '_lightbox' ? '' : $block['image_target'] );
						$image_lightbox = ( $block['image_target'] == '_lightbox' ? 'bf-lightbox-iframe' : '' );
						$image_link     = parse_url_override( $block['image_link'], $block['image_link_url'] );

						// Check for link on this block
						if ( ! empty( $image_link ) ) {
							$output .= '<a href="' . $image_link . '" target="' . $image_target . '" rel="' . $block['image_rel'] . '" class="' . $image_lightbox . '">';
						}
					};

					// Icon
					if ( 'icon' == $block['image_type'] ) {

						$output .= '<i class="fa fa-' . $block['image_icon'] . ' ' . $block['icon_size'] . '" aria-hidden="true"></i>';
					}

					// Image
					if ( 'image' == $block['image_type'] && ( $block['image_id'] || $block['featured_image'] ) ) {

						$post_id = get_queried_object_id();

						if ( isset( $block['featured_image'] ) && 'yes' == $block['featured_image'] && has_post_thumbnail( $post_id ) ) {
							$block['image_id'] = get_post_thumbnail_id( $post_id );
						}

						$image_id = $block['image_id'];
						$src      = ( is_numeric( $image_id ) ) ? wp_get_attachment_url( $image_id ) : get_stylesheet_directory_uri() . '/images/missing-image.png';
						$srcset   = wp_get_attachment_image_srcset( $image_id, 'full' );
						$alt      = get_post_meta( $image_id, '_wp_attachment_image_alt', true );

						if ( ! empty( $block['image_width'] ) ) {

							$sizes = "{$block[ 'image_width' ]}px";

						} else {
							// Generate sensible sizes attribute based on the widget structure size
							$structure_classes = array(
								'one-half'      => 50,
								'one-third'     => 33.3333,
								'one-fourth'    => 25,
								'one-fifth'     => 20,
								'two-thirds'    => 66.6667,
								'two-fifths'    => 40,
								'three-fourths' => 75,
								'three-fifths'  => 60,
								'four-fifths'   => 80,
								'full-width'    => 100,
							);

							$widget_width = str_replace( '-last', '', $instance['structure_class'] );
							$breakpoint   = get_theme_mod( 'breakpoint', 768 );

							// We'll use the corresponding widget width's percentage to suggest the image size to use if we're above
							// our breakpoint. If we're below the breakpoint, we'll use the image that is closest to the viewport
							$sizes = "(min-width: {$breakpoint}px) {$structure_classes[$widget_width]}vw, 100vw";
						}

						$output .= '<div class="' . $block['image_height'] . '">';
						$output .= '<img src="' . $src . '" alt="' . $alt . '" srcset="' . $srcset . '" sizes="' . $sizes . '"/>';
						$output .= '</div>';
					}

					if ( empty( $bucket_link ) ) {

						// Close link
						$output .= ( ! empty( $image_link ) ) ? '</a>' : '';

					}

					echo $output;
					break;

				/**
				 * Button
				 */
				case 'button' :

					$button_target = $button_rel = '';
					$button_link   = parse_url_override( $block['button_link'], $block['button_link_url'] );

					if ( ! empty( $button_link ) && empty( $bucket_link ) ) {
						$button_link   = 'button_link="' . $button_link . '"';
						$button_target = 'button_target="' . $block['button_target'] . '"';
						$button_rel    = 'button_rel="' . $block['button_rel'] . '"';
					}

					// Set up shortcode vars
					$button_text  = 'button_text="' . esc_html( $block['button_text'] ) . '"';
					$button_icon  = 'button_icon="' . $block['button_icon'] . '"';
					$button_style = 'button_style="' . $block['button_style'] . '"';

					echo do_shortcode( "[bf_button $button_link $button_target $button_rel $button_text $button_icon $button_style]" );

					break;

				// Default: DO NOTHING
				default :
					break;
			}

			echo '</div>';

		}

		echo $args['after_widget'];


	}

	/**
	 * Build our Widget Form
	 *
	 * @param array $instance
	 *
	 * @return bool
	 */
	public function form( $instance ) {

		$field_prefix = $this->get_field_name( 'blocks' );

		$blocks = array();

		// Set up the saved data for use in our repeater fields
		if ( isset( $instance['blocks'] ) ) {
			foreach ( $instance['blocks'] as $index => $values ) {
				$id_prefix                     = "{$field_prefix}[{$index}]";
				$blocks[ $index ]['title_bar'] = bucket_repeater_title();
				$blocks[ $index ]['fields']    = bucket_repeater_fields( $id_prefix, $values );
			}
		}

		// Get our Fields
		$fields = widget_define_bucket_fields( $field_prefix, $blocks, $this->block_defaults );

		// Build our widget form
		$args = array(
			'fields'          => $fields,
			'display'         => 'basic',
			'echo'            => true,
			'widget_instance' => $this,
			'instance'        => $instance
		);
		\BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

		// Widget Form JS
		?>
		<script type="text/javascript">

			$('div[data-type="bucket_repeater"] select[data-type="block_type"]').each(function () {
				setBucketFormStates($(this));
			});

			$(document).on('change', 'div[data-type="bucket_repeater"] select[data-type="block_type"]', function () {
				setBucketFormStates($(this));
			});

			$('select[data-type="image_icon_type"]').each(function () {
				setBucketImageFormStates($(this));
			});

			$(document).on('change', 'select[data-type="image_icon_type"]', function () {
				setBucketImageFormStates($(this));
			});

			$('select[data-type="cta_style"]').each(function () {
				setBucketButtonFormStates($(this));
			});

			$(document).on('change', '.type-fields.button-fields select[data-type="cta_style"]', function () {
				setBucketButtonFormStates($(this));
			});

			function setBucketFormStates(element) {
				var type = element.val();

				element.parents('.bf-repeater-inside').find('.type-fields.active').slideUp(250);

				setTimeout(function () {

					element.parents('.bf-repeater-inside').siblings('.bf-repeater-top').find('h3').text(type.toUpperCase());

					switch (type) {
						case 'heading' :
							element.parents('.bf-repeater-inside').find('.heading-fields').slideDown(250).addClass('active');
							break;
						case 'text' :
							element.parents('.bf-repeater-inside').find('.text-fields').slideDown(250).addClass('active');
							break;
						case 'image' :
							element.parents('.bf-repeater-inside').find('.image-fields').slideDown(250).addClass('active');
							var imageSwitch = element.parents('.bf-repeater-inside').find('.image-fields').find('select[data-type="image_icon_type"]');
							setBucketImageFormStates(imageSwitch);
							break;
						case 'button' :
							element.parents('.bf-repeater-inside').find('.button-fields').slideDown(250).addClass('active');
							var buttonSwitch = element.parents('.bf-repeater-inside').find('.button-fields').find('select[data-type="cta_style"]');
							setBucketButtonFormStates(buttonSwitch);
							break;
						default :
							break;
					}
				}, 350);

				return true;

			}

			function setBucketButtonFormStates(element) {

				if ('cta-text-link' == element.val()) {
					element.parents('.form-wrapper').find('select[data-type="button_input"]').parent('.input-wrapper').hide();
					element.parents('.form-wrapper').find('input[data-type="button_input"]').parent('.input-wrapper').hide();

					element.parents('.form-wrapper').find('select[data-type="text_input"]').parent('.input-wrapper').show();
					element.parents('.form-wrapper').find('input[data-type="text_input"]').parent('.input-wrapper').show();
				} else {
					element.parents('.form-wrapper').find('select[data-type="button_input"]').parent('.input-wrapper').show();
					element.parents('.form-wrapper').find('input[data-type="button_input"]').parent('.input-wrapper').show();

					element.parents('.form-wrapper').find('select[data-type="text_input"]').parent('.input-wrapper').hide();
					element.parents('.form-wrapper').find('input[data-type="text_input"]').parent('.input-wrapper').hide();
				}

				return true;
			}

			function setBucketImageFormStates(element) {

				if ('image' == element.val()) {
					element.parents('.form-wrapper').find('input[data-type="image_input"]').parent('div.input-wrapper').show();
					element.parents('.form-wrapper').find('select[data-type="image_input"]').parent('div.input-wrapper').show();

					element.parents('.form-wrapper').find('input[data-type="icon_input"]').parent('div.input-wrapper').hide();
					element.parents('.form-wrapper').find('select[data-type="icon_input"]').parent('div.input-wrapper').hide();
				} else {
					element.parents('.form-wrapper').find('input[data-type="image_input"]').parent('div.input-wrapper').hide();
					element.parents('.form-wrapper').find('select[data-type="image_input"]').parent('div.input-wrapper').hide();

					element.parents('.form-wrapper').find('input[data-type="icon_input"]').parent('div.input-wrapper').show();
					element.parents('.form-wrapper').find('select[data-type="icon_input"]').parent('div.input-wrapper').show();

				}

				return true;

			}

		</script>

		<?php

		return true;
	}

	/**
	 * @param array $new_instance
	 * @param array $old_instance
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['blocks']           = $new_instance['blocks'];      // Blocks Array
		$instance['title']            = $new_instance['title']; // Title
		$instance['link_wrap']        = $new_instance['link_wrap'];   // Link Entire bucket?
		$instance['link_wrap_url']    = $new_instance['link_wrap_url'];   // Link Entire bucket?
		$instance['link_rel']         = $new_instance['link_rel'];    // rel attribute
		$instance['link_target']      = $new_instance['link_target']; // _target attribute
		$instance['image_id']         = $new_instance['image_id']; // _target attribute


		return $instance;
	}

}

/**
 * Registers the widget
 * @return bool
 */
function register_brightfire_bucket_widget() {
	register_widget( 'BrightFire\Theme\Stellar\BrightFire_Bucket_Widget' );
}

add_action( 'widgets_init', __NAMESPACE__ . '\register_brightfire_bucket_widget' );

/**
 * Default Field Defines for BF Bucket Widget
 * @return array
 */
function widget_define_bucket_fields( $field_prefix, $instance = array(), $defaults ) {

	return array(
		'title'            => array(
			'type'  => 'text',
			'label' => 'Title',
		),
		'link_wrap'        => array(
			'type'          => 'wp_page_select',
			'label'         => 'Bucket Link (WP Internal)',
			'default_value' => '',
		),
		'link_wrap_url'    => array(
			'type'          => 'url',
			'label'         => 'Bucket Link (URL Override)',
			'default_value' => '',
		),
		'link_rel'         => array(
			'label'         => 'Link REL',
			'type'          => 'text',
			'default_value' => '',
		),
		'link_target'      => array(
			'label'         => 'Link Target',
			'type'          => 'selectize',
			'choices'       => array(
				'_self'     => 'This Window',
				'_blank'    => 'New Window',
				'_lightbox' => 'Open in Lightbox' // puts class 'bf-lightbox-iframe' on link
			),
			'default_value' => '_self',
		),
		'image_id' => array(
			'label'       => 'Background image',
			'description' => '',
			'type'        => 'wp_image',
		),
		'blocks'           => array( // the field id serves as the JS object namespace for each repeater
			'type'      => 'repeater',
			'instance'  => $instance,
			'title_cb'  => array(
				'function' => '\BrightFire\Theme\Stellar\bucket_repeater_title',
				'params'   => array(),
			),
			'fields_cb' => array(
				'function' => '\BrightFire\Theme\Stellar\bucket_repeater_fields',
				'params'   => array( $field_prefix ),
				'defaults' => $defaults,
			),
			'js_cb'     => array(
				'function' => array( 'init_selectize' ),
			),
			'collapse'  => true,
			'sortable'  => true,
			'add_label' => 'Add Block Below',
			'del_label' => 'Remove Block',
			'sanitize'  => 'bypass',
			'atts'      => array(
				'data-type' => 'bucket_repeater'
			)
		),
	);
}

/**
 * Bucket Repeater: Title CallBack
 * @return string
 */
function bucket_repeater_title() {
	return "<h3>NONE</h3>"; // Default State
}

/**
 * Bucket Repeater: Fields CallBack
 *
 * @param $id
 * @param null $values
 *
 * @return string
 */
function bucket_repeater_fields( $id, $values = null ) {

	// Check values before we parse defaults
	$timestamp = '';

	if ( empty( $values ) ) {
		$timestamp = '[block-' . uniqid() . ']';
	}

	$defaults = define_block_defaults();
	$values   = wp_parse_args( $values, $defaults );

	$prefix = $id . $timestamp;

	$output = '';

	// Base Fields
	// -----------------------------------------------------------------
	$fields = array(
		$prefix . '[block_type]' => array(
			'label'    => 'Type',
			'type'     => 'selectize',
			'choices'  => array(
				'none'    => '--Select--',
				'heading' => 'Heading',
				'image'   => 'Image / Icon',
				'text'    => 'Text',
				'button'  => 'Button / Link'
			),
			'sanitize' => 'bypass',
			'atts'     => array(
				'data-type' => 'block_type'
			)
		),
	);

	$fields_instance = array(
		$prefix . '[block_type]' => $values['block_type'],
	);

	// Heading Fields
	// -----------------------------------------------------------------
	$_heading_fields = array(
		$prefix . '[block_heading]'           => array(
			'label'    => 'Heading Text',
			'type'     => 'text',
			'sanitize' => 'wp_kses_post'
		),
		$prefix . '[block_heading_size]'      => array(
			'label'         => 'Heading Type',
			'type'          => 'selectize',
			'choices'       => array(
				'h1' => '&lt;h1&gt;',
				'h2' => '&lt;h2&gt;',
				'h3' => '&lt;h3&gt;',
				'h4' => '&lt;h4&gt;',
				'h5' => '&lt;h5&gt;',
				'h6' => '&lt;h6&gt;',
			),
			'default_value' => 'h2'
		),
		$prefix . '[heading_global_classes]'  => array(
			'label'   => 'Heading Global Classes',
			'type'    => 'selectize',
			'choices' => bf_bucket_heading_classes(),
			'atts'    => array(
				'multiple' => 'multiple',
			),
			'permit'  => 1,
		),
		$prefix . '[heading_desktop_classes]' => array(
			'label'   => 'Heading Desktop Classes',
			'type'    => 'selectize',
			'choices' => bf_bucket_heading_breakpoint(),
			'atts'    => array(
				'multiple' => 'multiple',
			),
			'permit'  => 1,
		),
		$prefix . '[heading_mobile_classes]'  => array(
			'label'   => 'Heading Mobile Classes',
			'type'    => 'selectize',
			'choices' => bf_bucket_heading_breakpoint(),
			'atts'    => array(
				'multiple' => 'multiple',
			),
			'permit'  => 1,
		),
	);

	$heading_instance = array(
		$prefix . '[block_heading]'           => $values['block_heading'],
		$prefix . '[block_heading_size]'      => $values['block_heading_size'],
		$prefix . '[heading_global_classes]'  => $values['heading_global_classes'],
		$prefix . '[heading_desktop_classes]' => $values['heading_desktop_classes'],
		$prefix . '[heading_mobile_classes]'  => $values['heading_mobile_classes']
	);

	// Text Fields
	// -----------------------------------------------------------------
	$_text_fields = array(
		$prefix . '[block_text]'           => array(
			'label'    => 'Text',
			'type'     => 'textarea',
			'sanitize' => 'wp_kses_post'
		),
		$prefix . '[text_global_classes]'  => array(
			'label'   => 'Text Global Classes',
			'type'    => 'selectize',
			'choices' => bf_bucket_text_classes(),
			'atts'    => array(
				'multiple' => 'multiple',
			),
			'permit'  => 1,
		),
		$prefix . '[text_desktop_classes]' => array(
			'label'   => 'Text Desktop Classes',
			'type'    => 'selectize',
			'choices' => bf_bucket_text_breakpoint(),
			'atts'    => array(
				'multiple' => 'multiple',
			),
			'permit'  => 1,
		),
		$prefix . '[text_mobile_classes]'  => array(
			'label'   => 'Text Mobile Classes',
			'type'    => 'selectize',
			'choices' => bf_bucket_text_breakpoint(),
			'atts'    => array(
				'multiple' => 'multiple',
			),
			'permit'  => 1,
		),
	);

	$text_instance = array(
		$prefix . '[block_text]'           => $values['block_text'],
		$prefix . '[text_global_classes]'  => $values['text_global_classes'],
		$prefix . '[text_desktop_classes]' => $values['text_desktop_classes'],
		$prefix . '[text_mobile_classes]'  => $values['text_mobile_classes']
	);

	// Image Fields
	// -----------------------------------------------------------------

	$_image_fields = array(
		$prefix . '[image_link]'            => array(
			'label'         => 'Link (WP Internal)',
			'type'          => 'wp_page_select',
			'default_value' => '',
		),
		$prefix . '[image_link_url]'        => array(
			'label'         => 'Link (URL Override)',
			'type'          => 'url',
			'default_value' => ''
		),
		$prefix . '[image_rel]'             => array(
			'label'         => 'Link REL',
			'type'          => 'text',
			'default_value' => '',
		),
		$prefix . '[image_target]'          => array(
			'label'         => 'Link Target',
			'type'          => 'selectize',
			'choices'       => array(
				'_self'     => 'This Window',
				'_blank'    => 'New Window',
				'_lightbox' => 'Open in Lightbox' // puts class 'bf-lightbox-iframe' on link
			),
			'default_value' => '_self',
		),
		$prefix . '[image_type]'            => array(
			'label'         => 'Image or Icon?',
			'type'          => 'select',
			'choices'       => array(
				'image' => 'Image',
				'icon'  => 'Icon',
			),
			'atts'          => array(
				'data-type' => 'image_icon_type',
			),
			'default_value' => 'image',
		),
		$prefix . '[featured_image]'        => array(
			'label'         => 'Use Featured Image if available?',
			'type'          => 'selectize',
			'atts'          => array( // Hook for JS
				'data-type' => 'image_input'
			),
			'choices'       => yes_no_choices(),
			'default_value' => 'no',
		),
		$prefix . '[image_id]'              => array(
			'label'         => 'Select or Upload Image',
			'type'          => 'wp_image',
			'atts'          => array( // Hook for JS
				'data-type' => 'image_input'
			),
			'default_value' => '',
		),
		$prefix . '[image_icon]'            => array(
			'label'           => 'Select Icon',
			'type'            => 'fontawesome_select',
			'container_class' => 'stellar-icon',
			'atts'            => array( // Hook for JS
				'data-type' => 'icon_input'
			),
			'default_value'   => '',
		),
		$prefix . '[image_height]'          => array(
			'label'         => 'Image Height',
			'type'          => 'selectize',
			'choices'       => array(
				'image-short'  => 'Short',
				'image-medium' => 'Medium',
				'image-tall'   => 'Tall',
				'image-full'   => 'Full',
			),
			'default_value' => 'image-full',
			'atts'          => array(
				'data-type' => 'image_input'
			),
		),
		$prefix . '[image_width]'           => array(
			'label' => 'Force Image Width',
			'type'  => 'number',
			'atts'  => array(
				'data-type' => 'image_input'
			),
		),
		$prefix . '[icon_size]'             => array(
			'label'         => 'Icon Size Multiplier',
			'description'   => 'Multiplies the icon size with the Text Size used.',
			'type'          => 'selectize',
			'choices'       => array(
				'default' => 'Default',
				'fa-lg'   => 'Large (33%)',
				'fa-2x'   => '2x',
				'fa-3x'   => '3x',
				'fa-4x'   => '4x',
				'fa-5x'   => '5x',
			),
			'default_value' => 'default',
			'atts'          => array(
				'data-type' => 'icon_input'
			)
		),
		$prefix . '[image_global_classes]'  => array(
			'label'   => 'Image Global Classes',
			'type'    => 'selectize',
			'choices' => bf_bucket_image_classes(),
			'atts'    => array(
				'multiple' => 'multiple',
			),
			'permit'  => 1,
		),
		$prefix . '[image_desktop_classes]' => array(
			'label'   => 'Image Desktop Classes',
			'type'    => 'selectize',
			'choices' => bf_bucket_image_breakpoint(),
			'atts'    => array(
				'multiple' => 'multiple',
			),
			'permit'  => 1,
		),
		$prefix . '[image_mobile_classes]'  => array(
			'label'   => 'Image Mobile Classes',
			'type'    => 'selectize',
			'choices' => bf_bucket_image_breakpoint(),
			'atts'    => array(
				'multiple' => 'multiple',
			),
			'permit'  => 1,
		),
	);

	// Instance Juggle
	$image_instance = array(
		$prefix . '[image_link]'            => ( isset( $values['image_link'] ) ? $values['image_link'] : '' ),
		$prefix . '[image_link_url]'        => ( isset( $values['image_link'] ) ? $values['image_link_url'] : '' ),
		$prefix . '[image_type]'            => ( isset( $values['image_type'] ) ? $values['image_type'] : '' ),
		$prefix . '[featured_image]'        => ( isset( $values['featured_image'] ) ? $values['featured_image'] : '' ),
		$prefix . '[image_id]'              => ( isset( $values['image_id'] ) ? $values['image_id'] : '' ),
		$prefix . '[image_icon]'            => ( isset( $values['image_icon'] ) ? $values['image_icon'] : '' ),
		$prefix . '[image_height]'          => ( isset( $values['image_height'] ) ? $values['image_height'] : '' ),
		$prefix . '[image_width]'           => ( isset( $values['image_width'] ) ? $values['image_width'] : '' ),
		$prefix . '[icon_size]'             => ( isset( $values['icon_size'] ) ? $values['icon_size'] : '' ),
		$prefix . '[image_global_classes]'  => $values['image_global_classes'],
		$prefix . '[image_desktop_classes]' => $values['image_desktop_classes'],
		$prefix . '[image_mobile_classes]'  => $values['image_mobile_classes']
	);

	// Button Fields
	// -----------------------------------------------------------------
	$_button_fields = array(
		$prefix . '[button_text]'            => array(
			'label'         => 'Link Text',
			'type'          => 'text',
			'default_value' => '',
			'sanitize'      => 'wp_kses_post'
		),
		$prefix . '[button_link]'            => array(
			'label'         => 'Link (WP Internal)',
			'type'          => 'wp_page_select',
			'default_value' => '',
		),
		$prefix . '[button_link_url]'        => array(
			'label'         => 'Link (URL Override)',
			'type'          => 'url',
			'default_value' => ''
		),
		$prefix . '[button_style]'           => array(
			'label'         => 'Link Style',
			'type'          => 'selectize',
			'choices'       => array(
				'text-link' => 'Text Link',
				'btn'       => 'Button'
			),
			'default_value' => 'btn',
		),
		$prefix . '[button_rel]'             => array(
			'label'         => 'Link REL',
			'type'          => 'text',
			'default_value' => '',
		),
		$prefix . '[button_target]'          => array(
			'label'         => 'Link Target',
			'type'          => 'selectize',
			'choices'       => array(
				'_self'     => 'This Window',
				'_blank'    => 'New Window',
				'_lightbox' => 'Open in Lightbox' // puts class 'bf-lightbox-iframe' on link
			),
			'default_value' => '_self',
		),
		$prefix . '[button_icon]'            => array(
			'label'           => 'Link Icon',
			'type'            => 'fontawesome_select',
			'container_class' => 'stellar-icon',
			'default_value'   => '',
		),
		$prefix . '[button_global_classes]'  => array(
			'label'   => 'Button Global Classes',
			'type'    => 'selectize',
			'choices' => bf_bucket_button_classes(),
			'atts'    => array(
				'multiple' => 'multiple',
			),
			'permit'  => 1,
		),
		$prefix . '[button_desktop_classes]' => array(
			'label'   => 'Button Desktop Classes',
			'type'    => 'selectize',
			'choices' => bf_bucket_button_breakpoint(),
			'atts'    => array(
				'multiple' => 'multiple',
			),
			'permit'  => 1,
		),
		$prefix . '[button_mobile_classes]'  => array(
			'label'   => 'Button Mobile Classes',
			'type'    => 'selectize',
			'choices' => bf_bucket_button_breakpoint(),
			'atts'    => array(
				'multiple' => 'multiple',
			),
			'permit'  => 1,
		),
	);

	// Instance Juggle
	$button_instance = array(
		$prefix . '[button_text]'            => ( isset( $values['button_text'] ) ? $values['button_text'] : '' ),
		$prefix . '[button_link]'            => ( isset( $values['button_link'] ) ? $values['button_link'] : '' ),
		$prefix . '[button_link_url]'        => ( isset( $values['button_link'] ) ? $values['button_link_url'] : '' ),
		$prefix . '[button_icon]'            => ( isset( $values['button_icon'] ) ? $values['button_icon'] : '' ),
		$prefix . '[button_style]'           => ( isset( $values['button_style'] ) ? $values['button_style'] : '' ),
		$prefix . '[button_target]'          => ( isset( $values['button_target'] ) ? $values['button_target'] : '' ),
		$prefix . '[button_global_classes]'  => ( isset( $values['button_global_classes'] ) ? $values['button_global_classes'] : '' ),
		$prefix . '[button_desktop_classes]' => ( isset( $values['button_desktop_classes'] ) ? $values['button_desktop_classes'] : '' ),
		$prefix . '[button_mobile_classes]'  => ( isset( $values['button_mobile_classes'] ) ? $values['button_mobile_classes'] : '' ),
	);

	// Build our widget form
	$args = array(
		'fields'   => $fields,
		'instance' => $fields_instance,
		'display'  => 'basic',
		'echo'     => false,
	);
	$output .= \BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

	// Heading Fields
	$output .= '<div class="heading-fields type-fields" style="display: none;">';

	$args = array(
		'fields'   => $_heading_fields,
		'instance' => $heading_instance,
		'display'  => 'basic',
		'echo'     => false,
	);
	$output .= \BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

	$output .= '</div>';

	// Text Fields
	$output .= '<div class="text-fields type-fields" style="display: none;">';

	$args = array(
		'fields'   => $_text_fields,
		'instance' => $text_instance,
		'display'  => 'basic',
		'echo'     => false,
	);
	$output .= \BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

	$output .= '</div>';

	// Image Fields
	$output .= '<div class="image-fields type-fields" style="display: none;">';

	$args = array(
		'fields'   => $_image_fields,
		'instance' => $image_instance,
		'display'  => 'basic',
		'echo'     => false,
	);
	$output .= \BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

	$output .= '</div>';

	// Button Fields
	$output .= '<div class="button-fields type-fields" style="display: none;">';

	$args = array(
		'fields'   => $_button_fields,
		'instance' => $button_instance,
		'display'  => 'basic',
		'echo'     => false,
	);
	$output .= \BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

	$output .= '</div>';

	return $output;

}

/**
 * Bucket Widget: Define global field defaults
 * @return array
 */
function define_defaults() {
	return array(
		'title'            => '',
		'link_wrap'        => '',
		'link_wrap_url'    => '',
		'link_rel'         => '',
		'link_target'      => '_self',
		'image_id' => '',
		'blocks'           => array()
	);
}

/**
 * Bucket Widget: Define block field defaults
 * @return array
 */
function define_block_defaults() {
	return array(
		// Block Settings
		'block_type'              => '',

		// Heading Settings
		'block_heading'           => '',
		'block_heading_size'      => '',
		'heading_global_classes'  => array(),
		'heading_desktop_classes' => array(),
		'heading_mobile_classes'  => array(),

		// Text Settings
		'block_text'              => '',
		'text_global_classes'     => array(),
		'text_desktop_classes'    => array(),
		'text_mobile_classes'     => array(),

		// Image Settings
		'image_link'              => '',
		'image_link_url'          => '',
		'image_rel'               => '',
		'image_target'            => '_self',
		'image_type'              => '',
		'image_id'                => '',
		'image_icon'              => '',
		'image_height'            => 'image-full',
		'image_width'             => '',
		'icon_size'               => '',
		'image_global_classes'    => array(),
		'image_desktop_classes'   => array(),
		'image_mobile_classes'    => array(),

		// Button Settings
		'button_text'             => '',
		'button_link'             => '',
		'button_link_url'         => '',
		'button_rel'              => '',
		'button_target'           => '_self',
		'button_icon'             => '',
		'button_style'            => '',
		'button_size'             => '',
		'button_align'            => 'inherit',
		'button_global_classes'   => array(),
		'button_desktop_classes'  => array(),
		'button_mobile_classes'   => array(),
	);
}

function parse_url_override( $page_id, $manual_url ) {

	if ( empty( $manual_url ) && ! empty( $page_id ) ) {
		$rtn = get_permalink( $page_id );
	} elseif ( ! empty( $manual_url ) ) {
		$rtn = do_shortcode( $manual_url );
	} else {
		$rtn = '';
	}

	return $rtn;

}