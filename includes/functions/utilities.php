<?php

namespace BrightFire\Theme\Stellar;

function yes_no_choices() {

	return array(
		'yes' => 'Yes',
		'no'  => 'No',
	);
}

function left_right_choices() {

	return array(
		''      => 'None',
		'left'  => 'Left',
		'right' => 'Right',
	);
}

function row_usage_choices() {

	return array(
		'header'  => 'Header',
		'content' => 'Content',
		'footer'  => 'Footer',
	);
}

function background_position_choices() {

	return array(
		'left top'      => 'Left Top',
		'left center'   => 'Left Center',
		'left bottom'   => 'Left Bottom',
		'right top'     => 'Right Top',
		'right center'  => 'Right Center',
		'right bottom'  => 'Right Bottom',
		'center top'    => 'Center Top',
		'center center' => 'Center Center',
		'center bottom' => 'Center Bottom',
	);
}

function background_size_choices() {

	return array(
		'auto'    => 'Auto',
		'cover'   => 'Cover',
		'contain' => 'Contain',
	);
}

function background_repeat_choices() {

	return array(
		'no-repeat' => 'No Repeat',
		'repeat'    => 'Repeat',
		'repeat-x'  => 'Repeat Horizontal',
		'repeat-y'  => 'Repeat Vertical',
	);
}

function background_attachment_choices() {

	return array(
		'scroll' => 'Scroll',
		'fixed'  => 'Fixed'
	);
}

function color_picker_palettes() {

	// Our saved color keys
	$theme_colors = array(
		'primary-color',
		'secondary-color',
		'tertiary-color',
		'accent-color',
		'body-background-color-top',
		'body-background-color-bottom'
	);

	$mods = get_theme_mods();

	$palettes = array( '#ffffff', '#000000', );

	foreach ( $theme_colors as $color ) {
		if ( isset( $mods[ $color ] ) && ! empty( $mods[ $color ] ) ) {
			array_push( $palettes, $mods[ $color ] );
		}
	}

	$palettes = apply_filters( 'stellar_color_picker_palettes', $palettes );

	$palettes = array_values( array_unique( $palettes ) );


	$script = '<script>';
	$script .= 'var stellarPalettes = ' . json_encode( $palettes ) . ';';
	$script .= '</script>';

	echo $script;
}

function in_array_r( $needle, $haystack, $strict = false ) {

	foreach ( $haystack as $item ) {

		if ( ( $strict ? $item === $needle : $item == $needle ) || ( is_array( $item ) && in_array_r( $needle, $item, $strict ) ) ) {

			return true;
		}
	}

	return false;
}

function background_style_generator( $args ) {

	$post_id = get_queried_object_id();

	$expected_args = array(
		'color'            => '',
		'color_bottom'     => '',
		'color_stop'   => '0',
		'color_bottom_stop'   => '100',
		'gradient_angle'   => '180',
		'featured_image'   => 'no',
		'image_id'         => '',
		'image_attachment' => '',
		'image_repeat'     => '',
		'image_size'       => '',
		'image_position'   => '',
	);

	// Merge $args with $expected_args
	$args = recurse_parse_args( $args, $expected_args );

	$props            = '';
	$background_image = array();
	$background       = ( ! empty( $args[ 'color' ] ) && empty( $args[ 'color_bottom' ] ) ) ? "background-color: {$args[ 'color' ]};" : '';

	if ( 'yes' == $args[ 'featured_image' ] && has_post_thumbnail( $post_id ) ) {
		// Reset the image_id to our featured image
		$args[ 'image_id' ] = get_post_thumbnail_id( $post_id );
	}

	// Get our background gradient and add to the $background_image array if we have one
	if ( ! empty( $args[ 'color' ] ) && ! empty( $args[ 'color_bottom' ] ) ) {
		$background_image[] = "linear-gradient( {$args[ 'gradient_angle' ]}deg, {$args[ 'color' ]}  {$args[ 'color_stop' ]}%, {$args[ 'color_bottom' ]} {$args[ 'color_bottom_stop' ]}%)";
	}

	/**
	 * Background Image
	 *
	 * If we have an image_id then the other options will be saved and used
	 */
	if ( ! empty( $args[ 'image_id' ] ) ) {

		if ( is_numeric( $args[ 'image_id' ] ) ) {
			$image_url = wp_get_attachment_image_src( $args[ 'image_id' ], 'full' )[ 0 ];
			// Add our image url to the $background_image array
			$background_image[] = "url( $image_url )";

			// Set up the rest of our properties
			$fixed    = "background-attachment:{$args[ 'image_attachment' ]}, scroll;"; // scroll value is added as fix for possible gradient
			$repeat   = "background-repeat:{$args[ 'image_repeat' ]};";
			$size     = "background-size:{$args[ 'image_size' ]};";
			$position = "background-position:{$args[ 'image_position' ]}";

		} else {

			// MISSING IMAGE
			$image_url = get_stylesheet_directory_uri() . '/images/missing-image.png';

			// Add our image url to the $background_image array
			$background_image[] = "url( $image_url )";

			// Set up the rest of our properties
			$fixed    = "background-attachment: scroll, scroll;"; // scroll value is added as fix for possible gradient
			$repeat   = "background-repeat: repeat;";
			$size     = "background-size: 75px 75px;";
			$position = "background-position:center center;";
		}

		$props = "{$fixed} {$repeat} {$size} {$position}";
	}

	// Get possible values for background-image property
	$image = implode( ',', $background_image );

	if ( ! empty( $image ) ) {
		$background .= "background-image: {$image};";
	}

	return $background . $props;
}

function font_choices_array() {

	$fonts = array(
		array(
			'stack'         => '',
			'label'         => 'None',
			'family_styles' => ''
		),
		'alegreya-sans' => array(
			'stack' => '"Alegreya Sans", sans-serif;',
			'label' => 'Alegreya Sans',
			'family_styles' => "Alegreya+Sans:300,400,700"
		),
		'bitter'        => array(
			'stack'         => '"Bitter", $georgia',
			'label'         => 'Bitter',
			'family_styles' => "Bitter:300,400,700"
		),
		'cantarell' => array(
			'stack' => '"Cantarell", sans-serif;',
			'label' => 'Cantarell',
			'family_styles' => "Cantarell:400,700"
		),
		'cormorant' => array(
			'stack' => '"Cormorant Infant", serif;',
			'label' => 'Cormorant Infant',
			'family_styles' => "Cormorant+Infant:300,400,700"
		),
		'dosis' => array(
			'stack' => '"Dosis", sans-serif;',
			'label' => 'Dosis',
			'family_styles' => "Dosis:300,400,700"
		),
		'droid_serif'        => array(
			'stack'         => '"Droid Serif", $georgia',
			'label'         => 'Droid Serif',
			'family_styles' => "Droid+Serif:300,400,700"
		),
		'georgia'     => array(
			'stack'         => '$georgia',
			'label'         => 'Georgia',
			'family_styles' => ''
		),
		'helvetica'   => array(
			'stack'         => '$helvetica',
			'label'         => 'Helvetica',
			'family_styles' => ''
		),
		'lato'        => array(
			'stack'         => '"Lato", $lucida-grande',
			'label'         => 'Lato',
			'family_styles' => "Lato:300,400,700"
		),
		'martel' => array(
			'stack' => '"Martel", serif;',
			'label' => 'Martel',
			'family_styles' => "Martel:300,400,700"
		),
		'merriweather-sans' => array(
			'stack' => '"Merriweather Sans", sans-serif;',
			'label' => 'Merriweather Sans',
			'family_styles' => "Merriweather+Sans:300,400,700"
		),
		'montserrat'  => array(
			'stack'         => '"Montserrat", $helvetica',
			'label'         => 'Montserrat',
			'family_styles' => "Montserrat:300,400,700"
		),
		'open_sans'   => array(
			'stack'         => '"Open Sans", $helvetica',
			'label'         => 'Open Sans',
			'family_styles' => "Open+Sans:300,400,700"
		),
		'oswald'      => array(
			'stack'         => '"Oswald", $helvetica',
			'label'         => 'Oswald',
			'family_styles' => "Oswald:300,400,700"
		),
		'quicksand' => array(
			'stack' => '"Quicksand", sans-serif;',
			'label' => 'Quicksand',
			'family_styles' => "Quicksand:300,400,700"
		),
		'raleway'      => array(
			'stack'         => '"Raleway", $helvetica',
			'label'         => 'Raleway',
			'family_styles' => "Raleway:300,400,700"
		),
		'roboto'      => array(
			'stack'         => '"Roboto", $helvetica',
			'label'         => 'Roboto',
			'family_styles' => "Roboto:300,400,700"
		),
		'roboto_condensed' => array(
			'stack'         => '"Roboto Condensed", $helvetica',
			'label'         => 'Roboto Condensed',
			'family_styles' => "Roboto+Condensed:300,400,700"
		),
		'roboto_slab' => array(
			'stack'         => '"Roboto Slab", $helvetica',
			'label'         => 'Roboto Slab',
			'family_styles' => "Roboto+Slab:300,400,700"
		),
		'source_serif_pro'        => array(
			'stack'         => '"Source Serif Pro", $georgia',
			'label'         => 'Source Serif Pro',
			'family_styles' => "Source+Serif+Pro:300,400,700"
		),
		'trebuchet'        => array(
			'stack'         => '"Trebuchet MS", $lucida-grande',
			'label'         => 'Trebuchet',
			'family_styles' => ""
		),
	);

	return apply_filters( 'stellar_font_choices_array', $fonts );
}

function font_choices() {

	$rtn_arr = array();

	foreach ( font_choices_array() as $id => $info ) {
		$rtn_arr[ $id ] = $info[ 'label' ];
	}

	return $rtn_arr;
}

function stellar_font_styles( $header_font = null, $body_font = null, $accent_font = null ) {

	if( empty( $header_font ) ) {
		$header_font = get_theme_mod( 'header-font-family' );
	}

	if ( empty( $body_font ) ) {
		$body_font = get_theme_mod( 'base-font-family' );
	}

	if ( empty( $accent_font ) ) {
		$accent_font = get_theme_mod( 'accent-font-family' );
	}

	$fonts = font_choices_array();

	if ( $header_font || $body_font || $accent_font) {

		$font_styles = array();

		if ( ! empty( $header_font ) && ! empty( $fonts[ $header_font ][ 'family_styles' ] ) ) {
			$font_styles[] = $fonts[ $header_font ][ 'family_styles' ];
		}
		if ( ! empty( $body_font ) && ! empty( $fonts[ $body_font ][ 'family_styles' ] ) ) {
			$font_styles[] = $fonts[ $body_font ][ 'family_styles' ];
		}
		if ( ! empty( $accent_font ) && ! empty( $fonts[ $accent_font ][ 'family_styles' ] ) ) {
			$font_styles[] = $fonts[ $accent_font ][ 'family_styles' ];
		}

		if ( ! empty ( $font_styles ) ) {
			$fonts_selected = implode( "|", $font_styles );
			return "<link id='stellar-google-fonts' href='https://fonts.googleapis.com/css?family={$fonts_selected}' rel='stylesheet' type='text/css'>";
		}
	}
}

function recurse_parse_args( &$a, $b ) {
	$a = (array) $a;
	$b = (array) $b;
	$result = $b;
	foreach ( $a as $k => &$v ) {
		if ( is_array( $v ) && ( isset( $result[ $k ] ) ) ) {
			$result[ $k ] = recurse_parse_args( $v, $result[ $k ] );
		} else {
			$result[ $k ] = $v;
		}
	}
	return $result;
}

function directoryToArray( $path, $extension = true ) {

	$result = array();

	$cdir = scandir( $path );

	foreach ( $cdir as $key => $value ) {

		if ( ! in_array( $value, array( '.', '..', '.DS_Store' ) ) ) {
			if ( is_dir( $path . DIRECTORY_SEPARATOR . $value ) ) {
				$result[ $value ] = directoryToArray( $path . DIRECTORY_SEPARATOR . $value );
			} else {
				if( ! $extension ) {
					$result[] = pathinfo($value, PATHINFO_FILENAME);
				} else {
					$result[] = $value;
				}

			}
		}

	}

	return $result;
}

/**
 * Action hook for customize_register
 * @param $wp_customize
 */
function stellar_customize_register( $wp_customize ) {
	$wp_customize->remove_section( 'bf_settings_custom_scripts' );
}

function stellar_body_classes( $classes ) {

	$stellar_body_classes = array();

	// Body Padding
	$body_padding = get_theme_mod( 'body-background-padding' );
	switch( $body_padding ) {
		case 'none' :
			break;
		case 'base' :
			array_push( $stellar_body_classes, 'body-padding' );
			break;
		case 'half' :
			array_push( $stellar_body_classes, 'body-padding-half' );
			break;
		case 'large' :
			array_push( $stellar_body_classes, 'body-padding-large' );
			break;
	}

	$classes = array_merge( $stellar_body_classes, $classes );

	return $classes;

}

/**
 * Returns a formatted array of gravity form ids for use in select inputs
 * @return array
 */
function get_gf_forms() {
	$forms     = \RGFormsModel::get_forms( null, 'title' );
	$rtn_forms = array();
	foreach ( $forms as $form ) {
		$rtn_forms[ $form->id ] = $form->title;
	}

	$rtn_forms[0] = '-- Select --';

	asort( $rtn_forms );

	return $rtn_forms;
}

/**
 * Output helper for header and footer scripts.
 *
 * @param string $placement
 *
 * @return string | boolean
 */
function custom_scripts( $placement ){
	
	$scripts = get_option( 'stellar_scripts' );
	
	if ( $scripts ) {

		if ( 'head' === $placement ) {
			
			return stripslashes( $scripts[ 'header_scripts' ] );
			
		} elseif ( 'footer' === $placement ) {

			return stripslashes( $scripts[ 'footer_scripts' ] );

		} else {

			return false;
		}
	} else {

		return false;
	}
}