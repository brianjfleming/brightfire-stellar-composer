<?php

namespace BrightFire\Theme\Stellar;

/*
 * COLORS AND DESIGN
 */

function background_colors() {

	$choices = array(
		'background-accent'    => array(
			'name' => 'Background Color: Accent',
			'desc' => 'Applies Accent Color to the background of this widget container as set in your Stellar -> Theme Options settings.',
		),
		'background-dark'      => array(
			'name' => 'Background Color: Dark',
			'desc' => 'Applies a Dark Background color to the widget container. The exact color can vary by theme but will remain a dark color fitting with the styles of the active theme.',
		),
		'shade1'               => array(
			'name' => 'Background Color: Shade 1',
			'desc' => 'Darken the applied background color by 20%',
		),
		'shade2'               => array(
			'name' => 'Background Color: Shade 2',
			'desc' => 'Darken the applied background color by 40%',
		),
		'background-gray'      => array(
			'name' => 'Background Color: Gray',
			'desc' => 'Applies a Gray background color to the widget container. The exact color can vary by theme but will remain a gray fitting with the styles of the active theme.',
		),
		'background-light'     => array(
			'name' => 'Background Color: Light',
			'desc' => 'Applies a Light background color to the widget container. The exact color can vary by theme but will remain a light color fitting with the styles of the active theme.',
		),
		'tint1'                => array(
			'name' => 'Background Color: Tint 1',
			'desc' => 'Lighten the applied background color by 20%',
		),
		'tint2'                => array(
			'name' => 'Background Color: Tint 2',
			'desc' => 'Lighten the applied background color by 40%',
		),
		'background-primary'   => array(
			'name' => 'Background Color: Primary',
			'desc' => 'Applies Primary Color to the background of this widget container as set in your Stellar -> Theme Options settings.',
		),
		'background-secondary' => array(
			'name' => 'Background Color: Secondary',
			'desc' => 'Applies Secondary Color to the background of this widget container as set in your Stellar -> Theme Options settings.',
		),
		'background-tertiary'  => array(
			'name' => 'Background Color: Tertiary',
			'desc' => 'Applies Tertiary Color to the background of this widget container as set in your Stellar -> Theme Options settings.',
		),
		'alpha20'              => array(
			'name' => 'Background Alpha: 20%',
			'desc' => 'Reduces the opacity of the widget background color to 20% (80% translucent)',
		),
		'alpha50'              => array(
			'name' => 'Background Alpha: 50%',
			'desc' => 'Reduces the opacity of the widget background color to 50% (50% translucent)',
		),
		'alpha80'              => array(
			'name' => 'Background Alpha: 80%',
			'desc' => 'Reduces the opacity of the widget background color to 80% (20% translucent)',
		),
	);

	return $choices;
}

function heading_colors() {
	$choices = array(
		'heading-color-accent'    => array(
			'name' => 'Heading Color: Accent',
			'desc' => ''
		),
		'heading-color-dark'      => array(
			'name' => 'Heading Color: Dark',
			'desc' => ''
		),
		'heading-color-light'     => array(
			'name' => 'Heading Color: Light',
			'desc' => ''
		),
		'heading-color-gray'     => array(
			'name' => 'Heading Color: Gray',
			'desc' => ''
		),
		'heading-color-primary'   => array(
			'name' => 'Heading Color: Primary',
			'desc' => ''
		),
		'heading-color-secondary' => array(
			'name' => 'Heading Color: Secondary',
			'desc' => ''
		),
		'heading-color-tertiary'  => array(
			'name' => 'Heading Color: Tertiary',
			'desc' => ''
		),
	);

	return $choices;
}

function text_colors() {
	$choices = array(
		'text-color-accent'    => array(
			'name' => 'Text Color: Accent',
			'desc' => 'Applies Accent Color to text within this widget as set in your Stellar -> Theme Options settings.',
		),
		'text-color-dark'      => array(
			'name' => 'Text Color: Dark',
			'desc' => 'Applies a dark color to text within this widget. The exact color can vary by theme but will remain a dark color fitting with the styles of the active theme.',
		),
		'text-color-light'     => array(
			'name' => 'Text Color: Light',
			'desc' => 'Applies a light color to text within this widget. The exact color can vary by theme but will remain a light color fitting with the styles of the active theme.',
		),
		'text-color-gray'     => array(
			'name' => 'Text Color: Gray',
			'desc' => 'Applies a gray color to text within this widget. The exact color can vary by theme but will remain a light color fitting with the styles of the active theme.',
		),
		'text-color-primary'   => array(
			'name' => 'Text Color: Primary',
			'desc' => 'Applies Primary Color to text within this widget as set in your Stellar -> Theme Options settings.',
		),
		'text-color-secondary' => array(
			'name' => 'Text Color: Secondary',
			'desc' => 'Applies Secondary Color to text within this widget as set in your Stellar -> Theme Options settings.',
		),
		'text-color-tertiary'  => array(
			'name' => 'Text Color: Tertiary',
			'desc' => 'Applies Tertiary Color to text within this widget as set in your Stellar -> Theme Options settings.',
		),
	);

	return $choices;
}

function text_fonts() {
	$choices = array(
		'text-font-header'    => array(
			'name' => 'Text Font: Header',
			'desc' => 'Applies the Header Font to this widget as set in your Stellar -> Theme Options settings.',
		),
		'text-font-body'      => array(
			'name' => 'Text Font: Body',
			'desc' => 'Applies the Body Font to this widget as set in your Stellar -> Theme Options settings.',
		),
		'text-font-accent'    => array(
			'name' => 'Text Font: Accent',
			'desc' => 'Applies the Accent Font to this widget as set in your Stellar -> Theme Options settings.',
		),
		'text-weight-light'   => array(
			'name' => 'Text Weight: Light',
			'desc' => ''
		),
		'text-weight-regular' => array(
			'name' => 'Text Weight: Regular',
			'desc' => ''
		),
		'text-weight-bold'    => array(
			'name' => 'Text Weight: Bold',
			'desc' => ''
		),
		'text-uppercase'      => array(
			'name' => 'Text: Uppercase',
			'desc'
		),
		'text-italic'         => array(
			'name' => 'Text: Italic',
			'desc'
		)
	);

	return $choices;
}

function heading_fonts() {
	$choices = array(
		'heading-font-header'    => array(
			'name' => 'Heading Font: Header',
			'desc' => 'Applies the Header Font to this widget as set in your Stellar -> Theme Options settings.',
		),
		'heading-font-body'      => array(
			'name' => 'Heading Font: Body',
			'desc' => 'Applies the Body Font to this widget as set in your Stellar -> Theme Options settings.',
		),
		'heading-font-accent'    => array(
			'name' => 'Heading Font: Accent',
			'desc' => 'Applies the Accent Font to this widget as set in your Stellar -> Theme Options settings.',
		),
		'heading-uppercase'      => array(
			'name' => 'Heading: Uppercase',
			'desc'
		),
		'heading-italic'         => array(
			'name' => 'Heading: Italic',
			'desc'
		),
		'heading-weight-light'   => array(
			'name' => 'Heading Weight: Light',
			'desc' => ''
		),
		'heading-weight-regular' => array(
			'name' => 'Heading Weight: Regular',
			'desc' => ''
		),
		'heading-weight-bold'    => array(
			'name' => 'Heading Weight: Bold',
			'desc' => ''
		),
	);

	return $choices;
}

function button_fonts() {
	$choices = array(
		'btn-font-header'    => array(
			'name' => 'Button Font: Header',
			'desc' => 'Applies the Header Font to this widget as set in your Stellar -> Theme Options settings.',
		),
		'btn-font-body'      => array(
			'name' => 'Button Font: Body',
			'desc' => 'Applies the Body Font to this widget as set in your Stellar -> Theme Options settings.',
		),
		'btn-font-accent'    => array(
			'name' => 'Button Font: Accent',
			'desc' => 'Applies the Accent Font to this widget as set in your Stellar -> Theme Options settings.',
		),
		'btn-uppercase'      => array(
			'name' => 'Button: Uppercase',
			'desc'
		),
		'btn-italic'         => array(
			'name' => 'Button: Italic',
			'desc'
		),
		'btn-weight-light'   => array(
			'name' => 'Button Weight: Light',
			'desc' => ''
		),
		'btn-weight-regular' => array(
			'name' => 'Button Weight: Regular',
			'desc' => ''
		),
		'btn-weight-bold'    => array(
			'name' => 'Button Weight: Bold',
			'desc' => ''
		),
	);

	return $choices;
}

function link_colors() {
	$choices = array(
		'link-color-accent'    => array(
			'name' => 'Link Color: Accent',
			'desc' => 'Applies Accent Color to all links within this widget as set in your Stellar -> Theme Options settings.',
		),
		'link-color-dark'      => array(
			'name' => 'Link Color: Dark',
			'desc' => 'Applies a dark color to all links withing this widget. The exact color can vary by theme but will remain a dark color fitting with the styles of the active theme.',
		),
		'link-color-light'     => array(
			'name' => 'Link Color: Light',
			'desc' => 'Applies a light color to all links within this widget. The exact color can vary by theme but will remain a light color fitting with the styles of the active theme.',
		),
		'link-color-gray'     => array(
			'name' => 'Link Color: Gray',
			'desc' => 'Applies a gray color to all links within this widget. The exact color can vary by theme but will remain a gray color fitting with the styles of the active theme.',
		),
		'link-color-primary'   => array(
			'name' => 'Link Color: Primary',
			'desc' => 'Applies Primary Color to all links within this widget as set in your Stellar -> Theme Options settings.',
		),
		'link-color-secondary' => array(
			'name' => 'Link Color: Secondary',
			'desc' => 'Applies Secondary Color to all links within this widget as set in your Stellar -> Theme Options settings.',
		),
		'link-color-tertiary'  => array(
			'name' => 'Link Color: Tertiary',
			'desc' => 'Applies Tertiary Color to all links within this widget as set in your Stellar -> Theme Options settings.',
		),
		'link-color-brands'    => array(
			'name' => 'Link Color: Social Brands',
			'desc' => 'Applies Brand Color to all social media links within this widget.',
		),
	);

	return $choices;
}

function button_colors() {

	$choices = array(
		'btn-primary'        => array(
			'name' => 'Button Color: Primary',
			'desc' => 'Make buttons in this widget the Primary Color.'
		),
		'btn-secondary'      => array(
			'name' => 'Button Color: Secondary',
			'desc' => 'Make buttons in this widget the Secondary Color.'
		),
		'btn-tertiary'       => array(
			'name' => 'Button Color: Tertiary',
			'desc' => 'Make buttons in this widget the Tertiary Color.'
		),
		'btn-accent'         => array(
			'name' => 'Button Color: Accent',
			'desc' => 'Make buttons in this widget the Accent Color.'
		),
		'btn-dark'           => array(
			'name' => 'Button Color: Dark',
			'desc' => 'Make buttons in this widget the Dark Color.'
		),
		'btn-gray'           => array(
			'name' => 'Button Color: Gray',
			'desc' => 'Make buttons in this widget the Gray Color.'
		),
		'btn-light'          => array(
			'name' => 'Button Color: Light',
			'desc' => 'Make buttons in this widget the Light Color.'
		),
		'btn-text-primary'   => array(
			'name' => 'Button Text Color: Primary',
			'desc' => 'Make button text in this widget the Primary Color.'
		),
		'btn-text-secondary' => array(
			'name' => 'Button Text Color: Secondary',
			'desc' => 'Make button text in this widget the Secondary Color.'
		),
		'btn-text-tertiary'  => array(
			'name' => 'Button Text Color: Tertiary',
			'desc' => 'Make button text in this widget the Tertiary Color.'
		),
		'btn-text-accent'    => array(
			'name' => 'Button Text Color: Accent',
			'desc' => 'Make button text in this widget the Accent Color.'
		),
		'btn-text-dark'      => array(
			'name' => 'Button Text Color: Dark',
			'desc' => 'Make button text in this widget the Dark Color.'
		),
		'btn-text-gray'      => array(
			'name' => 'Button Text Color: Gray',
			'desc' => 'Make button text in this widget the Gray Color.'
		),
		'btn-text-light'     => array(
			'name' => 'Button Text Color: Light',
			'desc' => 'Make button text in this widget the Light Color.'
		),
		'btn-transparent'    => array(
			'name' => 'Button: Transparent',
			'desc' => 'Used with Button Color, creates a bordered button with the color chosen and no background.'
		),
	);

	return $choices;
}

function border_colors() {
	$colors = array(
		'light',
		'gray',
		'dark',
		'primary',
		'secondary',
		'tertiary',
		'accent',
		'transparent'
	);

	$borders = array();

	//Colors
	foreach ( $colors as $color ) {

		$borders["border-color-{$color}"] = array(
			'name' => "Border Color: " . ucwords( $color ),
			'desc' => "Applies $color color to borders."
		);
	}

	$mixes = array(
		'border-tint1'  => array(
			'name' => "Border Color: Tint 1",
			'desc' => "Lighten the applied border color by 20%."
		),
		'border-tint2'  => array(
			'name' => "Border Color: Tint 2",
			'desc' => "Lighten the applied border color by 40%."
		),
		'border-shade1' => array(
			'name' => "Border Color: Shade 1",
			'desc' => "Darken the applied border color by 20%."
		),
		'border-shade2' => array(
			'name' => "Border Color: Shade 2",
			'desc' => "Darken the applied border color by 40%."
		),
		'border-alpha20' => array(
			'name' => 'Border Color: Alpha 20%',
			'desc' => 'Reduces the opacity of the applied border color to 20% (80% translucent)',
		),
	);

	$choices = array_merge( $borders, $mixes );

	return $choices;
}

/*
 * SIZES AND ALIGNMENT
 */

function text_align() {
	$choices = array(
		'text-align-center' => array(
			'name' => 'Text Align: Center',
			'desc' => 'Aligns text within this widget to the center.',
		),
		'text-align-left'   => array(
			'name' => 'Text Align: Left',
			'desc' => 'Aligns text within this widget to the left.',
		),
		'text-align-right'  => array(
			'name' => 'Text Align: Right',
			'desc' => 'Aligns text within this widget to the right.',
		),
	);

	return $choices;
}

function text_sizes() {
	$sizes = array(
		'Small'         => 'Smallest font size.',
		'Base'          => 'Base font size. Same size as the BODY copy.',
		'Medium'        => 'Medium font size. Same size as the H5.',
		'Large'         => 'Large font size. Same size as the H4.',
		'XLarge'        => 'X large font size. Same size as the H3.',
		'XXLarge'       => 'XX large font size. Same size as the H2.',
		'XXXLarge'      => 'XXX large font size. Same size as the H1.',
		'Hero Small'    => 'One step larger than H1.',
		'Hero Medium'   => 'Two steps larger than H1.',
		'Hero Large'    => 'Three steps larger than H1.'
	);

	$text_sizes = array();

	foreach ( $sizes as $size => $desc ) {
		$text_sizes[ 'text-size-' . sanitize_title_with_dashes( $size ) ] = array(
			'name'  => "Text Size: {$size}",
			'desc'  => $desc
		);
	}

	return $text_sizes;
}

function heading_sizes() {
	$sizes = array(
		'Small'    => 'Smallest font size.',
		'Base'     => 'Base font size. Same size as the BODY copy.',
		'Medium'   => 'Medium font size. Same size as the H5.',
		'Large'    => 'Large font size. Same size as the H4.',
		'XLarge'   => 'X large font size. Same size as the H3.',
		'XXLarge'  => 'XX large font size. Same size as the H2.',
		'XXXLarge' => 'XXX large font size. Same size as the H1.',
		'Hero Small'    => 'One step larger than H1.',
		'Hero Medium'   => 'Two steps larger than H1.',
		'Hero Large'    => 'Three steps larger than H1.'	);

	$text_sizes = array();

	foreach ( $sizes as $size => $desc ) {
		$text_sizes[ 'heading-size-' . sanitize_title_with_dashes( $size ) ] = array(
			'name'  => "Heading Size: {$size}",
			'desc'  => $desc
		);
	}

	return $text_sizes;
}

function fixed_line_heights() {
	$sizes = array(
		'0-5'  => 'Half',
		'1'    => 'Base ',
		'1-5'  => 'Base x 1.5',
		'2'    => 'Base x 2',
		'2-5'  => 'Base x 2.5',
		'3'    => 'Base x 3',
		'3-5'  => 'Base x 3.5',
		'4'    => 'Base x 4',
	);

	$text_sizes = array();

	foreach ( $sizes as $size => $desc ) {
		$text_sizes[ "fixed-line-height-{$size}"  ] = array(
			'name'  => "Fixed Line Height: {$desc}",
			'desc'  => "$desc spacing"
		);
	}

	return $text_sizes;
}

function button_sizes() {

	$choices = array(
		'btn-mini'  => array(
			'name' => 'Button Padding: Mini',
			'desc' => 'Very small button.'
		),
		'btn-large' => array(
			'name' => 'Button Padding: Large',
			'desc' => 'Big button.'
		),
		'btn-block' => array(
			'name' => 'Button Block',
			'desc' => "Button spans 100% of the width of it&apos;s container."
		),
	);

	return $choices;
}

function button_text_sizes() {

	// THESE ONLY WORK ON SUBMIT INPUTS
	$sizes = array(
		'Small'    => 'Smallest font size.',
		'Base'     => 'Base font size. Same size as the BODY copy.',
		'Medium'   => 'Medium font size. Same size as the H5.',
		'Large'    => 'Large font size. Same size as the H4.',
		'XLarge'   => 'X large font size. Same size as the H3.',
		'XXLarge'  => 'XX large font size. Same size as the H2.',
		'XXXLarge' => 'XXX large font size. Same size as the H1.',
		'Hero'     => 'Largest font size.'
	);

	$choices = array();

	foreach ( $sizes as $size => $desc ) {
		$choices[ 'btn-text-size-' . strtolower( $size ) ] = array(
			'name' => "Button Text Size: {$size}",
			'desc' => $desc
		);
	}

	return $choices;
}

function spacing() {

	$sizes = array(
		'1'    => 'Base',
		'0-5'  => 'Half',
		'1-5'  => 'Base x 1.5',
		'2'    => 'Base x 2',
		'2-5'  => 'Base x 2.5',
		'3'    => 'Base x 3',
		'3-5'  => 'Base x 3.5',
		'4'    => 'Base x 4',
	);

	$sides = array(
		'',
		'top',
		'right',
		'bottom',
		'left'
	);

	$margins = $padding = '';

	foreach ( $sizes as $size => $name ) {

		foreach ( $sides as $side ) {

			$side_class = ( empty( $side ) ) ? '' : "-{$side}";
			$side_name  = ( empty( $side ) ) ? '' : ' ' . ucwords( $side );

			if ( empty( $side ) ) {
				$pad_desc = "{$name} padding on all sides.";
			} else {
				$pad_desc = "{$name} padding on {$side} side.";
			}

			$padding["padding{$side_class}-{$size}"] = array(
				'name' => "Padding{$side_name}: {$name}",
				'desc' => $pad_desc
			);

			if ( $side == 'top' || $side == 'bottom' ) {

				$margins["margin{$side_class}-{$size}"] = array(
					'name' => "Margin{$side_name}: {$name}",
					'desc' => "{$name} margin on {$side} side."
				);
			}
		}
	}

	$other_classes = array(
		'no-gutters' => array(
			'name' => 'No Gutters',
			'desc' => 'Remove grid gutters between all widgets in this row.',
		),
	);

	$choices = array_merge( $margins, $padding, $other_classes );

	return $choices;
}

function border_size_position() {

	$sides = array(
		'',
		'top',
		'right',
		'bottom',
		'left'
	);

	$widths = array(
		''       => '1px',
		'medium' => '3px',
		'heavy'  => '6px'
	);

	$choices = array();
	//Builds an array of our border combinations
	foreach ( $sides as $side ) {
		// Adds side to class
		$side_class = ( empty( $side ) ) ? '' : "-{$side}";
		$side_name  = ucwords( $side );

		// Widths
		foreach ( $widths as $width => $amount ) {

			// Adds width to class
			$width_class = ( empty( $width ) ) ? '' : "-{$width}";

			// Normal Width All Sides
			if ( empty( $width ) && empty( $side ) ) {
				$width_name = 'Borders';
				$width_desc = 'on all sides';
				// Larger Widths All Sides
			} elseif ( ! empty( $width ) && empty( $side ) ) {
				$width_name = 'Borders: ' . ucwords( $width );
				$width_desc = 'on all sides';
				// Normal Width Each Side
			} elseif ( empty( $width ) && ! empty( $side ) ) {
				$width_name = "Border: $side_name";
				$width_desc = "on the $side side.";
				// Large Widths Each Side
			} else {
				$width_name = "Border {$side_name}: " . ucwords( $width );
				$width_desc = "on the $side side.";
			}

			$choices["border{$side_class}{$width_class}"] = array(
				'name' => $width_name,
				'desc' => "Puts a {$amount} border {$width_desc}."
			);
		}
	}

	return $choices;
}

function rounded_corners() {

	$choices = array(

		//Rounded Corners
		'rounded-corners'              => array(
			'name' => 'Rounded Corners',
			'desc' => 'Adds Rounded Corners to all sides.',
		),
		'rounded-corners-top-left'     => array(
			'name' => 'Rounded Corner: Top Left',
			'desc' => 'Rounds top left corner.',
		),
		'rounded-corners-top-right'    => array(
			'name' => 'Rounded Corner: Top Right',
			'desc' => 'Rounds top right corner.',
		),
		'rounded-corners-bottom-left'  => array(
			'name' => 'Rounded Corner: Bottom Left',
			'desc' => 'Rounds bottom left corner.',
		),
		'rounded-corners-bottom-right' => array(
			'name' => 'Rounded Corner: Bottom Right',
			'desc' => 'Rounds bottom right corner.',
		),
	);

	return $choices;
}

function menu_dividers() {

	$choices = array(
		'bfmenu-divider-around'  => array(
			'name' => 'Menu: Border Divider Around Items',
			'desc' => 'Adds border to the outside of first and last nav items.',
		),
		'bfmenu-divider-between'  => array(
			'name' => 'Menu: Border Divider Between Items',
			'desc' => 'Adds border divider between top level menu items.',
		),
	);

	return $choices;
}

function misc() {

	$choices = array(
		'hide'          => array(
			'name' => 'Hide',
			'desc' => 'Hides this widget by applying `display: none` to its parent wrapper.',
		),
		'window-width'  => array(
			'name' => 'Full Width',
			'desc' => 'Sets this row content to expand full width from the left of the window to the right, without being contained to a defined pixel-width.',
		),
	);

	return $choices;
}

function list_column_choices() {
	return array(
		'list_col2' => array(
			'name' => 'List Item: 2 Columns',
			'desc' => 'Auto create a columned view for lists. Can be used in BrightFire Forms or on any &ltn;ul&gtn; as a class.'
		),
		'list_col3' => array(
			'name' => 'List Item: 3 Columns',
			'desc' => 'Auto create a columned view for lists. Can be used in BrightFire Forms or on any &ltn;ul&gtn; as a class.'
		),
		'list_col4' => array(
			'name' => 'List Item: 4 Columns',
			'desc' => 'Auto create a columned view for lists. Can be used in BrightFire Forms or on any &ltn;ul&gtn; as a class.'
		),
		'list_col5' => array(
			'name' => 'List Item: 5 Columns',
			'desc' => 'Auto create a columned view for lists. Can be used in BrightFire Forms or on any &ltn;ul&gtn; as a class.'
		)
	);
}

function text_line_height() {

	$choices = array(
		'line-height-large' => array(
			'name' => "Text: Large Line Height",
			'decs' => 'Makes the line-height 2x font-size'
		)
	);

	return $choices;
}

/*
 * WIDGET READY CLASSES
 */
function widget_offset_choices() {

	$choices = array(
		'offset-none'          => array(
			'name' => 'None',
			'desc' => 'No Offset. Offsets determine spacing options to the left of your widget.'
		),
		'offset-one-half'      => array(
			'name' => 'One Half',
			'desc' => 'Offset my widget by one-half the width of the parent container by using spacing to the left side of the widget',
		),
		'offset-one-third'     => array(
			'name' => 'One Third',
			'desc' => 'Offset my widget by one-third the width of the parent container by using spacing to the left side of the widget',
		),
		'offset-one-fourth'    => array(
			'name' => 'One Fourth',
			'desc' => 'Offset my widget by one-fourth the width of the parent container by using spacing to the left side of the widget',
		),
		'offset-one-fifth'     => array(
			'name' => 'One Fifth',
			'desc' => 'Offset my widget by one-fifth the width of the parent container by using spacing to the left side of the widget',
		),
		'offset-two-thirds'    => array(
			'name' => 'Two Thirds',
			'desc' => 'Offset my widget by two-thirds the width of the parent container by using spacing to the left side of the widget',
		),
		'offset-two-fifths'    => array(
			'name' => 'Two Fifths',
			'desc' => 'Offset my widget by twp-fifths the width of the parent container by using spacing to the left side of the widget',
		),
		'offset-three-fourths' => array(
			'name' => 'Three Fourths',
			'desc' => 'Offset my widget by three-fourths the width of the parent container by using spacing to the left side of the widget',
		),
		'offset-three-fifths'  => array(
			'name' => 'Three Fifths',
			'desc' => 'Offset my widget by three-fifths the width of the parent container by using spacing to the left side of the widget',
		),
		'offset-four-fifths'   => array(
			'name' => 'Four Fifths',
			'desc' => 'Offset my widget by four-fifths the width of the parent container by using spacing to the left side of the widget',
		),
	);

	return $choices;
}

function widget_structure_choices() {

	$choices = array(
		'full-width'         => array(
			'name' => 'Widget Full Width',
			'desc' => 'Will force the widget to take the full width of the parent container.',
		),
		'one-half'           => array(
			'name' => 'One Half',
			'desc' => 'Will force the widget to take one-half the width of the parent container and float left.',
		),
		'one-half-last'      => array(
			'name' => 'One Half (LAST)',
			'desc' => 'Will force the widget to take one-half the width of the parent container and float left. The (LAST) specification removes the right margin and will add a clearfix after the widget. Should be used at the end of a row of widgets.',
		),
		'one-third'          => array(
			'name' => 'One Third',
			'desc' => 'Will force the widget to take one-third the width of the parent container and float left.',
		),
		'one-third-last'     => array(
			'name' => 'One Third (LAST)',
			'desc' => 'Will force the widget to take one-third the width of the parent container and float left. The (LAST) specification removes the right margin and will add a clearfix after the widget. Should be used at the end of a row of widgets.',
		),
		'one-fourth'         => array(
			'name' => 'One Fourth',
			'desc' => 'Will force the widget to take one-fourth the width of the parent container and float left.',
		),
		'one-fourth-last'    => array(
			'name' => 'One Fourth (LAST)',
			'desc' => 'Will force the widget to take one-fourth the width of the parent container and float left. The (LAST) specification removes the right margin and will add a clearfix after the widget. Should be used at the end of a row of widgets.',
		),
		'one-fifth'          => array(
			'name' => 'One Fifth',
			'desc' => 'Will force the widget to take one-fifth the width of the parent container and float left.',
		),
		'one-fifth-last'     => array(
			'name' => 'One Fifth (LAST)',
			'desc' => 'Will force the widget to take one-fifth the width of the parent container and float left. The (LAST) specification removes the right margin and will add a clearfix after the widget. Should be used at the end of a row of widgets.',
		),
		'two-thirds'         => array(
			'name' => 'Two Thirds',
			'desc' => 'Will force the widget to take two-thirds the width of the parent container and float left.',
		),
		'two-thirds-last'    => array(
			'name' => 'Two Thirds (LAST)',
			'desc' => 'Will force the widget to take two-thirds the width of the parent container and float left. The (LAST) specification removes the right margin and will add a clearfix after the widget. Should be used at the end of a row of widgets.',
		),
		'two-fifths'         => array(
			'name' => 'Two Fifths',
			'desc' => 'Will force the widget to take two-fifths the width of the parent container and float left.',
		),
		'two-fifths-last'    => array(
			'name' => 'Two Fifths (LAST)',
			'desc' => 'Will force the widget to take two-fifths the width of the parent container and float left. The (LAST) specification removes the right margin and will add a clearfix after the widget. Should be used at the end of a row of widgets.',
		),
		'three-fourths'      => array(
			'name' => 'Three Fourths',
			'desc' => 'Will force the widget to take three-fourths the width of the parent container and float left.',
		),
		'three-fourths-last' => array(
			'name' => 'Three Fourths (LAST)',
			'desc' => 'Will force the widget to take three-fourths the width of the parent container and float left. The (LAST) specification removes the right margin and will add a clearfix after the widget. Should be used at the end of a row of widgets.',
		),
		'three-fifths'       => array(
			'name' => 'Three Fifths',
			'desc' => 'Will force the widget to take three-fifths the width of the parent container and float left.',
		),
		'three-fifths-last'  => array(
			'name' => 'Three Fifths (LAST)',
			'desc' => 'Will force the widget to take three-fifths the width of the parent container and float left. The (LAST) specification removes the right margin and will add a clearfix after the widget. Should be used at the end of a row of widgets.',
		),
		'four-fifths'        => array(
			'name' => 'Four Fifths',
			'desc' => 'Will force the widget to take four-fifths the width of the parent container and float left.',
		),
		'four-fifths-last'   => array(
			'name' => 'Four Fifths (LAST)',
			'desc' => 'Will force the widget to take four-fifths the width of the parent container and float left. The (LAST) specification removes the right margin and will add a clearfix after the widget. Should be used at the end of a row of widgets.',
		),
	);

	return $choices;
}

function widget_mix_choices() {

	$mixes = get_option( 'stellar_mixes' );

	$choices = array();

	if ( $mixes ) {
		foreach ( $mixes as $index => $values ) {

			if ( $values[ 'name' ] ) {
				$globalStr  = ( $values['custom_classes'][0] ) ? '<li><strong><i class="fa fa-globe"></i> Global:</strong> ' . implode( ', ', $values['custom_classes'] ) . '</li>' : '';
				$desktopStr = ( $values['desktop_classes'][0] ) ? '<li><strong><i class="fa fa-laptop"></i> Desktop:</strong> ' . implode( ', ', $values['desktop_classes'] ) . '</li>' : '';
				$mobileStr  = ( $values['mobile_classes'][0] ) ? '<li><strong><i class="fa fa-mobile"></i> Mobile:</strong> ' . implode( ', ', $values['mobile_classes'] ) . '</li>' : '';
				$titleStr   = ( $values['title_classes'][0] ) ? '<li><strong><i class="fa fa-header"></i> Widget Title:</strong> ' . implode( ', ', $values['title_classes'] ) . '</li>' : '';


				$choices[ $index ] = array(
					'name' => $values['name'],
					'desc' => '<ul>' . $globalStr . $desktopStr . $mobileStr . $titleStr . '</ul>'
				);
			}
		}
	}

	return $choices;
}

function widget_class_choices() {

	$choices = array_merge(
		background_colors(),
		border_colors(),
		border_size_position(),
		button_colors(),
		button_fonts(),
		button_sizes(),
		fixed_line_heights(),
		heading_colors(),
		heading_fonts(),
		heading_sizes(),
		link_colors(),
		misc(),
		rounded_corners(),
		spacing(),
		text_align(),
		text_colors(),
		text_fonts(),
		text_sizes(),
		array(
			'match-widget-height' => array(
				'name' => 'Match Widget Height',
				'desc' => 'Stretches all widgets in this widget area with this class to meet the height of the tallest naturally rendered',
			)
		)
	);

	// Remove stuff we do not want
	unset(
		$choices[ 'no-gutters' ],
		$choices[ 'hide' ],
		$choices[ 'window-width' ]
	);

	// Sort the choices
	asort( $choices );

	return $choices;
}

function widget_breakpoint_choices() {

	$choices = array_merge(
		border_size_position(),
		button_sizes(),
		fixed_line_heights(),
		heading_sizes(),
		misc(),
		spacing(),
		text_align(),
		text_sizes()
	);

	// Remove stuff we do not want
	unset(
		$choices[ 'no-gutters' ],
		$choices[ 'window-width' ]
	);

	// Sort the choices
	asort( $choices );

	return $choices;
}

function widget_title_class_choices() {

	$array = array(
		'force-over-padding' => array(
			'name' => 'Force To Edges',
			'desc' => 'Overlap the container padding. This allows the title to go from edge to bono. (⌐■_■)'
		)
	);

	$choices = array_merge(
		$array,
		background_colors(),
		border_colors(),
		border_size_position(),
		rounded_corners(),
		spacing(),
		text_align(),
		text_colors(),
		text_fonts(),
		text_sizes()
	);

	// Remove stuff we do not want
	unset(
		$choices['no-gutters']
	);

	// Sort the choices
	asort( $choices );

	return $choices;
}

/*
 * ROW READY CLASSES
 */
function row_class_choices() {

	$row_spacing = array();

	/*
	 * Logic to allow only top and bottom options for borders.
	 * We will never want (never say never!) borders on the right or left of ROWS
	 */
	$borders = border_size_position();
	$spacing = spacing();

	// Unset Left and Right Borders
	foreach ( $borders as $key => $values ) {
		if ( preg_match( '/left/', $key ) || preg_match( '/right/', $key ) ) {
			unset( $borders[ $key ] );
		}
	}

	// Unset Left and Right Padding
	foreach ( $spacing as $key => $values ) {
		if ( preg_match( '/top/', $key ) || preg_match( '/bottom/', $key ) ) {
			$row_spacing[ $key ] = $values;

			if ( preg_match( '/margin/', $key ) ) {
				unset( $row_spacing[ $key ] );
			}
		}
	}

	$force_no_padding = array(
		'force-no-padding' => array(
			'name' => 'Force Container To Edge',
			'desc' => 'Removes the left and right padding from the row so that it&apos;s container can touch the edge of the window.',
		)
	);

	// Collect our choices and merge them
	$choices = array_merge(
		$borders,
		background_colors(),
		border_colors(),
		$row_spacing,
		$force_no_padding
	);

	// Remove stuff we do not want
	unset(
		$choices['border'],
		$choices['window-width']
	);

	// Apply filters to our choices
	$choices = apply_filters( 'stellar_row_class_choices', $choices );

	// Sort the choices
	asort( $choices );

	return $choices;
}

function row_breakpoint_choices() {

	$choices = array_merge(
		misc(),
		row_class_choices()
	);

	// Remove stuff we do not want
	$unset = array_merge(
		background_colors(),
		border_colors(),
		array( 'force-no-padding', 'window-width' )
	);

	$choices = array_diff_key( $choices, $unset );

	// Apply filters to our choices
	$choices = apply_filters( 'stellar_row_breakpoint_choices', $choices );

	// Sort the choices
	asort( $choices );

	return $choices;
}

function row_container_class_choices() {

	// Collect our choices and merge them
	$choices = array_merge(
		background_colors(),
		border_colors(),
		border_size_position(),
		misc(),
		rounded_corners(),
		spacing()
	);

	// Remove stuff we do not want
	unset(
		$choices[ 'hide' ]
	);

	// Apply filters to our choices
	$choices = apply_filters( 'stellar_row_container_class_choices', $choices );

	// Sort the choices
	asort( $choices );

	return $choices;
}

/**
 * TODO: Audit to see if there are other things we want to include or unset here
 * @return array|mixed|void
 */
function row_container_breakpoint_choices() {

	// Collect our choices and merge them
	$choices = array_merge(
		border_size_position(),
		misc(),
		spacing()
	);

	// Remove stuff we do not want
	unset(
		$choices[ 'no-gutters' ],
		$choices[ 'hide' ],
		$choices[ 'window-width' ]
	);

	// Apply filters to our choices
	$choices = apply_filters( 'stellar_row_container_breakpoint_choices', $choices );

	// Sort the choices
	asort( $choices );

	return $choices;

}


function row_height_class_choices() {
	// Min height
	$heights = array(
		'auto' => 'Auto (Content Determined)',

		'min-height-viewport' => 'Lock to Viewport Height',
	);

	$range = range( 100, 800, 25 );
	foreach ( $range as $number ) {
		$heights["min-height-{$number}"] = "{$number}px min-height";
	}

	return $heights;
}

/*
 * BUCKET WIDGET BLOCKS
 */
function bf_bucket_heading_classes() {

	$choices = array_merge(
		background_colors(),
		border_colors(),
		border_size_position(),
		fixed_line_heights(),
		heading_colors(),
		heading_fonts(),
		heading_sizes(),
		rounded_corners(),
		spacing(),
		text_align()
	);

	unset(
		$choices['no-gutters']
	);

	asort( $choices );

	return $choices;
}

function bf_bucket_heading_breakpoint() {

	$choices = array_merge(
		border_size_position(),
		fixed_line_heights(),
		heading_sizes(),
		spacing(),
		text_align()
	);

	unset(
		$choices['no-gutters']
	);

	asort( $choices );

	return $choices;
}

function bf_bucket_text_classes() {
	$choices = array_merge(
		background_colors(),
		border_colors(),
		border_size_position(),
		fixed_line_heights(),
		rounded_corners(),
		spacing(),
		text_align(),
		text_colors(),
		text_fonts(),
		text_line_height(),
		text_sizes()
	);

	unset(
		$choices['no-gutters']
	);

	asort( $choices );

	return $choices;
}

function bf_bucket_text_breakpoint() {
	$choices = array_merge(
		border_size_position(),
		fixed_line_heights(),
		spacing(),
		text_align(),
		text_sizes()
	);

	unset(
		$choices['no-gutters']
	);

	asort( $choices );

	return $choices;
}

function bf_bucket_image_classes() {
	$choices = array_merge(
		background_colors(),
		border_colors(),
		border_size_position(),
		link_colors(),
		rounded_corners(),
		spacing(),
		text_align(),
		text_colors(),
		text_sizes(),
		array(
			'circle-image' => array(
				'name' => "Circle Image",
				'decs' => 'Use with a square image to make it a circle.'
			)
		)
	);

	unset(
		$choices['no-gutters']
	);

	asort( $choices );

	return $choices;
}

function bf_bucket_image_breakpoint() {
	$choices = array_merge(
		border_size_position(),
		spacing(),
		text_align(),
		text_sizes()
	);

	unset(
		$choices['no-gutters']
	);

	asort( $choices );

	return $choices;
}

function bf_bucket_button_classes() {
	$choices = array_merge(
		background_colors(),
		border_colors(),
		border_size_position(),
		button_colors(),
		button_fonts(),
		button_sizes(),
		fixed_line_heights(),
		link_colors(),
		rounded_corners(),
		spacing(),
		text_align(),
		text_fonts(),
		text_sizes()
	);

	unset(
		$choices['no-gutters']
	);

	asort( $choices );

	return $choices;
}

function bf_bucket_button_breakpoint() {
	$choices = array_merge(
		border_size_position(),
		button_sizes(),
		fixed_line_heights(),
		spacing(),
		text_align(),
		text_sizes()
	);

	unset(
		$choices['no-gutters']
	);

	asort( $choices );

	return $choices;
}

/*
 * FILTERED WIDGETS
 */
function menu_bar_class_choices() {

	$choices = array_merge(
		background_colors(),
		border_colors(),
		border_size_position(),
		fixed_line_heights(),
		menu_dividers(),
		misc(),
		rounded_corners(),
		spacing(),
		text_align(),
		text_fonts(),
		text_sizes(),
		array(
			'menu-uppercase'      => array(
				'name' => 'Menu: Uppercase',
				'desc' => 'Uppercase top level menu items only.'
			),
			'match-widget-height' => array(
				'name' => 'Match Widget Height',
				'desc' => 'Stretches all widgets in this widget area with this class to meet the height of the tallest naturally rendered',
			)
		)
	);

	// Remove stuff we do not want
	unset(
		$choices[ 'no-gutters' ],
		$choices[ 'window-width' ],
		$choices[ 'text-uppercase' ],
		$choices[ 'text-italic' ]
	);

	// Sort the choices
	asort( $choices );

	return $choices;
}

function menu_button_bar_class_choices() {

	$choices = array_merge(
		background_colors(),
		border_colors(),
		border_size_position(),
		button_colors(),
		button_sizes(),
		fixed_line_heights(),
		misc(),
		spacing(),
		text_align(),
		text_fonts(),
		text_sizes()
	);

	// Remove stuff we do not want
	unset(
		$choices[ 'no-gutters' ],
		$choices[ 'window-width' ]
	);

	// Sort the choices
	asort( $choices );

	return $choices;
}

function menu_bar_breakpoint_choices() {

	$choices = array_merge(
		border_size_position(),
		misc(),
		spacing(),
		text_align(),
		text_sizes()
	);

	// Remove stuff we do not want
	unset(
		$choices[ 'no-gutters' ],
		$choices[ 'window-width' ]
	);

	// Sort the choices
	asort( $choices );

	return $choices;
}


function brightfire_forms_class_choices() {

	$choices = array_merge( button_text_sizes(), widget_class_choices() );

	// Sort the choices
	asort( $choices );

	return $choices;
}

function brightfire_forms_breakpoint_choices() {

	$choices = array_merge( button_text_sizes(), widget_breakpoint_choices() );

	// Sort the choices
	asort( $choices );

	return $choices;
}

function column_gutter_padding_choices() {

	$choices = array(
		'gutter-none' => 'None',
		'gutter-base' => 'Base',
		'gutter-1-5'  => 'Base x 1.5',
		'gutter-2'    => 'Base x 2',
		'gutter-2-5'  => 'Base x 2.5',
		'gutter-3'    => 'Base x 3',
	);

	return $choices;
}

/*
 * Filter the GLOBAL widget class choices
 */
add_filter( 'stellar_widget_class_choices', function ( $choices, $widget ) {

	if ( is_a( $widget, 'WP_Widget' ) ) {

		// MENU BAR WIDGET
		if ( 'brightfire_menu_bar' == $widget->id_base ) {
			$choices = menu_bar_class_choices();
		}

		// MENU BUTTON BAR WIDGET
		if ( 'brightfire_menu_button_bar' == $widget->id_base ) {
			$choices = menu_button_bar_class_choices();
		}

		// MENU BUTTON BAR WIDGET
		if ( 'brightfire_menu_inline' == $widget->id_base ) {
			$choices = array_merge( $choices, menu_dividers(), fixed_line_heights() );
		}

		if( 'brightfire_menu_stacked' == $widget->id_base ) {
			$choices = array_merge( $choices, fixed_line_heights() );
		}

		// BRIGHTFIRE FORMS WIDGET
		if ( 'brightfire_form_widget' == $widget->id_base ) {
			$choices = brightfire_forms_class_choices();
		}

		// BRIGHTFIRE FORMS WIDGET
		if ( 'bf_cpc' == $widget->id_base ) {
			$choices = array_merge( $choices, text_line_height() );
			asort( $choices );
		}
	}

	return $choices;

}, 10, 2 );

/*
 * Filter the BREAKPOINT widget class choices
 */
add_filter( 'stellar_widget_breakpoint_choices', function ( $choices, $widget ) {

	if ( is_a( $widget, 'WP_Widget' ) ) {

		// MENU BAR WIDGET
		if ( 'brightfire_menu_bar' == $widget->id_base ) {
			$choices = menu_bar_breakpoint_choices();
		}

		// BRIGHTFIRE FORMS WIDGET
		if ( 'brightfire_form_widget' == $widget->id_base ) {
			$choices = brightfire_forms_breakpoint_choices();
		}
	}

	return $choices;

}, 10, 2 );