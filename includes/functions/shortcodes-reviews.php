<?php


namespace BrightFire\Theme\Stellar;

/**
 * Shortcode for Stellar Customer Reviews
 *
 * @param $atts
 *
 * @return string
 */
function bf_stellar_reviews( $atts ) {
	$atts = shortcode_atts(
		array(
			'limit'     => '15', // Number of posts per page
			'schema'    => \BrightFire\Plugin\Reviews\Settings\get_option_array( 'bf_review', 'schema' ),
			'rating_color' => 'rating-gold',
			'social_style' => 'circle',
			'show_image' => 'hide-avatar',
			'widget'    => 'false', // backport for reviews query model
			'slider'    => false
		), $atts, 'bf_review'
	);

	// This should always have a value to validate
	if ( '' == $atts['schema'] ) {
		$atts['schema'] = 'LocalBusiness';
	}

	$output = '';

	// Company Image
	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );

	$reviews = \BrightFire\Plugin\Reviews\Query\get_reviews($atts);

	$slider_class_wrapper = ( $atts['slider'] ? ' review-slider' : '' );
	$slider_class_ul = ( $atts['slider'] ? ' class="slides"' : '' );

	$output .= '<div class="brightfire-reviews-widget-wrap">';

	$output .= '<div class="brightfire-reviews' . $slider_class_wrapper .' ' . $atts['show_image'] . '">';

		$output .= '<ul' . $slider_class_ul . '>';

	foreach( $reviews['review'] as $review ) {

		$initials = bf_stellar_reviews_initials( $review['review_author'] );

		$output .= '<li class="review-entry" itemprop="review" itemscope itemtype="http://schema.org/Review">';

		// Item Reviewed
		$output .= '<span itemprop="itemReviewed" itemscope itemtype="http://schema.org/' . $atts['schema'] . '">';
		$output .= '<meta itemprop="name" content="' . do_shortcode('[bf_location field=company_name]') . '">';
		$output .= '<meta itemprop="address" content="' . do_shortcode('[bf_location field=address] [bf_location field=city] [bf_location field=state] [bf_location field=zip]') . '">';
		$output .= '<meta itemprop="telephone" content="' . do_shortcode('[bf_location field=phone]') . '">';
		$output .= '<meta itemscope itemprop="image" content="' . $image[0] . '">';
		$output .= '</span>';


		$output .= '<div class="review-image">';

		if ( ! empty( $review['image_url'] ) ) {
			$output .= '<img class="review-author-image" src="' . $review['image_url'] . '" alt="' . $review['image_alt'] . '" />';
		} else {
			$output .= '<div class="review-author-initials ' . $review['review_source'] . '">' . $initials . '</div>';
		}

		$output .= '</div>';

		$output .= '<div class="review-info">';

		if( $review['rating'] ) {
			$output .= '<div class="aggregate-rating-stars ' . $atts['rating_color'] . '" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">';
			$output .= '<meta itemprop="ratingValue" content="' . $review['rating'] . '">';
			$output .= bf_stellar_reviews_stars( $review['rating'], $review['review_source'], $atts['social_style'] );
			$output .= '</div>';
		}

		$output .= '<span class="review-pullquote" itemprop="description">' . $review['review_pullquote'] . '</span>';

		$output .= '<span class="review-author" itemprop="author" itemscope itemtype="http://schema.org/Person"><span itemprop="name" class="reviewer-author-name">' . $review['review_author'] . '</span>, ' . $review['review_author_meta'] . '</span>';

		if ( '' != $review['review'] && 80 < strlen( $review['review'] ) && ! $atts['slider'] ) {
			$output .= '<a class="review-expand" href="javascript:void(0);" data-show="#review-' . $review['id'] . '">Read Full Review</a>';
			$output .= '<div id="review-' . $review['id'] . '" class="review-body" itemprop="reviewBody">' . apply_filters( 'the_content', $review['review'] ) . '</div>';
		}


		$output .= '</div>';
		$output .= '<div class="clear"></div>';
		$output .= '</li>';

	}

	$output .= '</ul>';
	$output .= '</div>';

	if( $atts['slider'] ) {
		$output .= '<a href="' . get_home_url() . '/reviews" class="review-archive-link">&nbsp;</a>';
		$output .= '<a href="javascript: void(0);" class="review-archive-text">See All Reviews</a>';
	}

	$output .= '</div>';

	return $output;

}
add_shortcode( 'bf_stellar_reviews', __NAMESPACE__ . '\bf_stellar_reviews' );

/**
 * Shortcode for Stellar Reviews Aggregate
 *
 * @param $atts
 *
 * @return string
 */
function bf_stellar_reviews_aggregate( $atts ) {
	$atts = shortcode_atts(
		array(
			'rating_color'    => 'rating-gold',
		), $atts, 'bf_stellar_review'
	);

	$output = '';

	// Aggregate Data
	$data = bf_stellar_reviews_aggregate_data();

	// Stars
	$output .= '<div class="aggregate-rating" itemscope itemtype="http://schema.org/InsuranceAgency">';
	$output .= '<meta itemprop="name" content="' . do_shortcode('[bf_location field=company_name]') . '">';
	$output .= '<meta itemprop="address" content="' . do_shortcode('[bf_location field=address] [bf_location field=city] [bf_location field=state] [bf_location field=zip]') . '">';
	$output .= '<meta itemprop="telephone" content="' . do_shortcode('[bf_location field=phone]') . '">';

	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );

	$output .= '<meta itemscope itemprop="image" content="' . $image[0] . '">';
	$output .= '<div class="aggregate-rating-stars ' . $atts['rating_color'] . '">';
	$output .= $data['stars'];
	$output .= '</div>';

	// Callout
	$output .= '<div class="aggregate-rating-callout" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">';
	$output .= '<meta itemprop="itemReviewed" content="InsuranceAgency">';
	$output .= $data['verbiage'];
	$output .= '</div>';
	$output .= '</div>';

	return $output;

}
add_shortcode( 'bf_stellar_reviews_aggregate', __NAMESPACE__ . '\bf_stellar_reviews_aggregate' );


function bf_stellar_reviews_stars( $rating = 0, $social = '', $social_branding = 'circle' ) {

	$output = $social_output = '';
	$rating_left = $rating;

	// Convert social to source properly
	switch( $social ) {
		case 'google' :
			$fa_social = 'google-plus';
			break;
		default:
			$fa_social = $social;
			break;
	}

	// Address Social Icon if we need one

	if( ! empty( $social ) ) {
		switch ( $social_branding ) {
			case 'square' :
				$social_output = '<span class="fa-stack"><i class="fa fa-square fa-stack-2x ' . $fa_social . '"></i><i class="fa fa-' . $fa_social . ' fa-stack-1x fa-inverse"></i></span>';
				break;
			case 'logo' :
				$social_output = '<i class="fa fa-' . $fa_social . ' ' . $fa_social . ' logo"></i>';
				break;
			default :
				$social_output = '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x ' . $fa_social . '"></i><i class="fa fa-' . $fa_social . ' fa-stack-1x fa-inverse"></i></span>';
				break;
		}

		// Review Source URLs
		$review_source_url = esc_url( do_shortcode( \BrightFire\Plugin\Reviews\Settings\get_option_array( 'bf_review', $social ) ) );
		if ( '' == $review_source_url ) {
			// Set our review source URL via hardcoded filter option
			$review_source_url = apply_filters( 'bf_review_sources_urls', null, $social );
		}

		$output .= '<span class="link-color-brands">';
		$output .= '<a href="' . $review_source_url . '" target="_blank" class="' . $social . '">';
		$output .= $social_output;
		$output .= '</a>';
		$output .= '</span>';
	}





	// Dole out our stars until we're out of stars
	for( $i = 0; $i < 5; $i++ ) {

		$perc = 0;

		if( $rating_left >= 1 ) {
			$perc = 100;
		} elseif ( $rating_left > 0 && $rating_left < 1 ) {
			$perc = ( $rating_left / 1 ) * 100;
		}

		$output .= '<span class="aggregate-star fa-stack">';
		$output .= '<i class="fa fa-star star-gray fa-stack-1x"></i>';
		$output .= '<i class="fa fa-star star-fill fa-stack-1x" style="width: ' . $perc . '%;"></i>';
		$output .= '</span>';

		$rating_left--;
	}

	return $output;

}

function bf_stellar_reviews_aggregate_data() {

	// Setup
	$average_rating = 0;
	$company_name = do_shortcode('[bf_location field=company_name]');

	// Get Rated Reviews
	$all_rated_reviews = \BrightFire\Plugin\Reviews\get_ratings_for_reviews();
	$total_rated_reviews = count( $all_rated_reviews );

	// Calculate Average
	if ( 0 < $total_rated_reviews ) {
		$average_rating =  array_sum( $all_rated_reviews ) / $total_rated_reviews;
		$average_rating = round( $average_rating, 1);
	}

	// Return aggregate data
	return array(
		'total_rated_reviews' => $total_rated_reviews,
		'average_rating' => $average_rating,
		'callout' => "See why {$company_name} is rated <span itemprop='ratingValue'>{$average_rating}</span> out of 5 stars!",
		'verbiage' => "Rated <span itemprop='ratingValue'>{$average_rating}</span> out of 5 based on <span itemprop='reviewCount'>{$total_rated_reviews}</span> rated reviews.",
		'stars' => bf_stellar_reviews_stars( $average_rating )
	);
}

function bf_stellar_reviews_initials( $name ) {

	$initials = '';

	// Alpha Numeric and Spaces only
	$name = preg_replace( '/[^a-zA-Z0-9 ]+/', '', $name );

	// Turn name into array
	$names = explode( ' ', $name );

	// Grab the first letter of each name
	foreach ( $names as $initial ){
		$initials .= ( isset( $initial[0] ) ? $initial[0] : '');
	}

	// Get only two initials and make sure they are upper case
	$initials = strtoupper( substr ( $initials , 0, 2 ) );

	return $initials;
}