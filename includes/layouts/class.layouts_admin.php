<?php

/**
 * Class Stellar_Layouts_Admin
 *
 * Extends the Stellar_Layouts class to provide admin functionality.
 * Instantiated on 'admin_init' and stored in the global $stellar_layout
 *
 */
Class Stellar_Layouts_Admin extends Stellar_Layouts {

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Extends Create Defaults to save the option to the database
	 * @return array
	 */
	public function create_defaults() {
		$parsed_defaults = parent::create_defaults();

		$this->update_layout( $parsed_defaults );

		return $parsed_defaults;
	}

	/**
	 * Updates our stellar_layout option
	 * @param $layout_object
	 *
	 * @return bool
	 */
	public function update_layout( $layout_object ) {

		$this->set_layout( $layout_object );

		return update_option( 'stellar_layout', $layout_object );

	}

	/**
	 * Updates a single object (sidebar, menu_panel, template, row)
	 *
	 * @param $option
	 * @param $item
	 *
	 * @uses update_layout()
	 *
	 * @return bool
	 */
	public function update_layout_item( $option, $item ) {

		$current = $new = $this->get_layout();

		$new[ $option ][ $item[ 'id' ] ] = $item[ 'data' ];

		return $this->update_layout_option( $option, $new[ $option ] );

	}

	/**
	 * Removes a single object (sidebar, menu_panel, template, row)
	 * @param $option
	 * @param $id
	 *
	 * @uses update_layout()
	 *
	 * @return bool
	 */
	public function delete_layout_item( $option, $id ) {

		$current_layout = $new_layout = $this->get_layout();

		// Delete the index from new optoin
		unset( $new_layout[ $option ][ $id ] );

		// Send Update Request
		return $this->update_layout( $new_layout );

	}

	/**
	 * Updates the layout option array of objects
	 *
	 * @param $option
	 * @param $value
	 *
	 * @uses update_layout()
	 *
	 * @return bool
	 */
	public function update_layout_option( $option, $value ) {

		$layout = $this->get_layout();

		$layout[ $option ] = $value;

		return $this->update_layout( $layout );

	}

	/**
	 * Check to see if an object within an option is required
	 * or used by something
	 *
	 * @param $option
	 * @param $id
	 *
	 * @return bool
	 */
	public function is_required( $option, $id ) {

		$check_ids = array();
		
		switch( $option ) {
			case 'templates' :
				$assignments = $this->get_layout_option( 'assignments' );
				$check_ids = array_values( array_unique( array_merge( array_values( $assignments['page'] ), array_values( $assignments['required'] ) ) ) );
				break;
			case 'rows' :
				// Build an array of all template rows
				$templates = $this->get_layout_option( 'templates' );
				foreach( $templates as $template_id => $template_data ) {
					$usages = \BrightFire\Theme\Stellar\row_usage_choices();
					foreach ( $usages as $section => $label ){
						if ( isset( $template_data[ $section ] ) ){
							$check_ids = array_merge( $check_ids, $template_data[ $section ] );
						}
					}
					$check_ids = array_unique( $check_ids );
				}
				break;
			case 'sidebars' :
				// Build an array of all sidebar assignemnts
				$assignments = $this->get_layout_option( 'assignments' );
				$check_ids = array_values( array_unique( array_values( $assignments['sidebars'] ) ) );
				break;
			case 'menu_panels' :

				/** TODO: find a classy way to do this */

				break;
			default :

				/** TODO: is there a need for a default case action */

				break;
		}

		// Check to see what is or is not required
		return \BrightFire\Theme\Stellar\in_array_r( $id, $check_ids );

	}

	/**
	 * Return an array of id's associated with the item.
	 *
	 * @TODO This can be extended to templates, sidebars etc later.. currently only gets template ids associated with rows
	 *
	 * @param $id
	 *
	 * @return array
	 */
	public function get_associated_ids( $id ) {

		$arr = array(
			'templates' => array(),
			'rows'      => array(),
			'sidebar'   => array(),
			'posts'     => array()
		);

		//Check what type of ID we have
		$templates      = $this->get_layout_option( 'templates' );
		$rows           = $this->get_layout_option( 'rows' );

		switch( $id ) {
			case (array_key_exists( $id, $rows ) && $templates ) :
				foreach ( $templates as $template => $data ) {
					if ( \BrightFire\Theme\Stellar\in_array_r( $id, $data ) ) {
						array_push( $arr[ 'templates' ], $template  );
					}
				}
				break;
		}
		
		return $arr;
	}
}