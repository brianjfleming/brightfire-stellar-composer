<?php

namespace BrightFire\Theme\Stellar;

/**
 * Filter page_templates to inject our custom layouts into the
 * template selection list on pages
 *
 * @param $page_templates
 *
 * @return array
 */
function filter_page_templates( $page_templates ) {

	global $stellar_layout;

	$templates = $stellar_layout->get_layout_option( 'templates' );

	foreach ( $templates as $id => $template ) {

		$page_templates = array_merge( $page_templates, array( $id => $template['name'] ) );

	}

	return $page_templates;
}

/**
 * Filter the '_wp_page_template' meta option to get this value
 * from our layout page assignments. This helps recipes travel.
 *
 * @param $metadata
 * @param $object_id
 * @param $meta_key
 * @param $single
 *
 * @return mixed original metadata or template ID string
 */
function ensure_template_assignments( $metadata, $object_id, $meta_key, $single ) {

	if( '_wp_page_template' == $meta_key ) {

		global $stellar_layout;

		// Get assigned templates
		$assigned_templates = $stellar_layout->get_layout_option( 'assignments' )[ 'page' ];

		$page_slug = get_post( $object_id )->post_name;

		if( array_key_exists( $page_slug, $assigned_templates ) ) {

			return $assigned_templates[ $page_slug ];

		} else {

			return $metadata;
		}

	} else {
		return $metadata;
	}
}

/**
 * Use fontawesome icon in place of email text
 *
 * @return string
 */
function email_icon() {
	return '<i class="fa fa-envelope" aria-hidden="true"></i>';
}

/**
 * Use fontawesome icon in place of linkedin text
 *
 * @return string
 */
function linkedin_icon() {
	return '<i class="fa fa-linkedin-square" aria-hidden="true"></i>';
}