<?php

namespace BrightFire\Theme\Stellar;

/**
 * Instantiation of the stellar_layout global object
 */
function stellar_layout_init() {

	global $stellar_layout;

	if( is_admin() ) {
		$stellar_layout = new \Stellar_Layouts_Admin();
	} else {
		$stellar_layout = new \Stellar_Layouts();
	}

}

/**
 * Hook save_post to save page template assignments
 *
 * @param $post_id
 * @param $post
 *
 * @return bool
 */
function save_post_template( $post_id, $post ) {

	global $stellar_layout;

	// This only works for Pages
	if ( 'page' == $post->post_type ) {

		if ( isset( $_REQUEST[ 'page_template' ] ) ) {
			$assignment = $_REQUEST[ 'page_template' ];
		} else {
			$assignment = ''; // For posterity
		}

		// We have an assigned Template
		if ( ! empty ( $assignment ) ) {

			// Let's get all assignments
			$assignments = $stellar_layout->get_layout_option( 'assignments' );

			// This is our assignment identifier
			$page = $post->post_name;

			// See if we have assigned templates to pages
			if ( isset( $assignments[ 'page' ] ) ) {

				// Let's only deal with those pages
				$pages = $assignments[ 'page' ];

			} else {

				// Make a container
				$pages = array();

			}

			if ( '-1' == $assignment ) {
				return false;
			}

			// Is this set to "default" ?
			if ( 'default' == $assignment ) {

				// Let's bounce, we don't need this
				if ( ! array_key_exists( $page, $pages ) ) {

					return false;

					// Otherwise let's remove the old setting
				} else {

					unset( $pages[ $page ] );
					$data = $pages;
				}

				// We have a template set for this page
			} else {

				// Add the page template assignment to our array data
				$data = array_merge( $pages, array( $page => $assignment ) );

			}

			// Update the assignments
			$assignments['page'] = $data;
			$stellar_layout->update_layout_option( 'assignments', $assignments );
		}
	}

}

/**
 * Save single sidebar selection
 *
 * @param $post_id
 * @param $post
 *
 * @return bool|void
 */
function save_post_sidebar( $post_id, $post ) {

	global $stellar_layout;

	// Check if our nonce is set.
	if ( ! isset( $_REQUEST['sidebar_meta_box_nonce'] ) ) {
		return;
	}


	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_REQUEST['sidebar_meta_box_nonce'], 'sidebar_meta_box' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( wp_is_post_autosave( $post ) || wp_is_post_revision( $post ) ) {
		return;
	}

	// Check the user's permissions.
	if ( ! current_user_can( BF_STELLAR_LAYOUTS_CAPABILITY, $post_id ) ) {
		return;
	}

	/* OK, it's safe for us to save the data now. */

	if ( isset( $_REQUEST[ 'layouts_sidebar' ] ) ) {
		$sidebar = $_REQUEST[ 'layouts_sidebar' ];
	} else {
		$sidebar = ''; // For posterity
	}

	// We have an assigned Sidebar
	if ( !empty( $sidebar ) ) {

		// Let's get all assignments
		$assignments = $stellar_layout->get_layout_option( 'assignments' );

		$page = $post->post_name;

		// See if we have assigned sidebars
		if ( isset( $assignments[ 'sidebars' ] ) ) {

			// Let's only deal with those pages
			$sidebars = $assignments[ 'sidebars' ];

		} else {

			// Make a container
			$sidebars = array();

		}

		// Is this set to "default" ?
		if ( 'default' == $sidebar ) {

			// Let's bounce, we don't need this
			if ( ! array_key_exists( $page, $sidebars ) ) {

				return false;

				// Otherwise let's remove the old setting
			} else {

				unset( $sidebars[ $page ] );
				$data = $sidebars;
			}

			// We have a sidebar set for this page
		} else {

			// Add the page template assignment to our array data
			$data = array_merge( $sidebars, array( $page => $sidebar ) );

		}

		// Update the assignments
		$assignments['sidebars'] = $data;
		$stellar_layout->update_layout_option( 'assignments', $assignments );
	}
}

/**
 * Meta box display callback
 *
 * @param string $post
 */
function page_sidebar( $post = '' ) {

	global $stellar_layout;

	wp_nonce_field( 'sidebar_meta_box', 'sidebar_meta_box_nonce' );

	$selected = '';
	$assignments = $stellar_layout->get_layout_option( 'assignments' );
	if ( $post ){
		$post_name = $post->post_name;
	} else {
		$post_name = '';
	}

	if ( isset( $assignments[ 'sidebars'] ) && array_key_exists( $post_name, $assignments[ 'sidebars' ] ) ) {
		$selected = $assignments[ 'sidebars' ][ $post_name ];
	}

	echo select_sidebar_choices( 'layouts_sidebar', $selected );
}

/**
 * Adds Sidebar column to edit screen
 *
 * @param $columns
 *
 * @return mixed
 */
function add_sidebar_column( $columns ) {

	return array_merge( $columns, array( 'sidebar' => __( 'Sidebar', 'bf_stellar' ) ) );
}

/**
 * Adds name of sidebar to the Sidebar column
 *
 * @param $column
 * @param $post_id
 */
function display_sidebar_column( $column, $post_id ) {

	global $stellar_layout;

	if ($column == 'sidebar'){

		$post = get_post( $post_id );

		$assignments = $stellar_layout->get_layout_option( 'assignments' );

		if ( isset( $assignments[ 'sidebars' ] ) && array_key_exists( $post->post_name, $assignments[ 'sidebars' ] ) ) {
			$sidebar_id = $assignments[ 'sidebars' ][ $post->post_name ];
			$sidebar_data = $stellar_layout->get_layout_item( 'sidebars', $sidebar_id );
			$name = '<a href="' . admin_url() . 'admin.php?page=stellar-settings&tab=widget_areas">' . $sidebar_data['name'] . '</a>';
		} else {
			$name = '';
		}
		echo $name;
	}
}

/**
 * Adds Template column to edit screen
 *
 * @param $columns
 *
 * @return mixed
 */
function add_template_column( $columns ) {

	return array_merge( $columns, array( 'template' => __( 'Template', 'bf_stellar' ) ) );
}

/**
 * Adds name of template to the Template column
 *
 * @param $column
 * @param $post_id
 */
function display_template_column( $column, $post_id ) {

	if ($column == 'template'){

		global $stellar_layout;

		// Grab our templates
		$templates = $stellar_layout->get_layout_option( 'templates' );

		// Get template of this page
		$template =  get_post_meta( $post_id, '_wp_page_template', true );

		// If we have a template override, display it's name
		if( !empty( $templates[ $template ] ) ) {
			echo $templates[ $template ][ 'name' ];
		}
	}
}

function display_sidebar_bulk_edit( $column_name, $post_type ) {

	if ($column_name != 'sidebar') return;
	?>
	<fieldset class="inline-edit-col-right">
		<div class="inline-edit-col">
			<label><span class="title">Sidebar</span>
				<?php page_sidebar(); ?></label>
		</div>
	</fieldset>
	<?php
}

function add_page_sidebar_metabox() {
	add_meta_box( 'page_sidebar', __( 'Sidebar', 'bf_stellar' ), __NAMESPACE__ . '\page_sidebar', 'page', 'side' );
}

function register_page_layout_actions() {

	if ( 'page' == get_current_screen()->post_type ) {

		add_action( 'save_post', __NAMESPACE__ . '\save_post_sidebar', 10, 2 );
		add_action( 'save_post', __NAMESPACE__ . '\save_post_template', 10, 2 );
		add_action( 'manage_pages_custom_column' , __NAMESPACE__ . '\display_template_column', 10, 2 );
		add_filter( 'manage_pages_columns' , __NAMESPACE__ . '\add_template_column' );
		add_action( 'manage_pages_custom_column' , __NAMESPACE__ . '\display_sidebar_column', 10, 2 );
		add_filter( 'manage_pages_columns' , __NAMESPACE__ . '\add_sidebar_column' );
		add_action( 'bulk_edit_custom_box', __NAMESPACE__ . '\display_sidebar_bulk_edit', 10, 2 );
		add_action( 'add_meta_boxes', __NAMESPACE__ . '\add_page_sidebar_metabox' );

	}
}

function register_widget_areas() {

	global $stellar_layout;

	$rows = $stellar_layout->get_layout_option( 'rows' );

	/**
	 * Create widget areas for rows
	 */
	foreach ( $rows as $row_id => $row_data ) {

		$name = $row_data[ 'name' ];
		$desc = $row_data[ 'description' ];
		$left = $row_data[ 'left' ];
		$right = $row_data[ 'right' ];

		// Container for widget areas
		$widget_areas = [];

		if ( ! empty( $left ) ) {

			$widget_areas[ 'left' ] = array(
				'name'  => ( 12 == $left ) ? $name : $name . ' Left',
				'id'    => $row_id . '-left',
			);
		}

		if ( ! empty( $right ) ) {

			$widget_areas[ 'right' ] = array(
				'name'  => ( 12 == $right ) ? $name : $name . ' Right',
				'id'    => $row_id . '-right',
			);
		}

		foreach ( $widget_areas as $area ) {
			$args = array(
				'name'          => $area[ 'name' ],
				'id'            => $area[ 'id' ],
				'description'   => $desc,
				'class'         => '',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2 class="widgettitle">',
				'after_title'   => '</h2>'
			);

			register_sidebar( $args );
		}
	}

	/**
	 * Create widget areas for widget_areas
	 */
	foreach ( widget_area_types() as $option_id => $labels ) {

		$option = $stellar_layout->get_layout_option( $option_id );

		if ( ! empty( $option ) ) {

			foreach ( $option as $area_id => $area_data ) {

				$args = array(
					'name'          => $area_data[ 'name' ] . ' "' . $labels[ 'singular_label' ] . '"',
					'id'            => $area_id,
					'description'   => $area_data[ 'description' ],
					'class'         => '',
					'before_widget' => '<div id="%1$s" class="widget %2$s sidebar-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h2 class="widgettitle sidebar-widgettitle">',
					'after_title'   => '</h2>'
				);

				register_sidebar( $args );
			}
		}
	}
}

function bf_employee_filters() {

	// For Single employee post types, we want to ensure we apply needed filters
	// and adjust the CPC filters to run properly for display of employees
	if( is_singular( 'bf_employees' ) ) {

		add_filter('shortcode_atts_bf_employees', 'single_employee_attributes', 10, 3 );

		remove_filter( 'cpc_title', 'get_title' );
		add_filter('bf_employee_output_name', function() { return null; } );

		remove_filter( 'cpc_content', 'get_content' );
		add_filter('cpc_content', __NAMESPACE__ . '\bf_employee_single_output_content', 10, 2 );

	}

}

function bf_employee_single_output_content( $post ) {

	return do_shortcode_bf_employees( '' );

}