<?php


/**
 * Interface and Class for 'Stellar_Layouts'
 *
 * Responsibility: RETRIEVE stellar_layouts dB option and set class object;
 * if empty, then call create_defaults()
 *
 * Responsibility: RETRIEVE and SET globals for layouts; Will be utilized
 * by front-end scripts to get layout data, and parse appropriately for
 * rendering.
 *
 * Responsibility: RETRIEVE and SET globals for layouts; Will be utilized
 * by admin class to get layout data and update globals.
 *
 * Globals:
 *      $stellar_layout = array(
 *          'templates'   => array( stdObject() ),
 *          'rows'        => array( stdObject() ),
 *          'sidebars'    => array( stdObject() ),
 *          'menu_panels' => array( stdObject() ),
 *          'assignemnts' => array( 'required' => array(), 'page' => array() )
 *      );
 *
 */


/**
 * Interface Stellar_Layouts_IN
 */
Interface Stellar_Layouts_IN {

	public function get_layout();                       // Will Retrieve our $stellar_layout option
	public function set_layout( $value );               // Will Set our $stellar_layout option

	public function get_layout_option( $key );          // Return an index of our $stellar_layout option or return false
	public function set_layout_option( $key, $array ); // Set an key of our $stellar_layout option as an object eg: ('rows', array() )

	public function get_layout_item( $option, $id ); // Get an object by Option / ID from $stellar_layout eg: ( 'rows', 'header' )

	public function create_defaults();                  // Creates default settings; Layouts_Admin updates dB

}

/**
 * Class Stellar_Layouts
 */
Class Stellar_Layouts implements Stellar_Layouts_IN {

	private $stellar_layout;

	/**
	 * Stellar_Layouts constructor.
	 */
	public function __construct() {

		// Set our global from the database if we don't already have it
		if( empty( $this->stellar_layout ) ) {

			if( $current_option = get_option( 'stellar_layout' ) ) {
				$this->set_layout( $current_option );
			} else {
				$defaults = $this->create_defaults();
				$this->set_layout( $defaults );
			}

		}

//		\BFDEV\prettyprintr( $this->stellar_layout['sidebars'] );

	}

	/**
	 * @return array
	 */
	public function get_layout() {
		return $this->stellar_layout;
	}

	/**
	 * @param $value
	 */
	public function set_layout( $value ) {
		$this->stellar_layout = $value;
	}

	/**
	 * @param $option
	 * @param $id
	 *
	 * @return array | bool
	 */
	public function get_layout_item( $option, $id = NULL ) {

		// Switch Case for getting defaults
		$defaults = \BrightFire\Theme\Stellar\get_layout_option_defaults( $option );

		// If no ID, return defaults
		if( NULL == $id ) {
			return \BrightFire\Theme\Stellar\format_layout_item( $defaults );
		}

		if( $option_value = $this->get_layout_option( $option ) ) {

			$item_formatted = array(
				'id' => $id,
				'data' => $option_value[ $id ]
			);

			$item_parsed = \BrightFire\Theme\Stellar\format_layout_item( $item_formatted, $defaults );

			return $item_parsed[ $id ];

		} else {
			return false;
		}
	}

	/**
	 * @param $key
	 *
	 * @return array | bool
	 */
	public function get_layout_option( $key ) {

		if( is_array( $this->stellar_layout ) && array_key_exists( $key, $this->stellar_layout ) ) {
			return $this->stellar_layout[ $key ];
		} else {
			return false;
		}

	}

	/**
	 * @param $key
	 * @param $array
	 */
	public function set_layout_option( $key, $array ) {

		// Set our new value
		$this->stellar_layout[ $key ] = $array;

	}

	/**
	 * @return array
	 */
	public function create_defaults() {

		$default_layout_array = \BrightFire\Theme\Stellar\default_layout_array();
		$template_defaults = \BrightFire\Theme\Stellar\get_layout_option_defaults( 'templates' );
		$row_defaults = \BrightFire\Theme\Stellar\get_layout_option_defaults( 'rows' );
		$sidebar_defaults = \BrightFire\Theme\Stellar\get_layout_option_defaults( 'sidebars' );
		$menu_panel_defaults = \BrightFire\Theme\Stellar\get_layout_option_defaults( 'menu_panel' );
		$templates = $rows = $sidebars = $menu_panels = $assignments = array();

		// Templates
		foreach( $default_layout_array[ 'templates' ] as $template_data ) {
			$new_template = \BrightFire\Theme\Stellar\format_layout_item( $template_data, $template_defaults );
			if( is_array( $new_template ) ) { $templates = array_merge( $templates, $new_template ); }
		}

		// Rows
		foreach( $default_layout_array[ 'rows' ] as $row_data ) {
			$new_row = \BrightFire\Theme\Stellar\format_layout_item( $row_data, $row_defaults );
			if( is_array( $new_row ) ) { $rows = array_merge( $rows, $new_row ); }
		}

		// Sidebars
		foreach( $default_layout_array[ 'sidebars' ] as $sidebar_data ) {
			$new_sidebar = \BrightFire\Theme\Stellar\format_layout_item( $sidebar_data, $sidebar_defaults );
			if( is_array( $new_sidebar ) ) { $sidebars = array_merge( $sidebars, $new_sidebar ); }
		}

		// Menu Panels
		foreach( $default_layout_array[ 'menu_panels' ] as $menu_panel_data ) {
			$new_menu_panel = \BrightFire\Theme\Stellar\format_layout_item( $menu_panel_data, $menu_panel_defaults );
			if( is_array( $new_menu_panel ) ) { $menu_panels = array_merge( $menu_panels, $new_menu_panel ); }
		}

		// Assignments
		foreach( $default_layout_array[ 'assignments' ] as $type => $assignments_data ) {
			$assignments[ $type ] = $assignments_data;
		}

		$default_layout_parsed = array(
			'templates' => $templates,
			'rows' => $rows,
			'sidebars' => $sidebars,
			'menu_panels' => $menu_panels,
			'assignments' => $assignments,
		);

		return $default_layout_parsed;

	}

}