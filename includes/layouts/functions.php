<?php

namespace BrightFire\Theme\Stellar;

/**
 * @param $ids_position_array
 *
 * @return bool
 */
function sort_layout_items( $option, $sort = array() ) {

	global $stellar_layout;

	// Error if there is no row parameter
	if ( empty( $sort ) ) {
		return false;
	}

	$new = array();

	foreach( $sort as $item_id ) {

		$new[$item_id] = $stellar_layout->get_layout_item( $option, $item_id );

	}

	return $new;
}

/**
 * @param $item
 *          Data for a row, template, sidebar, or menu panel
 * @param $defaults
 *          Default values and index set for each
 *
 * @return bool|object
 */
function format_layout_item( $item, $defaults = array() ) {

	if( is_array( $item ) && array_key_exists( 'id', $item ) && array_key_exists( 'data', $item ) ) {

		$item_parsed = recurse_parse_args( $item, $defaults );

		if( ! empty( $defaults ) ) {
			$item_parsed['data'] = wp_parse_args( $item_parsed['data'], $defaults['data'] );
		}

		$id = $item_parsed['id'];
		$data = $item_parsed['data'];

		$rtn_item = array(
			$id => $data
		);

		return $rtn_item;

	} else if( is_object( $item ) ) {

		// Turn our object into a workable array
		$array = array();
		$array[ 'id' ] = $item->id;

		foreach ( $item->data as $key => $value ) {
			$array[ $key ] = $value;
		}

		return format_layout_item( $array, $defaults );

	} else {
		\BFDEV\prettyprintr( 'poor formated item' );
		\BFDEV\prettyprintr( $item );
	}

}

/**
 * Switch Case to return proper defaults
 * @param $option
 *
 * @return array
 */
function get_layout_option_defaults( $option ) {

	switch( $option ) {
		case 'templates' :
			$defaults = \BrightFire\Theme\Stellar\get_defaults_template();
			break;
		case 'rows' :
			$defaults = \BrightFire\Theme\Stellar\get_defaults_row();
			break;
		default :
			$defaults = array();
			break;
	}

	return $defaults;

}

function get_assigned_template_id() {

	global $stellar_layout;

	$assignments = $stellar_layout->get_layout_option( 'assignments' );
	$post = get_post();

	// Check if page has a template set
	$custom_template = get_page_template_slug();

	// No custom defined, get [required] layout template
	if ( empty( $custom_template ) ) {

		foreach ( $assignments[ 'required' ] as $assignment => $template_id ) {

			// For post types
			if ( is_singular( $assignment ) ) {

				if ( $post->post_type === $assignment ) {

					$id = $template_id;
				}

				// For archives
				// TODO: possibly allow templates to be set according to the Taxonomy of the archive.
			} elseif ( is_archive() && ( 'archive' == $assignment ) ) {

				$id = $template_id;

				// If home page is set to Latest Posts
			} elseif ( is_home() && ( 'archive' == $assignment ) ) {

				$id = $template_id;
			}
		}

		// Page has a custom template
	} elseif ( $custom_template ) {

		$id = $custom_template;


		// all else fails, set to default layout
	} else {

		// Default
		$id = apply_filters( 'default_required_template', 'page-default' );

	}

	return $id;
}

function build_row_parallax( $layers ) {

	$output = "<div class='stellar-row-parallax'>";

	$layer_total = count( $layers );

	if( $layer_total > 1 ) {
		$layer_total--;
		$speed = 0.1;
	} else {
		// Considerations for a single layer
		$layer_total = 1;
		$speed = 0.5;
	}

	$start = ( $speed * $layer_total );


	$depth = $start;

	foreach( $layers as $layer ) {

		$layer_styles = \BrightFire\Theme\Stellar\background_style_generator( $layer );
		$output .= '<div class="background-layer" style="' . $layer_styles . '" data-depth="' . $depth . '" data-type="parallax"></div>';

		$depth = $depth - $speed;
	}

	$output .= '</div>';

	return $output;
}

function build_row_flexslider( $slides, $options ) {

	$data_atts = array(
		'animation',
		'slideshowSpeed',
		'animationSpeed',
	);

	$data_attributes = '';

	foreach ( $data_atts as $attribute ) {

		$data = '';

		if ( isset( $options[ $attribute ] ) ) {
			$data = $options[ $attribute ];
		}

		$data_attributes .= " data-{$attribute}='{$data}'";
	}

	$output = "<div class='stellar-row-slider brightfire-slidebar'{$data_attributes}'><ul class='slides'>";
		foreach ( $slides as $slide ) {

			$slide_styles = \BrightFire\Theme\Stellar\background_style_generator( $slide );
			$output .= '<li style="' . $slide_styles . '"></li>';

		}
	$output .= '</ul></div>';
	
	return $output;
	
}

/**
 * Inject Stellar Long Quote Form
 */
function stellar_quote_long_form() {

	// only output if URL is "/quote-long-form"
	if( is_page('quote-long-form') ) {

		// Get our form ID
		$possible_params = array( 'f1', 'f2', 'f3', 'f4' );

		$form_id = false;
		$form_ids = array();

		foreach ( $possible_params as $possible_param ) {
			if ( isset( $_GET[ $possible_param ] ) ){
				array_push( $form_ids, intval( $_GET[ $possible_param ] ) );
			}
		}

		// only one space has as value, others are empty so summing array will work for this purpose
		$requested_form_id = array_sum( $form_ids );

		if ( $requested_form_id ) {
			$form_id = $requested_form_id;
		}

		add_filter( 'gform_pre_render',  __NAMESPACE__ . '\add_secure_icon' );
		echo "<div id='quote-long-form'>";

		if ( $form_id ) {

			$form = gravity_form( $form_id, true, true, false, true, true, false, false );
			echo ( $form ? $form : "<h2>Requested form doesn't exist</h2>" );

		} else {
			echo '<h2>Form ID required</h2>';
		}

		echo '</div>';


	}

}


/**
 * Adds secure icon to forms page is over SSL
 *
 * @param $form
 *
 * @return mixed
 */
function add_secure_icon( $form ) {
	if ( $_SERVER['HTTPS'] == "on" ) {
		$form["description"] = '<img class="stellar-secure-form" src="' . BF_LIB_IMAGES . 'ssl-lock-icon-172.png" alt="Form data will be submitted via a secure connection." />' . $form["description"];
	}

	return $form;
}

/**
 * Build output for Row Background Layers
 *
 * @param $row
 *
 * @return string
 */
function build_row_background( $row, $option = 'background-layers' ) {

	switch( $option ) {
		case 'background-layers':
			$behavior = 'background-behavior';
			$animation = 'animation';
			$slideshowspeed = 'slideshowSpeed';
			$animationspeed = 'animationSpeed';
			break;
		case 'container-background-layers':
			$behavior = 'container-background-behavior';
			$animation = 'container-animation';
			$slideshowspeed = 'container-slideshowSpeed';
			$animationspeed = 'container-animationSpeed';
			break;
	}

	$row_background = '';
	

	if ( !empty( $row[ $option ][0][ 'color' ] ) || !empty( $row[ $option ][0][ 'image_id' ] ) || ( isset( $row[ $option ][0][ 'featured_image' ] ) && 'yes' == $row[ $option ][0][ 'featured_image' ] ) ) {
		
		// Check for slider 
		if ( isset( $row[ $option ][1] ) && 'row-slider' == $row[ $behavior ] ) {

			$slider_options = array(
				'animation'      => $row[ $animation ],
				'slideshowSpeed' => $row[ $slideshowspeed ],
				'animationSpeed' => $row[ $animationspeed ],
			);

			$row_background = build_row_flexslider( $row[ $option ], $slider_options );

		} elseif ( 'parallax' == $row[ $behavior ] ) {

			$layers = array_reverse( $row[ $option ] );

			$row_background = build_row_parallax( $layers );

		} else {

			$layers = array_reverse( $row[ $option ] );

			foreach ( $layers as $layer ) {

				$background = background_style_generator( $layer );
				$row_background .= "<div class='background-layer' style='{$background}'></div>";

			}
		}
	}

	return $row_background;
}

/**
 * Build class output for Rows
 *
 * @param $row
 *
 * @return string
 */
function build_row_classes( $row ) {

	$row_classes =  array("row");
	if( $row[ 'custom-classes' ] ){
		foreach ( $row[ 'custom-classes' ] as $class ){
			$row_classes[] = $class;
		}
	}

	if( $row[ 'gutter' ] ){
		$row_classes[] = $row[ 'gutter' ];
	}


	if( $row[ 'desktop-classes' ] ){
		foreach ( $row[ 'desktop-classes' ] as $class ){
			if ( !empty( $class ) )
			$row_classes[] = "d-{$class}";
		}
	}

	if( $row[ 'mobile-classes' ] ){
		foreach ( $row[ 'mobile-classes' ] as $class ){
			if ( !empty( $class ) )
			$row_classes[] = "m-{$class}";
		}
	}

	if( 'row-slider' == $row[ 'background-behavior' ] ){
		$row_classes[] = "row-slider";
	}

	return implode( " ", $row_classes );
}

/**
 * Build class output for Rows
 *
 * @param $row
 *
 * @return string
 */
function build_container_classes( $row ) {

	$row_classes =  array("container");
	if( isset( $row[ 'container-classes' ] )  && is_array( $row[ 'container-classes' ] ) ){
		foreach ( $row[ 'container-classes' ] as $class ){
			$row_classes[] = $class;
		}
	}

	if( isset( $row[ 'container-desktop-classes' ] ) && is_array( $row[ 'container-desktop-classes' ] ) ){
		foreach ( $row[ 'container-desktop-classes' ] as $class ){
			if ( !empty( $class ) )
			$row_classes[] = "d-{$class}";
		}
	}

	if( isset( $row[ 'container-mobile-classes' ] ) && is_array( $row[ 'container-mobile-classes' ] ) ){
		foreach ( $row[ 'container-mobile-classes' ] as $class ){
			if ( !empty( $class ) )
			$row_classes[] = "m-{$class}";
		}
	}

	if( 'auto' != $row[ 'min_height' ] ) {
		$row_classes[] = $row[ 'min_height' ];
	}

	return implode( " ", $row_classes );
}

/**
 * Background styles for the Body
 *
 * @return string
 */
function stellar_body_background_styles() {

	$args[ 'color' ]            = get_theme_mod( 'body-background-color-top' );
	$args[ 'color_bottom' ]     = get_theme_mod( 'body-background-color-bottom' );
	$args[ 'image_id' ]         = get_theme_mod( 'body-background-image' );
	$args[ 'image_attachment' ] = get_theme_mod( 'body-background-attachment' );
	$args[ 'image_size' ]       = get_theme_mod( 'body-background-size' );
	$args[ 'image_repeat' ]     = get_theme_mod( 'body-background-repeat' );
	$args[ 'image_position' ]   = get_theme_mod( 'body-background-position' );

	return background_style_generator( $args );
}