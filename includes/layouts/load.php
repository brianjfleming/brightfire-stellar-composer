<?php

namespace BrightFire\Theme\Stellar;

/** Original Files */
require_once BF_STELLAR_INC . 'layouts/templates.php';
require_once BF_STELLAR_INC . 'layouts/recipes.php';

/** Refactor Files */
require_once( BF_STELLAR_INC . 'layouts/default-layout-array.php' );
require_once( BF_STELLAR_INC . 'layouts/actions.php' );
require_once( BF_STELLAR_INC . 'layouts/filters.php' );
require_once( BF_STELLAR_INC . 'layouts/functions.php' );
require_once( BF_STELLAR_INC . 'layouts/class.layouts.php' );
require_once( BF_STELLAR_INC . 'layouts/class.layouts_admin.php' );

/** Actions and Filters */
add_action( 'after_setup_theme', __NAMESPACE__ . '\stellar_layout_init' );
add_action( 'current_screen', __NAMESPACE__ . '\register_page_layout_actions' );
add_filter( 'theme_page_templates', __NAMESPACE__ . '\filter_page_templates' );
add_filter( 'get_post_metadata', __NAMESPACE__ . '\ensure_template_assignments', 10, 4 );
add_action( 'widgets_init', __NAMESPACE__ . '\register_widget_areas' );
add_action( 'stellar_after_main_content', __NAMESPACE__ . '\stellar_quote_long_form' );
add_action( 'wp', __NAMESPACE__ . '\bf_employee_filters' );

/** Network Removals */
remove_filter('template_include', 'bf_employees_templates'); // Remove Employee Template Load Filter

/** Employees Icons */
remove_filter( 'bf_employee_email_text', '\email_icon' );
add_filter( 'bf_employee_email_text', __NAMESPACE__ . '\email_icon' );

remove_filter( 'bf_employee_linkedin_text', '\linkedin_icon' );
add_filter( 'bf_employee_linkedin_text', __NAMESPACE__ . '\linkedin_icon' );
