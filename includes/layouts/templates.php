<?php

namespace BrightFire\Theme\Stellar;

/**
 * Loads the appropriate template
 */
function load_layout_template() {

	// GLOBALS
	global $stellar_layout, $wp_query;
	$assignments = $stellar_layout->get_layout_option( 'assignments' );
	$page_object = $wp_query->get_queried_object();

	/**
	 * Check if this is an archive first.
	 * Then check if the post has an override template or uses it's required template.
	 */

	if ( is_404() ) {

		$template_id = $assignments[ 'required' ][ 'page' ];

	} elseif ( is_archive() ) {

		$template_id = $assignments[ 'required' ][ 'archive' ];

	} else {

		$type = $page_object->post_type;

		if ( isset( $assignments[ $type ] ) && array_key_exists( $page_object->post_name, $assignments[ $type ] ) ) {

			$template_id = $assignments[ $type ][ $page_object->post_name ];

		} elseif ( is_front_page() ) {

			$template_id = $assignments[ 'required' ][ 'front_page' ];

		} elseif ( is_home() ) {

			$template_id = $assignments[ 'required' ][ 'posts_page' ];

		} elseif ( !empty( $assignments[ 'required' ][ $type ] ) ) {

			$template_id = $assignments[ 'required' ][ $type ];

		} else {

			$template_id = $assignments[ 'required' ][ 'page' ];

		}
	}

	// Get our template
	$template = $stellar_layout->get_layout_item( 'templates', $template_id );
	$sidebar_row = $template[ 'sidebar_row' ];
	$sidebar_col = $template[ 'sidebar_col' ];
	$fixed_row   = $template[ 'fixed_row' ];

	$template_output = array();

	// The template sections
	$sections = row_usage_choices();

	// Row columns
	$cols = array( 'left', 'right' );

	/**
	 * Assign output to sections
	 */
	foreach ( $sections as $section => $value ) {

		// Set up wrappers for our sections
		switch ( $section ) {
			case 'header' :
				$open = '<header id="site-header">';
				$close = '</header>';
				break;
			case 'content' :
				$open = '<div id="site-inner">';
				$close = '</div>';
				break;
			case 'footer' :
				$open = '<footer id="site-footer">';
				$close = '</footer>';
				break;
			default :
				$open = '<div class="' . $section . '">';
				$close = '</div>';
		}

		// Check section for rows
		if ( !empty ( $template[ $section ] ) ) {

			// Build rows for sections
			foreach ( $template[ $section ] as $row_id ) {

				// Our row data
				$row = $stellar_layout->get_layout_item( 'rows', $row_id );

				// Classes
				$row_classes = build_row_classes( $row );
				$row_container_classes = build_container_classes( $row );

				// Row Background
				$row_background = build_row_background( $row );

				// Row Fixed?
				if( $fixed_row == $row_id ) {
					$fixed_row_data = 'data-row-fixed="true"';
				} else {
					$fixed_row_data = '';
				}

				// Open row
				$row_output = '<div id="' . $row_id . '" class="' . $row_classes . '" ' . $fixed_row_data . '>';

				// Row Background
				$row_output .= $row_background;

				// Row Container
				$row_output .= '<div class="' . $row_container_classes . '">';

				// Container Width Background
				$row_output .= $row_background = build_row_background( $row, 'container-background-layers' );

				/**
				 * Widget Areas and Sidebars
				 */
				foreach ( $cols as $col ) {

					$width = $row[ $col ];
					$element = 'div';
					$css_classes = '';

					if ( !empty( $width ) ) {

						$sidebar_id = "{$row_id}-{$col}";

						if ( $row_id == $sidebar_row ) {

							$element = 'main';
							$css_classes = ' content ';

							if ( $sidebar_col && $col == $sidebar_col ) {

								$element = 'aside';
								$css_classes = ' sidebar ';

								// Sidebar override
								if( isset( $page_object ) && isset( $assignments[ 'sidebars' ] ) && array_key_exists( $page_object->post_name, $assignments[ 'sidebars' ] ) ) {
									$sidebar_id = $assignments[ 'sidebars' ][ $page_object->post_name ];
								}
							}

						}


						// Open Column Tag
						$row_output .= '<' . $element . ' id="' . $sidebar_id . '" class="column col-' . $width . $css_classes . '">';


						// Render our widget area
						ob_start();

						// Hook for Before <main>
						if( 'main' == $element ) {
							$row_output .= do_action('stellar_before_main_content');
						}

						if ( is_active_sidebar( $sidebar_id ) ) {
							dynamic_sidebar( $sidebar_id );
						}

						// Hook For After <main>
						if( 'main' == $element ) {
							$row_output .= do_action('stellar_after_main_content');
						}

						$row_output .= ob_get_contents();

						ob_end_clean();

						// Close Column tag
						$row_output .= '</' . $element . '>';

					}
				}

				// Close container / row
				$row_output .= '</div></div>';

				// Redefine our row for output
				$template_output[ $section ][] = $row_output;
			}

			// Print our output
			echo $open;

			foreach ( $template_output[ $section ] as $section_row ) {

				echo $section_row;
			}

			echo $close;
		}
	}
}