<?php

namespace BrightFire\Theme\Stellar;

$theme_section_fields = array(
	'bf_stellar_theme'      => array(
		'title'      => 'Theme',
		'capability' => 'edit_theme_options',
		'fields'     => array(
			'build-css' => array(
				'label' => '',
				'description' => '',
				'settings' => array(),
				'type' => 'button',
				'input_attrs'  => array(
					'value' => 'Preview CSS Changes',
					'class' => 'button button-primary',
					'id' => 'recompile_scss',
					'disabled' => 'disabled',
					'style' => 'width: 99%',
				),
				'permit' => 0,
			),
			'theme' => array(
				'label' => 'Current Theme',
				'description' => '',
				'type' => 'theme_text',
				'permit' => 0,
			),
			'header-font-family' => array(
				'label' => 'Header Font',
				'description' => '',
				'type' => 'select',
				'permit' => 0,
				'choices' => font_choices(),
				'transport' => 'postMessage',
				'default'   => 'helvetica'
			),
			'base-font-family' => array(
				'label' => 'Body Font',
				'description' => '',
				'type' => 'select',
				'permit' => 0,
				'choices' => font_choices(),
				'transport' => 'postMessage',
				'default'   => 'helvetica'
			),
			'accent-font-family' => array(
				'label' => 'Accent Font',
				'description' => '',
				'type' => 'select',
				'permit' => 0,
				'choices' => font_choices(),
				'transport' => 'postMessage',
				'default'   => 'helvetica'
			),
			'primary-color' => array(
				'label' => 'Primary Color',
				'description' => '',
				'type' => 'color_picker',
				'permit' => 0,
				'transport' => 'postMessage',
				'default' => '#1499D3',
			),
			'secondary-color' => array(
				'label' => 'Secondary Color',
				'description' => '',
				'type' => 'color_picker',
				'permit' => 0,
				'transport' => 'postMessage',
				'default' => '#4D6684'
			),
			'tertiary-color' => array(
				'label' => 'Tertiary Color',
				'description' => '',
				'type' => 'color_picker',
				'permit' => 0,
				'transport' => 'postMessage',
				'default' => '#3D3D3D',
			),
			'accent-color' => array(
				'label' => 'Accent Color',
				'description' => '',
				'type' => 'color_picker',
				'permit' => 0,
				'transport' => 'postMessage',
				'default' => '#bf1111',
			),
			'custom1-color' => array(
				'label' => 'Custom Color 1',
				'description' => '',
				'type' => 'color_picker',
				'permit' => 0,
				'transport' => 'postMessage',
				'default' => '#8f1111',
			),
			'custom2-color' => array(
				'label' => 'Custom Color 2',
				'description' => '',
				'type' => 'color_picker',
				'permit' => 0,
				'transport' => 'postMessage',
				'default' => '#4f1111',
			),
			'base-font-size' => array(
				'label' => 'Base Font Size',
				'description' => '',
				'type'  => 'number',
				'permit' => 0,
				'transport' => 'postMessage',
				'default' => '16'
			),
			'font-ratio' => array(
				'label' => 'Font Ratio',
				'description' => '',
				'type'  => 'select',
				'permit' => 0,
				'choices' => array(
					'$minor-second'     => 'Minor Second / 1.067',
					'$major-second'     => 'Major Second / 1.125',
					'$minor-third'      => 'Minor Third / 1.2',
					'$major-third'      => 'Major Third / 1.25',
					'$perfect-fourth'   => 'Perfect Fourth / 1.333',
//					'$augmented-fourth' => 'Augmented Fourth / 1.414',
//					'$perfect-fifth'    => 'Perfect Fith / 1.5',
//					'$minor-sixth'      => 'Minor Sixth / 1.6',
//					'$golden'           => 'Golden / 1.618',
//					'$major-sixth'      => 'Major Sixth / 1.667',
//					'$minor-seventh'    => 'Minor Seventh / 1.778',
//					'$major-seventh'    => 'Major Seventh / 1.875',
//					'$octave'           => 'Octave / 2',
//					'$major-tenth'      => 'Major Tenth / 2.5',
//					'$major-eleventh'   => 'Major Eleventh / 2.667',
//					'$major-twelfth'    => 'Major Twelfth / 3',
//					'$double-octave'    => 'Double Octave / 4',
				),
				'transport' => 'postMessage',
				'default'   => '$major-third'
			),
			'base-border-radius' => array(
				'label' => 'Base Border Radius',
				'description' => '',
				'type'  => 'number',
				'permit' => 0,
				'transport' => 'postMessage',
				'default'   => 3
			),
			'button-border-radius' => array(
				'label' => 'Button Style',
				'description' => '',
				'type' => 'select',
				'choices' => array(
					'50vh'   => 'Pill',
					'$base-border-radius'   => 'Rounded',
					'0px'   => 'Square',
				),
				'permit' => 0,
				'transport' => 'postMessage',
				'default' => '$base-border-radius',
			),
			'form-border-radius' => array(
				'label' => 'Form Style',
				'description' => '',
				'type' => 'select',
				'choices' => array(
					'$base-border-radius'   => 'Rounded',
					'0px'   => 'Square',
				),
				'permit' => 0,
				'transport' => 'postMessage',
				'default' => '$base-border-radius',
			),
			'max-width' => array(
				'label' => 'Max Width',
				'description' => '',
				'type'  => 'number',
				'permit' => 0,
				'transport' => 'postMessage',
				'default'   => 1200
			),
			'gutter' => array(
				'label' => 'Gutter Width',
				'description' => '',
				'type'  => 'select',
				'permit' => 0,
				'choices' => array(
					'1'     => 'Base',
					'1.5'   => 'Base x 1.5',
					'2'     => 'Base x 2',
					'2.5'   => 'Base x 2.5',
					'3'     => 'Base x 3',
				),
				'default'   => 1,
				'transport' => 'postMessage',
			),
			'breakpoint' => array(
				'label' => 'Mobile Breakpoint',
				'description' => '',
				'type'  => 'number',
				'permit' => 0,
				'transport' => 'postMessage',
				'default'   => 768
			),
		),
	)
);

$body_section_fields = array(
	'bf_stellar_body'      => array(
		'title'      => 'Theme',
		'capability' => 'edit_theme_options',
		'fields'     => array(
			'body-background-color-top' => array(
				'label' => 'Background color',
				'description' => '',
				'type' => 'color_picker',
				'permit' => 0,
				'transport' => 'postMessage',
				'input_attrs' => array(
					'data-alpha' => 'true'
				)
			),
			'body-background-color-bottom' => array(
				'label' => 'Background color bottom',
				'description' => '',
				'type' => 'color_picker',
				'permit' => 0,
				'transport' => 'postMessage',
				'input_attrs' => array(
					'data-alpha' => 'true'
				)
			),
			'body-background-image' => array(
				'label' => 'Background image',
				'description' => '',
				'type' => 'media',
				'permit' => 0,
				'transport' => 'postMessage',
			),
			'body-background-attachment' => array(
				'label' => 'Background image attachment',
				'default' => 'no',
				'description' => '',
				'type' => 'select',
				'permit' => 0,
				'choices' => background_attachment_choices(),
				'transport' => 'postMessage',
			),
			'body-background-size' => array(
				'label' => 'Background image size',
				'description' => '',
				'type' => 'select',
				'permit' => 0,
				'choices' => background_size_choices(),
				'transport' => 'postMessage',
			),
			'body-background-repeat' => array(
				'label' => 'Background image repeat',
				'description' => '',
				'type' => 'select',
				'permit' => 0,
				'choices' => background_repeat_choices(),
				'transport' => 'postMessage',
			),
			'body-background-position' => array(
				'label' => 'Background image position',
				'description' => '',
				'type' => 'select',
				'permit' => 0,
				'choices' => background_position_choices(),
				'transport' => 'postMessage',
			)
		),
	)
);

new \BrightFire_Theme_Stellar_Customizer( 'bf_stellar_theme', 'Theme Settings', $theme_section_fields );
new \BrightFire_Theme_Stellar_Customizer( 'bf_stellar_body', 'Body Settings', $body_section_fields );

/**
 * Proper Segregation of customizer controls JS vs. customizer preview JS
 */
function stellar_customize_controls_enqueue_scripts() {
	wp_enqueue_script( 'stellar-customizer', BF_STELLAR_URL . '/assets/js/obj.customizer.js', array( 'jquery', 'customize-controls', 'wp-color-picker' ), BF_STELLAR_VERSION, true );
}

function stellar_customize_preview_init() {

	$admin_url = admin_url( 'admin-ajax.php' );
	wp_enqueue_script( 'stellar-customizer-preview', BF_STELLAR_URL . '/assets/js/obj.customizer-preview.js', array( 'customize-preview-widgets' ), BF_STELLAR_VERSION );

	$row_ids = array();

	global $stellar_layout;
	$rows = $stellar_layout->get_layout_option( 'rows' );

	foreach( $rows as $row_id => $row ) {
		$row_ids[] = $row_id;
	}

	$row_ids = json_encode( $row_ids );
	wp_localize_script( 'stellar-customizer-preview', 'row_ids', $row_ids  );
	wp_localize_script( 'stellar-customizer-preview', 'ajaxurl', $admin_url  );
}