<?php

namespace BrightFire\Theme\Stellar;

function row_customizer_options() {

	global $stellar_layout;

	$rows = $stellar_layout->get_layout_option( 'rows' );

	if ( ! $rows ) {
		return;
	}
	
	foreach ( $rows as $id => $data ) {

		$options = array();

		$key_prefix = "stellar_layout[rows][$id]";
		$row_title = $data['name'] . ' Row';

		$options [ $key_prefix . '[custom-classes]' ] = array(
			'label' => 'Global Custom Classes',
			'description' => '',
			'type' => 'selectize',
			'option_type' => 'option',
			'input_attrs' => array (
				'data-customize-setting-link' => $key_prefix . '[custom-classes]',
				'multiple' => 'multiple',
				),
			'permit' => 0,
			'choices' => row_class_choices(),
			'transport' => 'postMessage',
		);

		$options [ $key_prefix . '[desktop-classes]' ] = array(
			'label' => 'Desktop Classes',
			'description' => '',
			'type' => 'selectize',
			'option_type' => 'option',
			'input_attrs' => array (
				'data-customize-setting-link' => $key_prefix . '[desktop-classes]',
				'multiple' => 'multiple',
				),
			'permit' => 0,
			'choices' => row_breakpoint_choices(),
			'transport' => 'postMessage',
		);

		$options [ $key_prefix . '[mobile-classes]' ] = array(
			'label' => 'Mobile Classes',
			'description' => '',
			'type' => 'selectize',
			'option_type' => 'option',
			'input_attrs' => array (
				'data-customize-setting-link' => $key_prefix . '[mobile-classes]',
				'multiple' => 'multiple',
				),
			'permit' => 0,
			'choices' => row_breakpoint_choices(),
			'transport' => 'postMessage',
		);

		$options [ $key_prefix . '[container-classes]' ] = array(
			'label' => 'Container Classes',
			'description' => '',
			'type' => 'selectize',
			'option_type' => 'option',
			'input_attrs' => array (
				'data-customize-setting-link' => $key_prefix . '[container-classes]',
				'multiple' => 'multiple',
			),
			'permit' => 0,
			'choices' => row_container_class_choices(),
			'transport' => 'postMessage',
		);

		$options [ $key_prefix . '[container-desktop-classes]' ] = array(
			'label' => 'Container Desktop Only Classes',
			'description' => '',
			'type' => 'selectize',
			'option_type' => 'option',
			'input_attrs' => array (
				'data-customize-setting-link' => $key_prefix . '[container-desktop-classes]',
				'multiple' => 'multiple',
			),
			'permit' => 0,
			'choices' => row_container_breakpoint_choices(),
			'transport' => 'postMessage',
		);

		$options [ $key_prefix . '[container-mobile-classes]' ] = array(
			'label' => 'Container Mobile Only Classes',
			'description' => '',
			'type' => 'selectize',
			'option_type' => 'option',
			'input_attrs' => array (
				'data-customize-setting-link' => $key_prefix . '[container-mobile-classes]',
				'multiple' => 'multiple',
			),
			'permit' => 0,
			'choices' => row_container_breakpoint_choices(),
			'transport' => 'postMessage',
		);

		$options [ $key_prefix . '[min_height]' ] = array(
			'label' => 'Height Options',
			'description' => '',
			'type' => 'select',
			'option_type' => 'option',
			'input_attrs' => array(
				'data-customize-setting-link' => $key_prefix . '[min_height]',
			),
			'permit' => 0,
			'choices' => row_height_class_choices(),
			'transport' => 'postMessage',
		);

		$options [ $key_prefix . '[gutter]' ] = array(
			'label' => 'Additional Gutter Padding',
			'description' => '',
			'type' => 'select',
			'option_type' => 'option',
			'input_attrs' => array(
				'data-customize-setting-link' => $key_prefix . '[gutter]',
			),
			'permit' => 0,
			'choices' => column_gutter_padding_choices(),
			'transport' => 'postMessage',
		);

		$section_id = 'bf_stellar_row_' . $id;

		$sections_fields = array(
			$section_id => array(
				'capability' => 'edit_theme_options',
				'fields'     => $options,
			)
		);

		new \BrightFire_Theme_Stellar_Customizer( $section_id, $row_title, $sections_fields );
	}
}

add_action( 'after_setup_theme', __NAMESPACE__ . '\row_customizer_options' );

function row_customizer_previews() {
	
	//Placeholder for our preview style tag
	$style = '<style id="stellar-preview">';
	$style .= '</style>';
	
	echo $style;
}

if ( is_customize_preview() ) {
	add_action('wp_footer', __NAMESPACE__ . '\row_customizer_previews', 99 );
}