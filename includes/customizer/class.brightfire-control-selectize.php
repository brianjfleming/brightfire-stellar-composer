<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 7/7/15
 * Time: 4:06 PM
 */

if  ( ! class_exists( 'WP_Customize_Control' ))
	return;


class BrightFire_Control_Selectize extends WP_Customize_Control {

	/**
	 * The type of customize control being rendered.
	 *
	 * @since  1.0.0
	 * @access public
	 * @var    string
	 */
	public $type = 'selectize';

	/**
	 * Displays the control content.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function render_content() {

		if ( empty( $this->choices ) )
			return;

		$args = array(
			'fields'     => array(
				esc_attr( $this->id ) => array(
					'type'    => 'selectize',
					'label'   => $this->label,
					'choices' => $this->choices,
					'atts'    => $this->input_attrs,
					'permit'  => 1,
				),
			),
			'instance'   => array( $this->id => $this->value() ),
			'display'    => 'basic',
			'echo'       => true,
		);

		BF_Admin_API_Fields::bf_admin_api_fields_build( $args );
	}
}