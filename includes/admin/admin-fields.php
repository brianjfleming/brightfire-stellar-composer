<?php

namespace BrightFire\Theme\Stellar;

/**
 * Form table helper
 *
 * @param array $table
 *
 * @return string
 */
function form_table( $table = array() ) {

	if ( empty( $table ) ) {

		// Sample parameter array
		$table = array(

			// Table ID attribute
			'id'      => '',

			// Space delimited classes for the table
			'classes' => '',

			// Table rows, this must always be an array of arrays
			'tr'      => array(

				// Table row
				array(
					// Row ID attribute
					'id'      => '',
					// Space delimited classes for the table row
					'classes' => '',
					// A table header cell, must be a text string
					'th'      => 'Name of setting',
					// A table data cell
					'td'      => 'HTML for settings.',
				),

				// Table row
				array(
					'th' => 'Another Row Title',
					'td' => 'HTML for this row',
				),
			),
		);
	}

	// Table variables
	$id      = isset( $table[ 'id' ] ) ? $table[ 'id' ] : '';
	$classes = isset( $table[ 'classes' ] ) ? $table[ 'classes' ] : '';
	$rows    = ( isset( $table[ 'tr' ] ) ) ? $table[ 'tr' ] : array();

	// Open the table
	$output = '<table id="' . $id . '" class="form-table ' . $classes . '"><tbody>';

	foreach ( $rows as $row ) {

		// Row variables
		$id      = isset( $row[ 'id' ] ) ? $row[ 'id' ] : '';
		$classes = isset( $row[ 'classes' ] ) ? $row[ 'classes' ] : '';
		$header  = isset( $row[ 'th' ] ) ? __( $row[ 'th' ], 'bf_stellar' ) : '';
		$data    = isset( $row[ 'td' ] ) ? $row[ 'td' ] : '';

		// Open table row
		$output .= '<tr id="' . $id . '" class="' . $classes . '">';

		// Row header cell
		$output .= '<th scope="row">' . $header . '</th>';

		// Row data cell
		$output .= '<td>' . $data . '</td>';

		// Close the row
		$output .= '</tr>';
	}

	// Close the table
	$output .= '</tbody></table>';

	return $output;
}

function select_field( $field_id = '', $choices = array(), $selected = '' ) {


	$id   = ( $field_id ) ? ' id="' . $field_id . '"' : '';
	$name = ( $field_id ) ? ' name="' . $field_id . '"' : '';

	$selector = '<select' . $id . $name . '>';

	foreach ( $choices as $value => $name ) {
		$selector .= '<option value="' . $value . '" ' . selected( $selected, $value, false ) . '>' . $name . '</option>';
	}

	$selector .= '</select>';

	return $selector;
}

function image_selector( $id = '', $value = '' ) {

	if ( $value ) {
		$img   = wp_get_attachment_image_src( $value );
		$style = ' style="background-image : url(' . $img[ 0 ] . ');"';
		$class = ' remove-image';
	} else {
		$style = '';
		$class = ' set-image';
	}

	$image_selector = '<div><div class="image-container' . $class . '"' . $style . '><div class="remove-message">Remove Image</div></div>';
	$image_selector .= '<input id="' . $id . '" name="' . $id . '" type="hidden" value="' . $value . '" /></div>';

	return $image_selector;
}

function color_selector( $id = '', $value = '' ) {

	return '<input id="' . $id . '" name="' . $id . '" class="color-picker" type="text" value="' . $value . '" />';
}

function column_range_slider( $left = '', $right = '' ) {

	$output = '
		<div class="column-ranger">
			<div class="left">Left<span class="number">' . $left . '</span></div>
			<input type="range" value="' . $left . '" list = "column-ranger-tick" min="0" max="12">
			<div class="right">Right<span class="number">' . $right . '</span></div>
		</div>';

	return $output;
}

/**
 * Populates a select field with row choices
 *
 * @uses select_field()
 *
 * @param string $selected
 *
 * @return string
 */
function select_rows_choices( $selected = '' ) {

	global $stellar_layout;

	$rows = $stellar_layout->get_layout_option( 'rows' );

	$options = array(
		'' => 'None'
	);

	foreach ( $rows as $row_id => $row_data ) {
		$options[ $row_id ] = $row_data[ 'name' ];
	}

	return select_field( '', $options, $selected );
}

function select_sidebar_choices( $field_id = '', $selected = '' ) {

	global $stellar_layout;

	$sidebars = $stellar_layout->get_layout_option( 'sidebars' );
	$options  = array();

	// How to do pretty for bulk edit??????????????
//		$options[ '' ] = '— No Change —';

	$options[ 'default' ] = 'Default';

	foreach ( $sidebars as $sidebar_id => $sidebar_data ) {

		$options[ $sidebar_id ] = $sidebar_data[ 'name' ];
	}

	return select_field( $field_id, $options, $selected );
}

function select_rows( $usage = '', $row_ids ) {

	$selectors = '';

	if ( $row_ids ) {

		foreach ( $row_ids as $row_id ) {

			$selectors .= row_repeater( $usage, $row_id );
		}

	} else {

		$selectors .= row_repeater( $usage );
	}

	return $selectors;
}

function widget_area_repeater( $option, $option_id ) {

	global $stellar_layout;
	$output = $new_row = $editing = $viewing = '';
	$hidden = "style='display:none'";

	if ( empty( $option ) ) {
		$option = array(
			$option_id . '-' . time() => array(
				'name'        => '',
				'description' => ''
			)
		);

		$new_row = true;
	}

	foreach ( $option as $id => $data ) {

		if ( $new_row ) {
			$viewing = $hidden;
		} else {
			$editing = $hidden;
		}

		if ( $stellar_layout->is_required( $option_id, $id ) ) {
			$delete = '';
		} else {
			$delete = '<span class="view-widget-area" ' . $viewing . '><button class="button delete-widget-area"><i class="fa fa-minus"></i> Delete</button></span> ';
		}

		$output .= '<tr id="' . $id . '"><td>';
		$output .= '<p ' . $viewing . ' class="view-widget-area name">' . stripslashes( $data[ 'name' ] ) . '</p>';
		$output .= '<input ' . $editing . ' class="editing-widget-area name large-text" type="text" value="' . stripslashes( $data[ 'name' ] ) . '" /></td><td>';

		$output .= '<p ' . $viewing . '  class="view-widget-area description">' . stripslashes( $data[ 'description' ] ) . '</p>';
		$output .= '<input ' . $editing . ' class="editing-widget-area description large-text" type="text" value="' . stripslashes( $data[ 'description' ] ) . '" /></td><td>';

		$output .= $delete . '<span ' . $viewing . ' class="view-widget-area"><button class="button edit-widget-area"><i class="fa fa-pencil"></i> Edit</button></span>';
		$output .= '<span ' . $editing . ' class="editing-widget-area"><button class="button cancel-widget-area"><i class="fa fa-remove"></i> Cancel</button></span> <span ' . $editing . ' class="editing-widget-area"><button class="button save-widget-area"><i class="fa fa-check"></i> 
		Save</button></span><span class="spinner"></span>';
		$output .= '</td></tr>';
	}

	return $output;
}

function assignment_checkboxes( $template_id = null ) {

	global $stellar_layout;

	$checkboxes = '';

	// layout assignment types
	$available_assignments = required_assignment_types();

	$assigned = $stellar_layout->get_layout_option( 'assignments' )[ 'required' ];

	foreach ( $available_assignments as $assignment => $label ) {

		$checked = $disabled = false;

		foreach ( $assigned as $required => $template ) {
			if ( $required == $assignment && $template_id == $template ) {
				$checked  = true;
				$disabled = 'disabled';
			}
		}

		$checkboxes .= '<label for="' . $assignment . '-assignment"><input name="' . $assignment . '-assignment" id="' . $assignment . '-assignment" class="assignment" value="' . $assignment . '" type="checkbox" ' . checked( $checked, true, false ) . ' ' . $disabled . '/> ' . $label . '</label><br>';
	}

	return $checkboxes;
}

function template_assignments_list( $id ) {

	global $stellar_layout;

	if ( ! isset( $id ) ) {

		return false;
	}

	$output              = '<ul>';
	$assignments         = $stellar_layout->get_layout_option( 'assignments' );
	$stellar_assignments = required_assignment_types();

	foreach ( $assignments as $assignment_group => $templates ) {

		if ( ! empty( $templates ) && 'sidebars' != $assignment_group ) {

			switch ( $assignment_group ) {
				case 'required' :
					$title = 'Required Defaults';
					break;
				case 'page' :
					$title = 'Pages';
					break;
				default :
					$title = ucwords( $assignment_group );
			}

			$output .= '<li><strong>' . $title . '</strong>';
			$output .= '<ul>';

			foreach ( $templates as $assignment => $template_id ) {
				if ( $template_id == $id ) {
					$label = ( isset( $stellar_assignments[ $assignment ] ) ) ? $stellar_assignments[ $assignment ] : $assignment;
					$output .= '<li>' . $label . '</li>';
				}
			}

			$output .= '</ul></li>';
		}
	}

	return $output;
}

function admin_message( $type = '', $message = '' ) {

	if ( 'success_deleted' == $type ) {
		$icon = '<span class="fa fa-minus-circle"></span>';
		$id   = 'success-message';
	} elseif ( 'success' == $type ) {
		$icon = '<span class="fa fa-check-circle"></span>';
		$id   = 'success-message';
	} elseif ( 'error' == $type ) {
		$icon = '<span class="fa fa-exclamation-triangle "></span>';
		$id   = 'error-message';
	}

	return '<div id="' . $id . '" class="bf-notice"><p>' . $icon . ' ' . $message . '</p></div>';
}

function message_after_redirect() {

	if ( isset( $_GET[ 'deleted' ] ) ) {

		$message = sprintf( __( '%s deleted.', 'bf_stellar' ), '<strong>' . $_GET[ 'deleted' ] . '</strong>' );

		echo admin_message( 'success_deleted', $message );

	} elseif ( isset( $_GET[ 'loaded' ] ) ) {

		$message = sprintf( __( '%s loaded.', 'bf_stellar' ), '<strong>' . $_GET[ 'loaded' ] . '</strong>' );

		echo admin_message( 'success', $message );

	} else {

		return false;
	}
}

function row_thumbnail( $id = '', $type = 'row' ) {

	if ( empty( $id ) && $type == 'template' ) {
		return "<h3>None</h3>";
	}

	global $stellar_layout;

	$template = $sidebar_row = $sidebar_col = $title = $thumb = '';
	$sections = array( 1 );
	$cols     = array( 'left', 'right' );

	// Data for templates
	if ( 'template' == $type ) {

		$template    = $stellar_layout->get_layout_item( 'templates', $id );
		$sections    = row_usage_choices();
		$sidebar_row = $template[ 'sidebar_row' ];
		$sidebar_col = $template[ 'sidebar_col' ];
	}

	foreach ( $sections as $section => $label ) {

		$loop_rows = array();

		if ( isset( $template[ $section ] ) ) {

			$loop_rows = $template[ $section ];

		} elseif ( 'row' == $type && ! empty( $id ) ) {

			$loop_rows = array( $id );

		}

		if ( $loop_rows ) {
			foreach ( $loop_rows as $row_id ) {

				$row = $stellar_layout->get_layout_item( 'rows', $row_id );

				if ( isset( $_GET[ 'edit' ] ) ) {
					$template    = $stellar_layout->get_layout_item( 'templates', $_GET[ 'edit' ] );
					$sidebar_row = $template[ 'sidebar_row' ];
					$sidebar_col = $template[ 'sidebar_col' ];
				}

				$thumb .= '<div class="layout-thumb repeater-title-background row-' . $row_id . '" style="' . stellar_body_background_styles() . '">';
				$thumb .= '<div class="' . build_row_classes( $row ) . '">';
				$thumb .= build_row_background( $row );
				$thumb .= '<div class="' . build_container_classes( $row ) . '">';
				$thumb .= build_row_background( $row, 'container-background-layers' );
				foreach ( $cols as $col ) {

					$sidebar = ( $sidebar_row == $row_id && $sidebar_col == $col ) ? 'sidebar-col' : '';

					$classes = "col-{$row[ $col ]} {$col} {$sidebar}";

					$thumb .= "<div class='{$classes}'><div></div></div>";
				}
				$thumb .= '</div>';
				$thumb .= '</div>';
				$thumb .= '<div class="row-hover"></div><a class="row-edit-link" href="' . admin_url( 'admin.php?page=stellar-row-edit&edit=' . $row_id ) . '">' . $row[ 'name' ] . '</a>';
				$thumb .= '</div>';

			}
		} elseif ( empty ( $loop_rows ) && ( 'row' == $type ) ) {
			$thumb .= '<h3>None</h3>';
		}
	}

	return $thumb;
}

function row_background_title( $values = '' ) {

	return '<div class="background-layer" style="' . stellar_body_background_styles() . '"><div class="background-layer" style="' . background_style_generator( $values ) . '"></div></div>';
}

function row_background_fields( $id, $values = null ) {

	// We Always want our keys
	$values = recurse_parse_args( $values, array(
			'color'             => '',
			'color_bottom'      => '',
			'color_stop'        => '0',
			'color_bottom_stop' => '100',
			'gradient_angle'    => '180',
			'featured_image'    => 'no',
			'image_id'          => '',
			'image_attachment'  => '',
			'image_size'        => '',
			'image_repeat'      => '',
			'image_position'    => '',
		)
	);

	if ( empty( $values ) ) {

		$id = $id . '[layer' . time() . ']';
	}

	// Reformat our values for BF_Admin_API_Fields
	$instance = array(
		$id . '[color]'             => $values[ 'color' ],
		$id . '[color_bottom]'      => $values[ 'color_bottom' ],
		$id . '[color_stop]'        => $values[ 'color_stop' ],
		$id . '[color_bottom_stop]' => $values[ 'color_bottom_stop' ],
		$id . '[gradient_angle]'    => $values[ 'gradient_angle' ],
		$id . '[featured_image]'    => $values[ 'featured_image' ],
		$id . '[image_id]'          => $values[ 'image_id' ],
		$id . '[image_attachment]'  => $values[ 'image_attachment' ],
		$id . '[image_size]'        => $values[ 'image_size' ],
		$id . '[image_repeat]'      => $values[ 'image_repeat' ],
		$id . '[image_position]'    => $values[ 'image_position' ],
	);

	$field_args = array(
		'fields'   => array(
			$id . '[color]'             => array(
				'label'  => 'Background color 1',
				'type'   => 'wp_color',
				'atts'   => array(
					'data-key'   => 'color',
					'data-alpha' => 'true'
				),
				'permit' => 1,
			),
			$id . '[color_bottom]'      => array(
				'label'  => 'Background color 2',
				'type'   => 'wp_color',
				'atts'   => array(
					'data-key'   => 'color_bottom',
					'data-alpha' => 'true'
				),
				'permit' => 1,
			),
			$id . '[color_stop]'        => array(
				'label'         => 'Color 1 gradient stop',
				'type'          => 'range',
				'default_value' => 0,
				'data-key'      => 'color',
				'atts'          => array(
					'min'      => 0,
					'max'      => 100,
					'step'     => 5,
					'data-key' => 'color_stop',
				),
				'settings'      => array(
					'min-label'    => 0,
					'max-label'    => 100,
					'units'        => 'Percent',
					'show-current' => true,
				),
				'permit'        => 1,
			),
			$id . '[color_bottom_stop]' => array(
				'label'         => 'Color 2 gradient stop',
				'type'          => 'range',
				'default_value' => 100,
				'data-key'      => 'color',
				'atts'          => array(
					'min'      => 0,
					'max'      => 100,
					'step'     => 5,
					'data-key' => 'color_bottom_stop',
				),
				'settings'      => array(
					'min-label'    => 0,
					'max-label'    => 100,
					'units'        => 'Percent',
					'show-current' => true,
				),
				'permit'        => 1,
			),
			$id . '[gradient_angle]'    => array(
				'label'         => 'Gradient angle',
				'type'          => 'range',
				'default_value' => 0,
				'data-key'      => 'color',
				'atts'          => array(
					'min'      => 0,
					'max'      => 360,
					'step'     => 5,
					'data-key' => 'gradient_angle',
				),
				'settings'      => array(
					'min-label'    => 0,
					'max-label'    => 360,
					'units'        => 'Degrees',
					'show-current' => true,
				),
				'permit'        => 1,
			),
			$id . '[featured_image]'    => array(
				'label'   => 'Use Featured Image if available?',
				'type'    => 'selectize',
				'choices' => yes_no_choices(),
				'default' => 'no',
				'atts'    => array(
					'data-key' => 'featured_image',
				),
				'permit'  => 1,
			),
			$id . '[image_id]'          => array(
				'label'  => 'Background image',
				'type'   => 'wp_image',
				'atts'   => array(
					'data-key' => 'image_id',
				),
				'permit' => 1,
			),
			$id . '[image_attachment]'  => array(
				'label'   => 'Background image attachment',
				'type'    => 'selectize',
				'choices' => background_attachment_choices(),
				'atts'    => array(
					'data-key' => 'image_attachment',
				),
				'permit'  => 1,
			),
			$id . '[image_size]'        => array(
				'label'   => 'Background image size',
				'type'    => 'selectize',
				'choices' => background_size_choices(),
				'atts'    => array(
					'data-key' => 'image_size',
				),
				'permit'  => 1,
			),
			$id . '[image_repeat]'      => array(
				'label'   => 'Background image repeat',
				'type'    => 'selectize',
				'choices' => background_repeat_choices(),
				'atts'    => array(
					'data-key' => 'image_repeat',
				),
				'permit'  => 1,
			),
			$id . '[image_position]'    => array(
				'label'   => 'Background image position',
				'type'    => 'selectize',
				'choices' => background_position_choices(),
				'atts'    => array(
					'data-key' => 'image_position',
				),
				'permit'  => 1,
			)
		),
		'instance' => $instance,
		'display'  => 'basic',
		'echo'     => false
	);

	return \BF_Admin_API_Fields::bf_admin_api_fields_build( $field_args );

}

function slidebar_background_title( $values = '' ) {

	global $stellar_layout;

	$slides = $stellar_layout->get_layout_option( 'slides' );

	$slide_choices = array( 'none' => 'No Slidebar&trade; chosen.' );

	if ( $slides ) {
		foreach ( $slides as $slidebar => $data ) {
			$slide_choices[ $slidebar ] = $data[ 'name' ];
		}
	}

	$title = ( isset( $values[ 'slidebar' ] ) ) ? $slide_choices[ $values[ 'slidebar' ] ] : 'No Slidebar&trade; chosen.';

	return '<div class="background-layer repeater-title-background" style="' . background_style_generator( $values ) . '"><h3>' . $title . '</h3></div>';
}

function slidebar_background_fields( $id, $values = null ) {

	global $stellar_layout;

	$slides = $stellar_layout->get_layout_option( 'slides' );

	$slide_choices = array( 'none' => 'No Slidebar&trade; chosen.' );

	if ( $slides ) {
		foreach ( $slides as $slidebar => $data ) {
			$slide_choices[ $slidebar ] = $data[ 'name' ];
		}
	}

	$defaults = array(
		'slidebar'          => '',
		'color'             => '',
		'color_bottom'      => '',
		'color_stop'        => '0',
		'color_bottom_stop' => '100',
		'gradient_angle'    => '180',
		'image_id'          => '',
		'image_attachment'  => '',
		'image_size'        => '',
		'image_repeat'      => '',
		'image_position'    => '',
		'slide_padding'     => 'no',
		'widget_position'   => '',
	);

	// We Always want our keys
	if ( empty( $values ) ) {
		$id = $id . '[slide' . time() . ']';
	}

	$values = wp_parse_args( $values, $defaults );

	// Reformat our values for BF_Admin_API_Fields
	$instance = array(
		$id . '[slidebar]'          => $values[ 'slidebar' ],
		$id . '[color]'             => $values[ 'color' ],
		$id . '[color_bottom]'      => $values[ 'color_bottom' ],
		$id . '[color_stop]'        => $values[ 'color_stop' ],
		$id . '[color_bottom_stop]' => $values[ 'color_bottom_stop' ],
		$id . '[gradient_angle]'    => $values[ 'gradient_angle' ],
		$id . '[image_id]'          => $values[ 'image_id' ],
		$id . '[image_attachment]'  => $values[ 'image_attachment' ],
		$id . '[image_size]'        => $values[ 'image_size' ],
		$id . '[image_repeat]'      => $values[ 'image_repeat' ],
		$id . '[image_position]'    => $values[ 'image_position' ],
		$id . '[slide_padding]'     => $values[ 'slide_padding' ],
		$id . '[widget_position]'   => $values[ 'widget_position' ],
	);

	$field_args = array(
		'fields'   => array(
			$id . '[slidebar]'          => array(
				'label'   => 'Slidebar&trade;',
				'type'    => 'selectize',
				'choices' => $slide_choices,
				'atts'    => array(
					'data-key'              => 'slidebar',
					'data-current-slidebar' => $values[ 'slidebar' ],
				),
				'permit'  => 1,
			),
			$id . '[color]'             => array(
				'label'  => 'Background color',
				'type'   => 'wp_color',
				'atts'   => array(
					'data-key'   => 'color',
					'data-alpha' => 'true'
				),
				'permit' => 1,
			),
			$id . '[color_bottom]'      => array(
				'label'  => 'Background color bottom',
				'type'   => 'wp_color',
				'atts'   => array(
					'data-key'   => 'color_bottom',
					'data-alpha' => 'true'
				),
				'permit' => 1,
			),
			$id . '[color_stop]'        => array(
				'label'         => 'Color 1 gradient stop',
				'type'          => 'range',
				'default_value' => 0,
				'data-key'      => 'color',
				'atts'          => array(
					'min'      => 0,
					'max'      => 100,
					'step'     => 5,
					'data-key' => 'color_stop',
				),
				'settings'      => array(
					'min-label'    => 0,
					'max-label'    => 100,
					'units'        => 'Percent',
					'show-current' => true,
				),
				'permit'        => 1,
			),
			$id . '[color_bottom_stop]' => array(
				'label'         => 'Color 2 gradient stop',
				'type'          => 'range',
				'default_value' => 100,
				'data-key'      => 'color',
				'atts'          => array(
					'min'      => 0,
					'max'      => 100,
					'step'     => 5,
					'data-key' => 'color_bottom_stop',
				),
				'settings'      => array(
					'min-label'    => 0,
					'max-label'    => 100,
					'units'        => 'Percent',
					'show-current' => true,
				),
				'permit'        => 1,
			),
			$id . '[gradient_angle]'    => array(
				'label'         => 'Gradient angle',
				'type'          => 'range',
				'default_value' => 0,
				'data-key'      => 'color',
				'atts'          => array(
					'min'      => 0,
					'max'      => 360,
					'step'     => 5,
					'data-key' => 'gradient_angle',
				),
				'settings'      => array(
					'min-label'    => 0,
					'max-label'    => 360,
					'units'        => 'Degrees',
					'show-current' => true,
				),
				'permit'        => 1,
			),
			$id . '[image_id]'          => array(
				'label'  => 'Background image',
				'type'   => 'wp_image',
				'atts'   => array(
					'data-key' => 'image_id',
				),
				'permit' => 1,
			),
			$id . '[image_attachment]'  => array(
				'label'   => 'Background image attachment',
				'type'    => 'selectize',
				'choices' => background_attachment_choices(),
				'atts'    => array(
					'data-key' => 'image_attachment',
				),
				'permit'  => 1,
			),
			$id . '[image_size]'        => array(
				'label'         => 'Background image size',
				'type'          => 'selectize',
				'choices'       => background_size_choices(),
				'atts'          => array(
					'data-key' => 'image_size',
				),
				'permit'        => 1,
				'default_value' => 'cover'
			),
			$id . '[image_repeat]'      => array(
				'label'   => 'Background image repeat',
				'type'    => 'selectize',
				'choices' => background_repeat_choices(),
				'atts'    => array(
					'data-key' => 'image_repeat',
				),
				'permit'  => 1,
			),
			$id . '[image_position]'    => array(
				'label'   => 'Background image position',
				'type'    => 'selectize',
				'choices' => background_position_choices(),
				'atts'    => array(
					'data-key' => 'image_position',
				),
				'permit'  => 1,
			),
			$id . '[slide_padding]'     => array(
				'label'   => 'Add Grid Padding to Slide (left + right)',
				'type'    => 'selectize',
				'choices' => [
					'no'  => 'No',
					'yes' => 'Yes',
				],
				'atts'    => array(
					'data-key' => 'slide_padding',
				),
				'permit'  => 1,
			),
			$id . '[widget_position]'   => array(
				'label'   => 'Widget position',
				'type'    => 'selectize',
				'choices' => [
					'bottom'  => 'Bottom',
					'middle'  => 'Middle',
					'top'     => 'Top',
					'stretch' => 'Full Height'
				],
				'atts'    => array(
					'data-key' => 'widget_position',
				),
				'permit'  => 1,
			),
		),
		'instance' => $instance,
		'display'  => 'basic',
		'echo'     => false
	);

	return \BF_Admin_API_Fields::bf_admin_api_fields_build( $field_args );

}

/**
 * Mixes Repeater Title Callback
 * @return string
 */
function mixes_title_cb() {
	return '<h3>NEW</h3>';
}

/**
 * Mixes Repeater Field Callback
 *
 * @param null $values
 *
 * @return string
 */
function mixes_fields_cb( $id = '', $values = null ) {

	$name = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields'   => array(
				$id . '-name' => array(
					'label' => 'Mix Name',
					'type'  => 'text',
					'atts'  => array(
						'data-key' => 'mix-name',
						'data-required' => $values[ 'required' ]
					)
				),
				$id . '-id'   => array(
					'type'          => 'hidden',
					'default_value' => 'mix-' . uniqid(),
					'atts'          => array(
						'data-key' => 'mix-id',
					)
				)
			),
			'instance' => array(
				$id . '-name' => stripslashes( $values[ 'name' ] ),
				$id . '-id'   => $id,
			),
			'display'  => 'basic',
			'echo'     => false,
		)
	);

	$global_classes = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields'   => array(
				$id . '-custom_classes' => array(
					'label'   => '<i class="fa fa-globe"></i> Global',
					'type'    => 'selectize',
					'choices' => widget_class_choices(),
					'atts'    => array(
						'multiple' => 'multiple',
						'data-key' => 'global'
					)
				)
			),
			'instance' => array(
				$id . '-custom_classes' => $values[ 'custom_classes' ]
			),
			'display'  => 'basic',
			'echo'     => false,
		)
	);

	$desktop_classes = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields'   => array(
				$id . '-desktop_classes' => array(
					'label'   => '<i class="fa fa-laptop"></i> Desktop',
					'type'    => 'selectize',
					'choices' => widget_breakpoint_choices(),
					'atts'    => array(
						'multiple' => 'multiple',
						'data-key' => 'desktop'
					)
				)
			),
			'instance' => array(
				$id . '-desktop_classes' => $values[ 'desktop_classes' ]
			),
			'display'  => 'basic',
			'echo'     => false,
		)
	);

	$mobile_classes = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields'   => array(
				$id . '-mobile_classes' => array(
					'label'   => '<i class="fa fa-mobile"></i> Mobile',
					'type'    => 'selectize',
					'choices' => widget_breakpoint_choices(),
					'atts'    => array(
						'multiple' => 'multiple',
						'data-key' => 'mobile'
					)
				)
			),
			'instance' => array(
				$id . '-mobile_classes' => $values[ 'mobile_classes' ]
			),
			'display'  => 'basic',
			'echo'     => false,
		)
	);

	$title_classes = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields'   => array(
				$id . '-title_classes' => array(
					'label'   => '<i class="fa fa-header"></i> Widget Title',
					'type'    => 'selectize',
					'choices' => widget_title_class_choices(),
					'atts'    => array(
						'multiple' => 'multiple',
						'data-key' => 'title'
					)
				)
			),
			'instance' => array(
				$id . '-title_classes' => $values[ 'title_classes' ]
			),
			'display'  => 'basic',
			'echo'     => false,
		)
	);

	$structure = '';
	$structure .= "<table width='100%' class='fixed'>";
	$structure .= "<tr valign='top'>";
	$structure .= "<td colspan='4'>{$name}</td>";
	$structure .= "</tr><tr valign='top'>";
	$structure .= "<td>{$global_classes}</td>";
	$structure .= "<td>{$desktop_classes}</td>";
	$structure .= "<td>{$mobile_classes}</td>";
	$structure .= "<td>{$title_classes}</td>";
	$structure .= "</tr>";
	$structure .= "</table>";

	return $structure;
}

/**
 * Remove unwanted buttons and add "Formats" to Visual Editor
 *
 * @param $buttons
 *
 * @return array
 */
function mce_register_buttons( $buttons ) {

	// Removes buttons
	unset(
		$buttons[7], // alignleft
		$buttons[8], // alignright
		$buttons[9], // aligncenter
		$buttons[16] // fontselect,fontsizeselect
	);

	// Adds FORMATS
	$buttons[] = 'styleselect';

	return $buttons;
}

/**
 * Add our formats to the styleselect button.
 *
 * @link https://codex.wordpress.org/TinyMCE_Custom_Styles
 *
 * @param $tiny_mce
 *
 * @return mixed
 */
function mce_insert_formats( $tiny_mce ) {


	$style_formats = [];

	foreach ( text_align() as $class => $data ) {
		$style_formats[] = array(
			'title' => $data[ 'name' ],
			'block' => 'div',
			'classes' => $class,
			'wrapper' => true
		);
	}

	foreach ( text_colors() as $class => $data ) {
		$style_formats[] = array(
			'title' => $data[ 'name' ],
			'inline' => 'span',
			'classes' => $class,
			'wrapper' => true
		);
	}

	foreach ( text_fonts() as $class => $data ) {
		$style_formats[] = array(
			'title' => $data[ 'name' ],
			'inline' => 'span',
			'classes' => $class,
			'wrapper' => true
		);
	}

	foreach ( background_colors() as $class => $data ) {
		$style_formats[] = array(
			'title' => $data[ 'name' ],
			'inline' => 'span',
			'classes' => $class,
			'wrapper' => true
		);
	}

	foreach ( link_colors() as $class => $data ) {
		$style_formats[] = array(
			'title' => $data[ 'name' ],
			'selector' => 'a',
			'classes' => $class,
		);
	}

	foreach ( button_colors() as $class => $data ) {
		$style_formats[] = array(
			'title' => $data[ 'name' ],
			'selector' => 'a',
			'classes' => $class . ' btn',
		);
	}

	foreach ( button_sizes() as $class => $data ) {
		$style_formats[] = array(
			'title' => $data[ 'name' ],
			'selector' => 'a',
			'classes' => $class . ' btn',
		);
	}

	$tiny_mce['style_formats'] = json_encode( $style_formats );

	return $tiny_mce;
}

/**
 * Add our styles to the editor so we can see what it will look like =
 */
function add_editor_styles() {
	add_editor_style( site_stellar_css_url() );
}
