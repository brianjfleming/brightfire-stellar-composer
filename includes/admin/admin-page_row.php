<?php

/**
 * Row Edit Page
 */

namespace BrightFire\Theme\Stellar;

function layouts_row_edit() {

	global $stellar_layout;

	scripts();
	
	// Security
	wp_nonce_field( 'layouts_ajax_nonce', 'security' );

	// Check if new or existing Row
	if ( isset( $_GET[ 'edit' ] ) ) {

		// Row Exists
		$row_data = $stellar_layout->get_layout_item( 'rows', $_GET['edit'] );
		$row_id = $_GET[ 'edit' ];

		// Row no longer exists
		if ( empty( $row_data ) ) {
			wp_die( 'This row no longer exists. Create a <a href="' . admin_url( 'admin.php?page=stellar-row-edit' ) . '">New Row</a>.' );
		}

		// Page Title
		$title = sprintf( __( 'Edit %s', 'bf_stellar' ), $row_data[ 'name' ] );
		
	} else {

		// Create New Row
		$the_row = $stellar_layout->get_layout_item( 'rows' );
		reset( $the_row );
		$row_id = key( $the_row );
		$row_data = $the_row[ $row_id ];

		// Page Title
		$title = __( 'New Row', 'bf_stellar' );
	}

	// Background Layer initial values
	$background_layer_values = array();
	if ( isset( $row_data[ 'background-layers' ] ) ) {
		foreach ( $row_data[ 'background-layers' ] as $index => $values ) {
			$id_prefix = "{$row_id}[ 'background-layers' ][{$index}]";
			$background_layer_values[ $index ]['title_bar'] = row_background_title( $values );
			$background_layer_values[ $index ]['fields']    = row_background_fields( $id_prefix, $values );
		}
	}

	// Container Background Layer initial values
	$container_background_layer_values = array();
	if ( isset( $row_data[ 'container-background-layers' ] ) ) {
		foreach ( $row_data[ 'container-background-layers' ] as $index => $values ) {
			$id_prefix = "{$row_id}[ 'container-background-layers' ][{$index}]";
			$container_background_layer_values[ $index ]['title_bar'] = row_background_title( $values );
			$container_background_layer_values[ $index ]['fields']    = row_background_fields( $id_prefix, $values );
		}
	}

	/******* Fields *******/

	// ID
	$id = '<input id="id" type="hidden" value="' . $row_id . '"/>';

	// Name
	$name = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields'     => array(
				'name'  => array(
					'type'    => 'text',
					'permit'  => 1,
				),
			),
			'instance'   => $row_data,
			'display'    => 'basic',
			'echo'       => false, )
	);

	// Description
	$description = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields'     => array(
				'description'  => array(
					'type'    => 'text',
					'permit'  => 1,
				),
			),
			'instance'   => $row_data,
			'display'    => 'basic',
			'echo'       => false, )
	);

	// Structure
	$column_widths = column_range_slider( $row_data[ 'left' ], $row_data[ 'right' ] );

	$height = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields' => array(
				'min_height' => array(
					'type' => 'selectize',
					'choices' => row_height_class_choices(),
					'permit' => 1,
				),
			),
			'instance' => $row_data,
			'display' => 'basic',
			'echo' => false
		)
	);

	$gutter = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields' => array(
				'gutter' => array(
					'type' => 'selectize',
					'choices' => column_gutter_padding_choices(),
					'permit' => 1,
				)
			),
			'instance' => $row_data,
			'display' => 'basic',
			'echo' => false
		)
	);

	// Global Classes
	$custom_classes = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields'     => array(
				'custom-classes'  => array(
					'type'    => 'selectize',
					'choices' => row_class_choices(),
					'atts'    => array(
						'multiple' => 'multiple',
					),
					'permit'  => 1,
				),
			),
			'instance'   => $row_data,
			'display'    => 'basic',
			'echo'       => false, )
	);

	// Desktop Classes
	$desktop_classes = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields'     => array(
				'desktop-classes'  => array(
					'type'    => 'selectize',
					'choices' => row_breakpoint_choices(),
					'atts'    => array(
						'multiple' => 'multiple',
					),
					'permit'  => 1,
				),
			),
			'instance'   => $row_data,
			'display'    => 'basic',
			'echo'       => false, )
	);

	// Mobile Classes
	$mobile_classes = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields'     => array(
				'mobile-classes'  => array(
					'type'    => 'selectize',
					'choices' => row_breakpoint_choices(),
					'atts'    => array(
						'multiple' => 'multiple',
					),
					'permit'  => 1,
				),
			),
			'instance'   => $row_data,
			'display'    => 'basic',
			'echo'       => false, )
	);

	// Background Settings
	$background_settings = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields'     => array(
				'background-behavior'  => array(
					'type'    => 'selectize',
					'label'   => 'Behavior',
					'choices' => array(
						'layered'    => array(
							'name' => 'Layered',
							'desc' => 'Backgrounds layer on top of each other.'
						),
						'row-slider'    => array(
							'name' => 'Slider',
							'desc' => 'Creates slides from each background layer.'
						),
						'parallax' => array(
							'name' => 'Witchcraft',
							'desc' => 'Renders background layers on top of each other and creates a parallax effect'
						)
					),
					'permit'  => 1,
				),
			),
			'instance'   => $row_data,
			'display'    => 'table',
			'echo'       => false, )
	);

	// Slider Options
	$hide_slide_options = ( 'layered' == $row_data[ 'background-behavior'] ) ? 'style="display:none"' : '';
	$background_settings .= '<div class="slide-options" ' . $hide_slide_options . '>' . \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields'     => array(
				'animation'      => array(
					'label'   => 'Animation',
					'type'    => 'selectize',
					'choices' => array(
						'fade'      => 'Fade',
						'crossfade' => 'Cross Fade',
						'slide'     => 'Slide',
					),
					'permit'  => 1,
				),
				'slideshowSpeed' => array(
					'label'         => 'Slide Duration',
					'type'          => 'range',
					'default_value' => 7,
					'permit'        => 1,
					'atts'          => array(
						'min'  => 0,
						'max'  => 10,
						'step' => .1
					),
					'settings'      => array(
						'min-label'    => 0,
						'max-label'    => 10,
						'units'        => 'Seconds',
						'show-current' => true,
					),
				),
				'animationSpeed' => array(
					'label'         => 'Transition Speed',
					'type'          => 'range',
					'default_value' => .6,
					'permit'        => 1,
					'atts'          => array(
						'min'  => 0,
						'max'  => 10,
						'step' => .1
					),
					'settings'      => array(
						'min-label'    => 0,
						'max-label'    => 10,
						'units'        => 'Seconds',
						'show-current' => true,
					),
				),
			),
			'instance'   => $row_data,
			'display'    => 'table',
			'echo'       => false, )
	) . '</div>';

	// Background Layers
	$background_layers_repeater = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields' => array(
				'background-layers' => array(
					'type' => 'repeater',
					'instance' => $background_layer_values,
					'title_cb' => array(
						'function' => __NAMESPACE__ . '\row_background_title',
						'params' => array(),
					),
					'fields_cb' => array(
						'function' => __NAMESPACE__ . '\row_background_fields',
						'params' => array( "{$row_id}[background-layers]" ),
					),
					'js_cb' => array( 'init_colorPickers', 'init_selectize', 'init_range' ),
					'collapse' => true,
					'sortable' => true,
					'add_label' => 'Add Layer Below',
					'del_label' => 'Remove Layer',
					'sanitize' => 'bypass',
				)
			),
			'section_id'    => 'background-layers',
			'display'         => 'basic',
			'echo'            => false,
			'instance'        => $background_layer_values
		)
	);

	// Container Design Settings
	$container_design = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields'     => array(
				'container-classes'  => array(
					'label'   => 'Container Classes',
					'type'    => 'selectize',
					'choices' => row_container_class_choices(),
					'atts'    => array(
						'multiple' => 'multiple',
					),
					'permit'  => 1,
				),
				'container-desktop-classes' => array(
					'label' => 'Container Desktop Only Classes',
					'type'  => 'selectize',
					'choices' => row_container_breakpoint_choices(),
					'atts' => array(
						'multiple' => 'multiple',
					),
					'permit' => 1,
				),
				'container-mobile-classes' => array(
					'label' => 'Container Mobile Only Classes',
					'type'  => 'selectize',
					'choices' => row_container_breakpoint_choices(),
					'atts' => array(
						'multiple' => 'multiple',
					),
					'permit' => 1,
				),
				'container-background-behavior'  => array(
					'type'    => 'selectize',
					'label'   => 'Container Background Behavior',
					'choices' => array(
						'layered'    => array(
							'name' => 'Layered',
							'desc' => 'Backgrounds layer on top of each other.'
						),
						'row-slider'    => array(
							'name' => 'Slider',
							'desc' => 'Creates slides from each background layer.'
						),
					),
					'permit'  => 1,
				),
			),
			'instance'   => $row_data,
			'display'    => 'table',
			'echo'       => false, )
	);

	// Slider Options
	$hide_container_slide_options = ( 'layered' == $row_data[ 'container-background-behavior'] ) ? 'style="display:none"' : '';
	$container_design .= '<div class="container-slide-options" ' . $hide_container_slide_options . '>' . \BF_Admin_API_Fields::bf_admin_api_fields_build(
			array(
				'fields'     => array(
					'container-animation'      => array(
						'label'   => 'Animation',
						'type'    => 'selectize',
						'choices' => array(
							'fade'      => 'Fade',
							'crossfade' => 'Cross Fade',
							'slide'     => 'Slide',
						),
						'permit'  => 1,
					),
					'container-slideshowSpeed' => array(
						'label'         => 'Slide Duration',
						'type'          => 'range',
						'default_value' => 7,
						'permit'        => 1,
						'atts'          => array(
							'min'  => 0,
							'max'  => 10,
							'step' => .1
						),
						'settings'      => array(
							'min-label'    => 0,
							'max-label'    => 10,
							'units'        => 'Seconds',
							'show-current' => true,
						),
					),
					'container-animationSpeed' => array(
						'label'         => 'Transition Speed',
						'type'          => 'range',
						'default_value' => .6,
						'permit'        => 1,
						'atts'          => array(
							'min'  => 0,
							'max'  => 10,
							'step' => .1
						),
						'settings'      => array(
							'min-label'    => 0,
							'max-label'    => 10,
							'units'        => 'Seconds',
							'show-current' => true,
						),
					),
				),
				'instance'   => $row_data,
				'display'    => 'table',
				'echo'       => false, )
		) . '</div>';

	// Container Background Layers
	$container_background_layers_repeater = \BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields' => array(
				'container-background-layers' => array(
					'type' => 'repeater',
					'instance' => $container_background_layer_values,
					'title_cb' => array(
						'function' => __NAMESPACE__ . '\row_background_title',
						'params' => array(),
					),
					'fields_cb' => array(
						'function' => __NAMESPACE__ . '\row_background_fields',
						'params' => array( "{$row_id}[container-background-layers]" ),
					),
					'js_cb' => array( 'init_colorPickers', 'init_selectize', 'init_range' ),
					'collapse' => true,
					'sortable' => true,
					'add_label' => 'Add Layer Below',
					'del_label' => 'Remove Layer',
					'sanitize' => 'bypass',
				)
			),
			'section_id'    => 'container-background-layers',
			'display'         => 'basic',
			'echo'            => false,
			'instance'        => $container_background_layer_values
		)
	);

	/******* Final Output *******/
	
	// Open Wrap
	$open = '<div class="wrap">';

	// Page Title
	$page_title = '<h1>' . $title . '</h1>';

	// General Settings
	$general = form_table( array(
		'tr' => array(
			array(

				'th' => 'Name',
				'td' => $name . $id,
			),
			array(
				'th' => 'Description',
				'td' => $description,
			),
			array(
				'th' => 'Structure',
				'td' => $column_widths,
			),
			array(
				'th' => 'Additional Gutter Padding',
				'td' => $gutter,
			),
			array(
				'th' => 'Height Options',
				'td' => $height,
			),
			array(
				'th' => 'Global Custom Classes',
				'td' => $custom_classes,
			),
			array(
				'th' => 'Desktop Classes',
				'td' => $desktop_classes,
			),
			array(
				'th' => 'Mobile Classes',
				'td' => $mobile_classes,
			),
		),
	) );

	// Preview
	$preview = '<div id="background-preview"></div>';

	// Design Settings
	$design = form_table( array(
		'tr' => array(
			array(
				'th' => 'Row Background',
				'td' => $background_settings . $background_layers_repeater,
			),
			array(
				'th' => 'Container Design',
				'td' => $container_design . $container_background_layers_repeater,
			),
		),
	) );

	// Delete Button
	$delete = ( false == $stellar_layout->is_required( 'rows', $row_id ) ) ? '<button id="delete-row" class="button">Delete Row</button> ' : '';

	// Save Button
	$save = '<button id="save-row" class="button-primary">Save Row</button>';

	// Close Wrap
	$close = '</div>';

	echo $open . $page_title . $general . $preview . $design . $delete . $save . $close;
}