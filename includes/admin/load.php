<?php
namespace BrightFire\Theme\Stellar;

require_once BF_STELLAR_INC . 'admin/admin-fields.php';
require_once BF_STELLAR_INC . 'admin/admin.php';
require_once BF_STELLAR_INC . 'admin/admin-ajax.php';
require_once BF_STELLAR_INC . 'admin/admin-page_settings.php';
require_once BF_STELLAR_INC . 'admin/admin-page_row.php';
require_once BF_STELLAR_INC . 'admin/admin-page_template.php';

/**
 * Adds our admin menus
 */
add_action( 'admin_menu', __NAMESPACE__ . '\stellar_admin_menus' );

/**
 * Admin custom scripts
 */
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\enqueue_admin_scripts' );

/*
 * Register ajax functionality
 */
add_action( 'wp_ajax_layouts_ajax', __NAMESPACE__ . '\ajax_layouts' );
add_action( 'wp_ajax_mixes_ajax', __NAMESPACE__ . '\ajax_mixes' );
add_action( 'wp_ajax_recipe_ajax', __NAMESPACE__ . '\ajax_theme_recipes' );
add_action( 'wp_ajax_customizer_ajax', __NAMESPACE__ . '\ajax_image_customizer' );
add_action( 'wp_ajax_slides_ajax', __NAMESPACE__ . '\ajax_slides' );
add_action( 'wp_ajax_repeater', __NAMESPACE__ . '\ajax_repeater' );
add_action( 'wp_ajax_theme_ajax', __NAMESPACE__ . '\ajax_theme' );
add_action( 'wp_ajax_delete_stellar_css', __NAMESPACE__ . '\ajax_delete_stellar_css' );
add_action( 'wp_ajax_stellar_scripts', __NAMESPACE__ . '\ajax_stellar_scripts' );
add_action( 'wp_before_admin_bar_render', __NAMESPACE__ . '\stellar_admin_bar' );
add_action( 'init', __NAMESPACE__ . '\recipe_import_export_handler' ); // Recipe File Header Force Download and upload
add_action( 'admin_menu', __NAMESPACE__ . '\remove_template_metabox' );
add_action( 'add_meta_boxes', __NAMESPACE__ . '\post_form_metabox' );
add_action( 'save_post', __NAMESPACE__ . '\save_post_force_ssl', 10, 2 );
add_filter( 'tiny_mce_before_init', __NAMESPACE__ . '\mce_insert_formats' );
add_filter( 'mce_buttons', __NAMESPACE__ . '\mce_register_buttons' );
add_action( 'admin_init', __NAMESPACE__ . '\add_editor_styles' );