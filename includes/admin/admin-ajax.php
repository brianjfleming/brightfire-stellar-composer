<?php

namespace BrightFire\Theme\Stellar;

/**
 * Ajax functions for CRUD of Layouts
 */
function ajax_layouts() {
	global $stellar_layout;
	
	check_ajax_referer( 'layouts_ajax_nonce', 'security' );

	// SAVE ROW
	if ( isset( $_POST[ 'save_row' ] ) ) {

		$row = $_POST[ 'save_row' ];

		$stellar_layout->update_layout_item( 'rows', $row );

		$response[ 'title' ]   = sprintf( __( 'Edit %s', 'bf_stellar' ), $row[ 'data' ][ 'name' ] );
		$response[ 'history' ] = 'admin.php?page=stellar-row-edit&edit=' . $row[ 'id' ];
		$response[ 'success' ] = admin_message( 'success', sprintf( __( 'Row: %s saved!' ), '<strong>' . $row[ 'data' ][ 'name' ] . '</strong>' ) );

		wp_send_json( $response );

	}

	// DELETE ROW
	if ( isset( $_POST[ 'delete_row' ] ) ) {

		$row_id = $_POST[ 'delete_row' ][ 'id' ];
		$stellar_layout->delete_layout_item( 'rows', $row_id );

		wp_send_json( admin_url( 'admin.php?page=stellar-settings&tab=rows&deleted=Row:%20' . $row[ 'name' ] ) );
	}

	// PREVIEW BACKGROUND
	if ( isset( $_POST[ 'preview_background' ] ) ) {
		$data = $_POST[ 'preview_background' ];

		$response = '<div id="background-preview" class="' . 
		            build_row_classes( $data ) . '" style="' . 
		            stellar_body_background_styles() . '">' .
		            build_row_background( $data ) .
		            '<div class="row-container ' .
		            build_container_classes( $data ) . '">' .
		            build_row_background( $data, 'container-background-layers' ) . '<span>Background preview</span></div></div>';

		wp_send_json( $response );
	}

	// SAVE TEMPLATE
	if ( isset( $_POST[ 'save_template' ] ) ) {

		// All new data
		$template = $_POST[ 'save_template' ];

		// Check if our Main Content Row is good
		if ( ! in_array( $template[ 'data' ][ 'sidebar_row' ], $template[ 'data' ][ 'content' ] ) ){
			$response[ 'title' ]   = sprintf( __( 'Edit %s', 'bf_stellar' ), $template[ 'data' ][ 'name' ] );
			$response[ 'history' ] = 'admin.php?page=stellar-template-edit&edit=' . $template[ 'id' ];
			$response[ 'success' ] = admin_message( 'error', 'Template not saved. <strong>Main Content Row</strong> is invalid or not set.' );

			wp_send_json( $response );
		}

		// Saved required assignments
		$assignments = $stellar_layout->get_layout_option( 'assignments' );
		$required = $assignments['required'];

		// Hold new assignments in variable
		$reassignments = $template[ 'data' ][ 'assignments' ];

		// Do not save assignments with the template
		unset( $template[ 'data' ][ 'assignments' ] );

		// Set up REQUIRED assignments
		foreach ( $reassignments as $reassigned ) {
			$required = array_merge( $required, array( $reassigned => $template[ 'id' ] ) );
		}

		$assignments['required'] = $required;

		$stellar_layout->update_layout_option( 'assignments', $assignments );

		// Sanitize layout fields that need it.
		$template['data']['name'] = sanitize_text_field( $template['data']['name'] );
		$template['data']['description'] = sanitize_text_field( $template['data']['description'] );

		$stellar_layout->update_layout_item( 'templates', $template );

		$response[ 'title' ]   = sprintf( __( 'Edit %s', 'bf_stellar' ), $template[ 'data' ][ 'name' ] );
		$response[ 'history' ] = 'admin.php?page=stellar-template-edit&edit=' . $template[ 'id' ];
		$response[ 'success' ] = admin_message( 'success', sprintf( __( 'Template: %s saved!' ), '<strong>' . stripslashes( $template[ 'data' ][ 'name' ] ). '</strong>' ) );

		wp_send_json( $response );
	}

	// TOGGLE ROW SELECTORS
	if ( isset( $_POST[ 'rows_change' ] ) ) {

		$header = $content = $footer = array();

		$template_rows = $_POST[ 'rows_change' ][ 'rows' ];

		foreach ( $template_rows[ 'headerRows' ] as $row_id ) {
			$header[ $row_id ] = $stellar_layout->get_layout_option( 'rows' )[ $row_id ][ 'name' ];
		}

		foreach ( $template_rows[ 'contentRows' ] as $row_id ) {
			$content[ $row_id ] = $stellar_layout->get_layout_option( 'rows' )[ $row_id ][ 'name' ];
		}

		foreach ( $template_rows[ 'footerRows' ] as $row_id ) {
			$footer[ $row_id ] = $stellar_layout->get_layout_option( 'rows' )[ $row_id ][ 'name' ];
		}

		$fixed = array_merge( array( '' => 'None' ), $header, $content, $footer );

		$response[ 'content' ] = select_field( 'contentrow-select', $content, $_POST[ 'rows_change' ][ 'content_selected' ] );
		$response[ 'fixed' ] = select_field( 'fixedrow-select', $fixed, $_POST[ 'rows_change' ][ 'fixed_selected' ] );
		wp_send_json( $response );
	}

	// DELETE TEMPLATE
	if ( isset( $_POST[ 'delete_template' ] ) ) {

		$template = $_POST[ 'delete_template' ];
		$stellar_layout->delete_layout_item( 'templates', $template[ 'id' ] );

		wp_send_json( admin_url( 'admin.php?page=stellar-settings&tab=templates&deleted=Template:%20' . $template[ 'name' ] ) );

	}

	// ADD WIDGET AREA
	if ( isset( $_POST[ 'add_widget_area' ] ) ) {

		$id_prefix = $_POST[ 'add_widget_area' ];

		$response = widget_area_repeater( '', $id_prefix );

		wp_send_json( $response );

	}

	// SAVE WIDGET AREA
	if ( isset( $_POST[ 'save_widget_area' ] ) ) {

		$widget_area = $_POST[ 'save_widget_area' ];
		$sort = $widget_area[ 'sort' ];
		$option = $widget_area[ 'option' ];

		// We don't need this info saved
		unset( $widget_area[ 'sort' ] );
		unset( $widget_area[ 'option' ] );

		// Update our layout
		$stellar_layout->update_layout_item( $option, $widget_area  );

		// Update our sort keys
		$sorted = sort_layout_items( $option, $sort );
		$stellar_layout->update_layout_option( $option, $sorted );

		$response = admin_message( 'success', sprintf( __( '%s saved!' ), '<strong>' . stripslashes( $widget_area['data'][ 'name' ] ). '</strong>' ) );

		wp_send_json( $response );

	}

	// SORT ITEMS
	if ( isset( $_POST[ 'sort_items' ] ) ) {

		$items = $_POST[ 'sort_items' ];

		$sorted = sort_layout_items( $items[ 'option' ], $items[ 'ids' ] );
		$stellar_layout->update_layout_option( $items[ 'option' ], $sorted );

	}

	// DELETE WIDGET AREA
	if ( isset( $_POST[ 'delete_widget_area' ] ) ) {

		$widget_area = $_POST[ 'delete_widget_area' ];
		$stellar_layout->delete_layout_item( $widget_area[ 'option' ], $widget_area[ 'id' ] );

		$response = admin_message( 'success_deleted', sprintf( __( '%s deleted.' ), '<strong>' . stripslashes( $widget_area[ 'name' ] ). '</strong>' ) );

		wp_send_json( $response );
	}

	if ( isset( $_POST[ 'toggle_thumb' ] ) ) {

		$response = row_thumbnail( $_POST[ 'toggle_thumb' ] );

		wp_send_json( $response );

	}
}

function ajax_mixes() {

	check_ajax_referer( 'mixes_ajax_nonce', 'security' );

	// SAVE MIXES
	if ( isset( $_POST[ 'save_mixes' ] ) ) {
		update_option( 'stellar_mixes', $_POST[ 'save_mixes' ] );
		$response[ 'success' ] = admin_message( 'success', 'Mixes Saved' );

		wp_send_json( $response );
	}

}

function ajax_slides() {

	if ( isset( $_POST[ 'formBackground' ] ) ) {

		$response = background_style_generator( $_POST[ 'formBackground' ] );

		wp_send_json( $response );
	}
}

function ajax_theme() {

	check_ajax_referer( 'theme_ajax_nonce', 'security' );

	if ( isset( $_POST[ 'set_theme' ] ) ) {

		// Set our Theme Mod
		set_theme_mod( 'theme', $_POST[ 'set_theme' ] );

		// Recompile our SCSS
		delete_stellar_css();

		// Make Note that we have switched our theme for hooking on recompile
		set_theme_mod( 'stellar_theme_switched', true );

		wp_send_json( true );
	}

	if ( isset( $_POST[ 'reset_theme' ] ) ) {

		// Remove Theme Mod
		remove_theme_mod( 'theme' );

		// Recompile our SCSS
		delete_stellar_css();

		wp_send_json( true );
	}
}

function ajax_repeater() {

	if ( isset( $_POST[ 'rowSelected'] ) ) {

		$response = '<div class="bf-admin-api bf-repeater-collapse"></div>';
		$response .= row_thumbnail( $_POST[ 'rowSelected' ] );

		wp_send_json( $response );
	}
}

/**
 * Function to switch through CRUD operations of recipes
 */
function ajax_theme_recipes() {

	check_ajax_referer( 'layouts_ajax_nonce', 'security' );

	/**
	 * Save Recipe
	 */
	if ( isset( $_POST[ 'save' ] ) && '' != $_POST[ 'save' ] ) {

		$recipe_name = $_POST[ 'save' ];
		$scope = $_POST[ 'scope' ];

		// Recipe ID
		$recipe_id = 'stellar_recipe_' . sanitize_title( $recipe_name );

		// Get current theme recipes for this site
		$current_recipes = get_recipes()[ $scope ];

		// Do not allow saving recipes of the same name.
		if ( $current_recipes ) {
			foreach( $current_recipes as $recipe ) {

				if ( $recipe_id == $recipe[ 'recipe_id' ] ) {

					$response = array(
						'delete' => recipe_selector( 'delete-recipe-selector' ),
						'load' => recipe_selector( 'load-recipe-selector' ),
						'export' => recipe_selector( 'export-recipe-selector' ),
						'message' => admin_message( 'error', sprintf( __('Please choose a different name, %s is already used.' ), '<strong>"' . $recipe_name . '"</strong>' ) )
					);

					wp_send_json( $response );
				}
			}
		}

		// Layout
		$layout = get_option( 'stellar_layout' );

		// Clean sidebars
		// TODO: remove this once production recipes have been saved
		foreach ( $layout[ 'assignments' ][ 'sidebars' ] as $page => $sidebar ) {
			if ( preg_match( '/revision/', $page ) || preg_match( '/autosave/', $page ) ) {
				unset( $layout[ 'assignments' ][ 'sidebars' ][ $page ] );
			}
		}

		// Widgets
		$sidebars = wp_get_sidebars_widgets();

		// Remove orphaned widgets from array
		foreach ( $sidebars as $key => $value ) {
			if ( strpos( $key, 'orphaned_widgets' ) !== false ) {
				unset( $sidebars[ $key ] );
			}
		}

		// Loop through all widgets and set names to retrieve settings
		$all_widgets = array();
		foreach ( $sidebars as $sidebar => $widgets ) {
			foreach ( $widgets as $widget ) {
				$instance = preg_replace( '/-[0-9]+$/', '', $widget );
				$all_widgets[] = 'widget_' . $instance;
			}
		}

		// Get widget settings and hold in array
		$widget_settings = array();
		foreach ( array_unique( $all_widgets ) as $widget ) {
			$widget_settings[ $widget ] = get_option( $widget );
		}

		// Roll up the entire option
		$recipe = array(
			'original_env'    => BF_ENV,
			'original_blog'   => get_current_blog_id(),
			'recipe_name'     => $recipe_name,
			'recipe_id'       => $recipe_id,
			'layout'          => $layout,
			'theme_mods'      => get_theme_mods(),
			'sidebars'        => $sidebars,
			'widget_settings' => $widget_settings,
			'mixes'           => get_option( 'stellar_mixes', array() )
		);

		$recipe = recipe_image_save( $recipe );
		
		// Save recipe
		update_scoped_option( $scope, $recipe_id, $recipe );

		// Update page
		$response = array(
			'delete' => recipe_selector( 'delete-recipe-selector' ),
			'load' => recipe_selector( 'load-recipe-selector' ),
			'export' => recipe_selector( 'export-recipe-selector' ),
			'message' => admin_message( 'success', sprintf( __( '%s saved!' ), '<strong>' . $recipe_name . '</strong>' ) )
		);

		wp_send_json( $response );
	}

	/**
	 * Load Recipe
	 */
	if ( isset( $_POST[ 'load' ] ) ) {

		$id = $_POST[ 'load' ];
		$scope = $_POST[ 'scope' ];
		
		// Recipe data
		$recipe = get_scoped_option( $id, $scope );

		// Check if we want to import images
		$recipe[ 'import_images' ] = $_POST[ 'import_images' ];
		
		// Load the recipe
		load_recipe( $recipe );

		$response = admin_url( 'admin.php?page=stellar-settings&loaded=Recipe: ' . $recipe[ 'recipe_name' ]);

		wp_send_json( $response );
	}

	/**
	 * Delete Recipe
	 */
	if ( isset( $_POST['delete'] ) || '0' === $_POST['delete'] ) {

		$id = $_POST[ 'delete' ];
		$scope = $_POST[ 'scope' ];

		// Name for display on success
		$recipe_name = get_scoped_option( $id, $scope )[ 'recipe_name' ];

		// Delete
		delete_scoped_option( $id, $scope );

		// Update page
		$response = array(
			'delete' => recipe_selector( 'delete-recipe-selector' ),
			'load' => recipe_selector( 'load-recipe-selector' ),
			'export' => recipe_selector( 'export-recipe-selector' ),
			'message' => admin_message( 'success_deleted', sprintf( __( '%s deleted!' ), '<strong>' . $recipe_name . '</strong>' ) )
		);

		wp_send_json( $response );
	}


}

function ajax_image_customizer() {

	if ( isset( $_POST[ 'imageID' ] ) ) {

		$image_url = wp_get_attachment_image_src( $_POST[ 'imageID' ], 'full' )[ 0 ];

		wp_send_json( $image_url );

	}
	if ( isset( $_POST[ 'build' ] ) ) {

		$data = $_POST[ 'build' ];

		$response[ 'css' ] = compile_site_scss( $data );

		$response[ 'fonts' ] = stellar_font_styles( $data[ 'header-font-family' ], $data[ 'base-font-family' ], $data[ 'accent-font-family' ] );

		wp_send_json( $response );
	}
}

/**
 * Ajax callback for Delete CSS admin bar trigger
 */
function ajax_delete_stellar_css() {

	check_ajax_referer( 'delete-stellar-css', 'security' );

	if ( isset( $_POST[ 'delete' ] ) && 'stellarCSS' == $_POST[ 'delete' ] ) {

		delete_stellar_css();
	}
}

/**
 * Ajax callback for Delete CSS admin bar trigger
 */
function ajax_stellar_scripts() {

	check_ajax_referer( 'stellar-scripts', 'security' );

	if ( isset( $_POST[ 'save_scripts' ] ) ) {

		update_option( 'stellar_scripts', $_POST[ 'save_scripts' ] );

		$response = admin_message( 'success', sprintf( __( '%s saved!' ), '<strong>Scripts</strong>' ) );

		wp_send_json( $response );
	}
}