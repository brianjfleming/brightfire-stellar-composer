<?php

/**
 * Template Edit Page
 */

namespace BrightFire\Theme\Stellar;


function layouts_template_edit() {

	global $stellar_layout;

	scripts();
	
	// Security
	wp_nonce_field( 'layouts_ajax_nonce', 'security' );

	// Check if new or existing Template
	if ( isset( $_GET[ 'edit' ] ) ) {

		// Template
		$template = $stellar_layout->get_layout_item( 'templates', $_GET['edit'] );
		$template_id = $_GET[ 'edit' ];

		// Template no longer exists
		if ( empty( $template ) ) {
			wp_die( 'This template no longer exists. Create a <a href="' . admin_url( 'themes.php?page=stellar-template-edit' ) . '">New Template</a>.' );
		}
		
		// Page title
		$title = sprintf( __( 'Edit %s', 'bf_stellar' ), stripslashes( $template[ 'name' ] ) );

	} else {

		// Template
		$the_template = $stellar_layout->get_layout_item( 'templates' );

		reset( $the_template );
		$template_id = key( $the_template );
		$template = $the_template[$template_id];

		// Page title
		$title = __( 'New Template', 'bf_stellar' );
	}
	
	////////////
	// Fields //
	////////////
	$id               = '<input id="id" type="hidden" value="' . $template_id . '"/>';
	$name             = '<input id="name" class="large-text" type="text" value="' . stripslashes( $template[ 'name' ] ) . '" />';
	$description      = '<textarea id="description" class="large-text">' . stripslashes( $template[ 'description' ] ) . '</textarea>';

	$row_sections = array( 'header', 'content', 'footer' );
	$content_row_options = array();
	$fixed_row_options = array(
		'' => 'None'
	);

	foreach ( $row_sections as $section ) {

		$instance = array();

		if ( isset( $template[ $section ] ) ) {
			foreach ( $template[ $section ] as $index => $value ) {
				$instance[ $index ]['title_bar'] = row_thumbnail( $value );
				$instance[ $index ]['fields']    = select_rows_choices( $value );

				if ( 'content' == $section ) {
					$content_row_options[ $value ] = $stellar_layout->get_layout_option('rows')[ $value ][ 'name' ];
				}

				$fixed_row_options[ $value ] = $stellar_layout->get_layout_option('rows')[ $value ][ 'name' ];
			}
		}

		$repeater_fields = array(
			'template-rows' => array(
				'type' => 'repeater',
				'instance' => $instance,
				'title_cb' => array(
					'function' => __NAMESPACE__ . '\row_thumbnail',
					'params' => array(),
				),
				'fields_cb' => array(
					'function' => __NAMESPACE__ . '\select_rows_choices',
					'params' => '',
				),
				'js_cb' => array( 'init_selectize', 'init_content_row_select' ),
				'collapse' => true,
				'sortable' => true,
				'add_label' => 'Add Row Below',
				'del_label' => 'Remove Row',
				'sanitize' => 'bypass',

			),
		);

		// Build our widget form
		$args = array(
			'fields'          => $repeater_fields,
			'display'         => 'basic',
			'echo'            => false,
			'instance'        => $instance
		);
		$repeater = \BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

		// Stores data in a variable named for the current section
		// example: If the loop is on "header" then the following data is stored in the $header variable
		$$section = array(
			'id' => $section,
			'th' => ucfirst( $section ) . ' rows',
			'td' => $repeater,
		);
	}

	$table = form_table(
		array(
			'tr' => array(
				array(
					'th' => 'Name',
					'td' => $name . $id,
				),
				array(
					'th' => 'Description',
					'td' => $description,
				),
				$header,
				$content,
				$footer,
				array(
					'id' => 'contentrow',
					'th' => __( 'Main Content Row', 'bf_stellar' ),
					'td' => select_field( 'contentrow-select', $content_row_options, $template[ 'sidebar_row' ]  ) . "<sup>*Required</sup>"
				),
				array(
					'id' => 'sidebarcol',
					'th' => __( 'Sidebar Column', 'bf_stellar' ),
					'td' => select_field( 'sidebar-col', left_right_choices(), $template[ 'sidebar_col' ] ) . '<p>The column of the Main Content Row you wish to act as a sidebar.</p>',
				),
				array(
					'id' => 'fixedrow',
					'th' => __( 'Fixed Row', 'bf_stellar' ),
					'td' => select_field( 'fixedrow-select', $fixed_row_options, $template[ 'fixed_row' ]  ) . '<p>If you want a row on this template to be fixed to the top of screen on travel</p>'
//					'td' => select_rows_choices( $template[ 'fixed_row' ] ) . '<p>If you want a row on this template to be fixed to the top of screen on travel</p>',
				),
				array(
					'id' => 'assignments',
					'th' => 'Required assignments',
					'td' => assignment_checkboxes( $template_id ),
				),
			)
		)
	);

	// Check if we can delete
	if ( $stellar_layout->is_required( 'templates', $template_id ) ) {
		$delete = '';
	} else {
		$delete = '<button id="delete-template" class="button">Delete Template</button> ';
	}

	// Final output
	$open = '<div class="wrap"><h1>' . $title . '</h1>';
	$close = $delete . '<button id="save-template" class="button-primary">Save Template</button></div>';
	echo $open . $table . $close;
}