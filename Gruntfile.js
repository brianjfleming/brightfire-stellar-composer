module.exports = function( grunt ) {

	// Project configuration
	grunt.initConfig( {
		pkg:    grunt.file.readJSON( 'package.json' ),
		concat: {
			options: {
				stripBanners: true,
				banner: '/*! <%= pkg.title %> - v<%= pkg.version %>\n' +
					' * <%= pkg.homepage %>\n' +
					' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
					' */\n'
			},
			main: {
				src: [
					'assets/js/src/plugins/jquery.actual.js',
					'assets/js/src/plugins/jquery.flexslider.js',
					'assets/js/src/brightfire-stellar.js',
					'assets/js/src/bf_parallax.js',
					'assets/js/src/bfmenu.js'
				],
				dest: 'assets/js/brightfire-stellar.js'
			},
			customizer_preview: {
				src: [
					'assets/js/src/obj.customizer-preview.js'
				],
				dest: 'assets/js/obj.customizer-preview.js'
			},
			customizer: {
				src: [
					'assets/js/src/color-picker.js',
					'assets/js/src/obj.customizer.js'
				],
				dest: 'assets/js/obj.customizer.js'
			},
			admin: {
				src: [
					'assets/js/src/admin/admin.js',
					'assets/js/src/admin/recipes.js',
					'assets/js/src/admin/templates.js',
					'assets/js/src/admin/themes.js',
					'assets/js/src/admin/widget_areas.js',
					'assets/js/src/admin/rows.js',
					'assets/js/src/admin/mixes.js',
					'assets/js/src/admin/scripts.js',
					'assets/js/src/admin/jquery.bf_repeater_title_preview.js',
					'assets/js/src/admin/jquery.row_background_preview.js',
					'assets/js/src/plugins/wp-color-picker-alpha.js',
					'assets/js/src/color-picker.js'
				],
				dest: 'assets/js/admin.js'
			}
		},
		jshint: {
			all: [
				'Gruntfile.js',
				'assets/js/src/**/*.js',
				'!assets/js/src/plugins/*.js',
				'assets/js/test/**/*.js',
				'!assets/js/src/bfmenu.js'
			]
		},
		uglify: {
			all: {
				files: {
					'assets/js/brightfire-stellar.min.js': ['assets/js/brightfire-stellar.js'],
					'assets/js/admin.min.js': ['assets/js/admin.js'],
					'themes/peterson/compiled.min.js': ['assets/js/brightfire-stellar.js','themes/peterson/scripts.js'],
					'themes/gillman/compiled.min.js': ['assets/js/brightfire-stellar.js','themes/gillman/scripts.js','themes/gillman/js/waypoints.min.js']
				},
				options: {
					banner: '/*! <%= pkg.title %> - v<%= pkg.version %>\n' +
						' * <%= pkg.homepage %>\n' +
						' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
						' */\n',
					mangle: {
						except: ['jQuery']
					}
				}
			}
		},
		
		sass:   {
			all: {
				options: {
					precision: 2
				},
				files: {
					'assets/css/admin.css': 'assets/css/sass/admin.scss'
				}
			}
		},
		
		
		autoprefixer: {
			dist: {
				options: {
					browsers: [ 'last 1 version', '> 1%', 'ie 8' ]
				},
				files: { 
					// 'assets/css/brightfire-stellar.css': [ 'assets/css/brightfire-stellar.css' ],
					'assets/css/admin.css': [ 'assets/css/admin.css' ]
				}
			}
		},
		
		cssmin: {
			options: {
				banner: '/*! <%= pkg.title %> - v<%= pkg.version %>\n' +
					' * <%=pkg.homepage %>\n' +
					' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
					' */\n'
			},
			minify: {
				expand: true,

				cwd: 'assets/css/',
				src: ['brightfire-stellar.css'],

				dest: 'assets/css/',
				ext: '.min.css'
			}
		},
		watch:  {
			livereload: {
				files: ['assets/css/*.css'],
				options: {
					livereload: true
				}
			},
			styles: { 
				files: ['assets/css/sass/admin.scss'],
				tasks: ['sass', 'autoprefixer', 'cssmin'],
				options: {
					debounceDelay: 500
				}
			},
			scripts: {
				files: ['assets/js/src/**/*.js', 'assets/js/vendor/**/*.js', 'themes/**/scripts.js'],
				tasks: ['jshint', 'concat', 'uglify'],
				options: {
					debounceDelay: 500
				}
			}
		},
		clean: {
			main: ['release/<%= pkg.version %>']
		},
		copy: {
			// Copy the theme to a versioned release directory
			main: {
				src:  [
					'**',
					'!**/.*',
					'!**/readme.md',
					'!node_modules/**',
					'!vendor/**',
					'!tests/**',
					'!release/**',
					'!assetsjs/src/**',
					'!images/src/**',
					'!bootstrap.php',
					'!bower.json',
					'!composer.json',
					'!composer.lock',
					'!Gruntfile.js',
					'!package.json',
					'!phpunit.xml',
					'!phpunit.xml.dist',
					'!bootstrap.php.dist'
				],
				dest: 'release/<%= pkg.version %>/'
			}
		},
		compress: {
			main: {
				options: {
					mode: 'zip',
					archive: './release/brightfire-stellar.<%= pkg.version %>.zip'
				},
				expand: true,
				cwd: 'release/<%= pkg.version %>/',
				src: ['**/*'],
				dest: 'brightfire-stellar/'
			}
		}
	} );

	// Load tasks
	require('load-grunt-tasks')(grunt);

	// Register tasks
	
	grunt.registerTask( 'default', [ 'watch' ] );
	

	grunt.registerTask( 'build', ['clean', 'copy', 'compress', 'clean'] );


	grunt.util.linefeed = '\n';
};
